var webpack = require('webpack');
module.exports = {
    entry: './resources/assets/js/react/App.js',
    output: {
        path: './public/js',
        filename: 'components.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react']
                }
            }
        ]
    },
    plugins: [
       // new webpack.optimize.UglifyJsPlugin({minimize: true})
    ]
};