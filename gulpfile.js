var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.copy('bower_components/jquery/dist/jquery.min.js', 'public/js/');
    mix.copy('bower_components/bootstrap/dist/js/bootstrap.min.js', 'public/js/');

    mix.copy('bower_components/moment/min/moment.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/angular/angular.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/angular-route/angular-route.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/angular-moment/angular-moment.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/angular-animate/angular-animate.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/autosize/dist/autosize.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/angular-loading-bar/build/loading-bar.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/cropper/dist/cropper.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/ng-cropper/dist/ngCropper.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/ng-file-upload/ng-file-upload-all.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/lightslider/dist/js/lightslider.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/promise-polyfill/promise.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/react/react.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/react/react-dom.min.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/alt/dist/alt.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/jquery-auto-complete/jquery.auto-complete.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/jquery.cookie/jquery.cookie.js', 'resources/assets/js/vendor/');
    mix.copy('bower_components/notifyjs/dist/notify.js', 'resources/assets/js/vendor/');
    mix.copy('vendor/talyssonoc/react-laravel/assets/react_ujs.js', 'resources/assets/js/vendor/');

    mix.copy('bower_components/font-awesome/css/font-awesome.min.css', 'public/stylesheets/');

    mix.copy('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css', 'resources/assets/css/vendor/');
    mix.copy('bower_components/angular-loading-bar/build/loading-bar.min.css', 'resources/assets/css/vendor/'),
    mix.copy('bower_components/ng-img-crop/compile/minified/ng-img-crop.css', 'resources/assets/css/vendor/');
    mix.copy('bower_components/cropper/dist/cropper.min.css', 'resources/assets/css/vendor/');
    mix.copy('bower_components/lightslider/dist/css/lightslider.min.css', 'resources/assets/css/vendor/');
    mix.copy('bower_components/jquery-auto-complete/jquery.auto-complete.css', 'resources/assets/css/vendor/');

    mix.copy('bower_components/font-awesome/fonts/', 'public/fonts/');
    mix.copy('bower_components/lightslider/dist/img/controls.png', 'public/img');

    mix.copy('resources/assets/img/', 'public/img/');
});

elixir(function(mix) {
    mix.scripts([
            'vendor/bootstrap-datetimepicker.min.js',
            'vendor/jquery.maskMoney.min.js',
            'vendor/jquery.cookie.js',
            'vendor/autosize.min.js',
            'vendor/lightslider.min.js',
            'vendor/jquery.auto-complete.js',
            'vendor/notify.js',
            'vendor/react_ujs.js',
            'vendor/promise.min.js',

        ], 'public/js/vendor.js');
});

elixir(function(mix) {
    mix.scripts([
            'vendor/angular.min.js',
            'vendor/angular-route.min.js',
            'vendor/angular-moment.min.js',
            'vendor/angular-animate.min.js',
            'vendor/loading-bar.min.js',
            'vendor/cropper.min.js',
            'vendor/ngCropper.min.js',
            'vendor/ng-file-upload-all.min.js',

            'angular/app.js',
            'angular/comment/controller.js',
            'angular/post/controller.js',
            'angular/post/directive.js',
            'angular/image/controller.js',
            'angular/user/controller.js',

        ], 'public/js/angular.js');
});

elixir(function(mix) {
    mix.scripts('vendor/jquery.auto-complete.js', 'public/js/jquery-auto-complete.js');
});

elixir(function(mix) {
    mix.scripts([
        '../vendor/emoji-picker/lib/js/nanoscroller.min.js',
        '../vendor/emoji-picker/lib/js/tether.min.js',
        '../vendor/emoji-picker/lib/js/config.js',
        '../vendor/emoji-picker/lib/js/util.js',
        '../vendor/emoji-picker/lib/js/jquery.emojiarea.js',
        '../vendor/emoji-picker/lib/js/emoji-picker.js',
    ], 'public/js/emoji-picker.js');
});

/*
 React files
*/
elixir(function(mix) {
    mix.scripts([
            'vendor/moment.min.js',
            'vendor/alt.js',
            'vendor/react.min.js',
            'vendor/react-dom.min.js',
        ],
        'public/js/react.js')
});

elixir(function(mix) {
    mix.scripts([
            'app.js'
        ], 
        'public/js/app.js');
});

elixir(function(mix) {
    mix.less('bootstrap-custom.less', __dirname + '/resources/assets/css/vendor/', {
        paths: __dirname + '/bower_components/bootstrap/less',
        compress: true
    });
});


elixir(function(mix) {
     mix.babel(['resources/assets/js/react/*.js',
                'resources/assets/js/react/actions/*.js',
                'resources/assets/js/react/stores/*.js', 
                'resources/assets/js/react/components/*.js',
    ], 'public/js/components.js');
});

elixir(function(mix) {
    mix.styles([
            'vendor/bootstrap-custom.css',
            'vendor/bootstrap-datetimepicker.css',
            'vendor/loading-bar.min.css',
            'vendor/ng-img-crop.css',
            'vendor/cropper.min.css',
            'vendor/lightslider.min.css',
            'vendor/jquery.auto-complete.css',
        ],
        'public/stylesheets/vendor.css')
});

elixir(function(mix) {
    mix.copy('resources/assets/vendor/emoji-picker/lib/img', 'public/img')
});

elixir(function(mix) {
    mix.styles('styles.css', 'public/stylesheets/styles.css')
});

var htmlmin = require('gulp-htmlmin');
var gulp = require('gulp');

gulp.task('compress', function() {
    var opts = {
        collapseWhitespace:    true,
        removeAttributeQuotes: false,
        conservativeCollapse: true,
        removeComments:        true,
        minifyJS:              true,
        ignoreCustomFragments: [ /<\?php[^\?]+\?>/g]
    };

    return gulp.src('./storage/framework/views/**/*')
               .pipe(htmlmin(opts))
               .pipe(gulp.dest('./storage/framework/views/'));
});
