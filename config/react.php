<?php

return [
  'source'     => public_path('js/react.js'),
  'components' => public_path('js/components.js'),
];
