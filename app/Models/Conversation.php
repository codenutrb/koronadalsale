<?php namespace App\Models;

use Log;
use Jenssegers\Mongodb\Model as Eloquent;

class Conversation extends Eloquent
{
    /**
    * The collection used by the model.
    *
    * @var string
    */
    protected $collection = 'conversation';

    /**
    * The attributes for the model.
    *
    * @var Array
    */
    public $fillable = ['user_id_1', 'user_id_2', 'user1_has_new', 'user2_has_new', 'message_count'];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var Array
    */
    protected $dates = ['deleted_at'];

    /**
    * One to many realtionship with messages
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function messages()
    {
        return $this->hasMany('App\Models\Message')
                    ->orderBy('created_at', 'desc');
    }


    public function activeMessages($userId) {

        if($this->user_id_1 === $userId) {
            $messages = $this->messages()
                        ->where('user1_has_deleted', '!=', true);
        } else {
            $messages = $this->messages()
                        ->where('user2_has_deleted', '!=', true);
        }

        return $messages->take(10)
                        ->get();
    }

    /**
    * Many to one relationship with user 
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user1() 
    {
        return $this->belongsTo('App\Models\User', 'user_id_1')
                    ->select(['id', 'name', 'avatar']);
    }

    /**
    * Many to one relationship with user 
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user2() 
    {
        return $this->belongsTo('App\Models\User', 'user_id_2')
                    ->select(['id', 'name', 'avatar']);
    }

    public function messagesCount() 
    {
        return $this->messages()
                    ->selectRaw('conversation_id, count(*) as aggregate')
                    ->groupBy('conversation_id');
    }

    /*public function getMessagesCountAttribute() 
    {
        if(!array_key_exists('messagesCount', $this->relation)) {
            $this->load('messagesCount');
        }

        $related = $this->getRelation('messagesCount');

        return ($related) ? (int) $related->aggregate : 0;
    }*/
}