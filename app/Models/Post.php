<?php
namespace App\Models;

use Jenssegers\Mongodb\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Eloquent 
{
    use SoftDeletes;
    
    /**
    * The collection used by the model
    *
    * @var string
    */
    protected $collection = 'post';

    /**
    * The attributes for the model.
    *
    * @var array
    */
    public $fillable = ['title', 'description', 'category', 'price', 'photos', 'location', 'likes'];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
    * Many to One relation
    *
    * @return Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user() 
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
    * One to Many relation
    *
    * @return Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function comments() 
    {
        return $this->hasMany('App\Models\Comment')->orderBy('created_at', 'desc')->where('deleted_at', null);
    }

}

