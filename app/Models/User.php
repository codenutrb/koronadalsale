<?php
namespace App\Models;

use Jenssegers\Mongodb\Model as Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Eloquent implements AuthenticatableContract, 
                                       CanResetPasswordContract 
{
    use Authenticatable, CanResetPassword, SoftDeletes;
 
    /**
    * The database collection used by the model.
    *
    * @var string
    */
    protected $collection = 'user';

    /**
    * The attributes for the model.
    *
    * @var array
    */
    public $fillable = [
        'name', 
        'email', 
        'location', 
        'phone', 
        'password', 
        'confirmation_code', 
        'avatar', 
        'avatar_original', 
        'gender', 
        'birthday', 
        'fb_id'
    ];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['password', 'confirmation_code'];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
    * One to Many relation.
    *
    * @return Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function posts() 
    {
        return $this->hasMany('App\Models\Post');
    }

    /**
    * One to Many realtion.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function comments() 
    {
        return $this->hasMany('App\Models\Comment');
    }

    /**
    * One to Many realtion.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function followers() 
    {
        return $this->hasMany('App\Models\Follow', 'followee');
    }

    /**
    * One to Many realtion.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function following() 
    {
        return $this->hasMany('App\Models\Follow', 'follower');
    }

    /**
    * One to Many relation with Notification
    *
    * @return \Illuminate\Database\Eloquent\HasMany
    */
    public function notifications()
    {
        return $this->hasMany('App\Models\Notification', 'to') 
                    ->orderBy('created_at', 'desc');
    }

    /**
    * Get selected attributes for user
    *
    * @param array $props
    * @return array
    */
    public function shallowUser($props = ['_id', 'name', 'avatar', 'avatar_original'])
    {
        return array_only($this->toArray(), $props);
    }
}
