<?php
namespace App\Models;

use Jenssegers\Mongodb\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Eloquent
{
    use SoftDeletes;

    /**
    * The collection used by the model.
    *
    * @var string
    */
    protected $collection = 'message';

    /**
    * The attributes of the model with default value
    *
    * @var string
    */
    protected $attributes = ['has_read' => false];

    /**
    * The attributes for the model.
    *
    * @var Array
    */
    public $fillable = ['conversation_id', 'body', 'from_user_id', 'to_user_id', 'has_read'];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var Array
    */
    protected $dates = ['deleted_at'];

    /**
    * Many to one relation to user as sender
    *
    * @return \Illuminate\Database\Eloquent\BelongsTo
    */
    public function user() 
    {
        return $this->belongsTo('App\Models\User')
                    ->select(['_id', 'name', 'avatar']);
    }

    /**
    * Many to one relation to conversation
    *
    * @return \Illuminate\Database\Eloquent\BelongsTo
    */
    public function conversation() 
    {
        return $this->belongsTo('App\Models\Conversation', 'conversation_id');
    }
}