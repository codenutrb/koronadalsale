<?php namespace App\Models;

use Log;
use Jenssegers\Mongodb\Model as Eloquent;

class Follow extends Eloquent
{

    /** 
    * The collection used by the model
    *
    * @var $string
    */
    public $collection = "follow";

     /**
    * The attributes for the model.
    *
    * @var string
    */
    public $fillable = ['follower', 'followee'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
    * User follower relationship
    *
    * @return \Illuminate\Database\Eloquent\BelongsTo
    */
    public function _follower()
    {
        return $this->belongsTo('App\Models\User', 'follower');
    }

    /**
    * User following relationship
    *
    * @return \Illuminate\Database\Eloquent\BelongsTo
    */
    public function _followee()
    {
        return $this->belongsTo('App\Models\User', 'followee');
    }
}