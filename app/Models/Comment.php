<?php
namespace App\Models;

use Log;
use Jenssegers\Mongodb\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Eloquent 
{
    use SoftDeletes;

    /**
    * The collection used by the model.
    *
    * @var string
    */
    public $collection = 'comment';

    /**
    * The attributes for the model.
    *
    * @var string
    */
    public $fillable = ['body'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
    * Many to One relationship.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user() 
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
    * Many to One relationship.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function post() 
    {
        return $this->belongsTo('App\Models\Post');
    }

    /**
    * Get selected attributes for user
    *
    * @param array $props
    * @return array
    */
    public function shallowUser($props)
    {
        $shallowUser = [];
        foreach($props as $key => $prop) {
            $shallowUser[$prop] = $this->user[$prop];
        }
        return $shallowUser;
    }
}
