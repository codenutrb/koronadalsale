<?php namespace App\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Notification extends Eloquent
{
    /**
    * The collection used by the model
    *
    * @var string
    */
    protected $collection = 'notification';

    /**
    * The attributes for the model.
    *
    * @var array
    */
    public $fillable = ['type', 'message', 'from', 'to', 'url', 'has_read', 'extra'];


    /**
    * Many to One relationship with user
    *
    * @return \Illuminate\Database\Eloquent\BelongsTo
    */
    public function fromUser()
    {
        return $this->belongsTo('App\Models\User', 'from')
                    ->select('_id', 'name', 'avatar');
    }

}