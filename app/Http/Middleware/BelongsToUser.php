<?php
namespace App\Http\Middleware;

use Closure;
use JWTAuth;

class BelongsToUserMiddleware 
{

    /**
    * Handles request that belong to user.
    *
    * @param \Illuminate\Http\Request $request
    * @param \Closure $next
    * @return mixed
    */
    public function handle($request, Closure $next) 
    {
        $json = $request->all();
        $user = JWTAuth::parseToken()->authenticate();

        // check if model to update belongs to current user in session
        // if model is user check for _id to match with user_id in session
        // for other models check for user_id to match with user_id in session
        if(array_key_exists('_id', $json) && $json['_id'] === $user->_id ||
           array_key_exists('user_id', $json) && $json['user_id'] === $user->_id) {

            return $next($request);
        } else {
            return response(['message' => 'You dont have a permission to access/modify this resource.'], 401);
        }
    }
}