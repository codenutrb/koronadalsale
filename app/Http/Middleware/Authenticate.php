<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use Response;
use Redirect;
use Route;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $api = $this->callApi($request, '/api/auth/token', 'POST'); 

        switch ($api->getStatusCode()) {
            case 200:
                return $next($request);
            
            default:
                if($request->ajax()) {
                    return response(['errors' => $api->original], $api->getStatusCode());
                } else {
                    return redirect()->guest('login');
                }
        }

    }

     /** 
    * Call internal API
    *
    * @param string $url
    * @param string $method
    * @param Array $inputs
    * @param string $token
    * @return Response
    */
    protected function callApi($request, $url, $method, $inputs = []) 
    {
        // add auth_token on every request
        if($request->session()->has('auth_token')) {
            $inputs['auth_token'] = $request->session()->get('auth_token');
        } 

        $api_request = $request->create($url, $method, $inputs);
        $originalInput = $request->input();

        $request->replace($api_request->input());

        $response = Route::dispatch($api_request);

        $request->replace($originalInput);

        return $response;
    }
}
