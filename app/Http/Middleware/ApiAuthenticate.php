<?php namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiAuthenticate 
{
    /**
    * Handle the incoming request
    *
    * \Illuminate\Http\Request $request
    * Closure $next
    * @return mixed
    */
    public function handle(Request $request, Closure $next)
    {
        $token = $this->getToken($request);
        
        try {
            $user = JWTAuth::authenticate($token);
        } catch(TokenExpiredException $ex) {
            return response(['Session has expired.'], 401);
        } catch(TokenInvalidException $ex) {
            return response(['Invalid session.'], 401);
        } catch(JWTException $ex) {
            return response(['Token is required.'], 401);
        }

        return $next($request);
    }

    /**
    * Gets the token from request or input 
    *
    * @return string 
    */
    public function getToken(Request $request)  
    {
        if($request->has('auth_token')) {
            return $request->get('auth_token');
        } else {
            return $this->parseAuthHeader($request);
        }
    } 

    /**
     * Parse token from the authorization header.
     *
     * @param string $header
     * @param string $method
     *
     * @return false|string
     */
    protected function parseAuthHeader($request, $header = 'Authorization', $method = 'bearer')
    {
        $header = $request->headers->get($header);

        if (! starts_with(strtolower($header), $method)) {
            return false;
        }

        return trim(str_ireplace($method, '', $header));
    }

}