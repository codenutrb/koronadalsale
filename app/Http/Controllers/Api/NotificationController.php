<?php namespace App\Http\Controllers\Api;

use App\Repositories\NotificationRepository;
use App\Http\Controllers\Api\Controller; 
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
    * The NotificationRepository instance;
    *
    * @var \App\Repositories\NotificationRepository
    */
    protected $notification_repo;

    /**
    * Creates a new MessageController instance
    *
    * @param \App\Repositories\MessageRepository
    * @return void;
    */
    public function __construct(NotificationRepository $notification_repo) 
    {
        $this->notification_repo = $notification_repo;
    }

    /**
    * Get the notifications for user
    *
    * @param Request $request
    * @return Response
    */
    public function getNotifications(Request $request) 
    {
        return $this->notification_repo
                    ->getNotifications($this->getLoggedInUser($request)->_id);
    }

    /**
    * Mark notification as read
    *
    * @param Request $request
    * @param string $id
    * @return Response
    */
    public function getRead(Request $request, $id = null) 
    {
        return $this->notification_repo
                    ->update($id, $this->getLoggedInUser($request)->_id);
    }

}