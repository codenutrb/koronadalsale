<?php
namespace App\Http\Controllers\Api;

use JWTAuth;
use Validator;
use Log;
use App\Http\Controllers\Api\Controller;  
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PasswordController extends Controller
{

    /**
    * The auth token
    *
    * @var string
    */
    protected $token;

    /**
    * The user id
    *
    * @var string
    */
    protected $user;

    /**
    * Creates a new AuthController instance
    *
    * @param \App\Repositories\UserRepository $user_repo
    * @return void
    */
    public function __construct(UserRepository $user_repo)
    {
        $this->user_repo = $user_repo; 
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
        $validator = Validator::make($request->only('email'),  ['email' => 'required|email']);
        
        if($validator->fails()) {
            return response($validator->errors(), 422);
        } 

        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return response(['message' => trans($response)], 200);

            case Password::INVALID_USER:
                return response(['message' => trans($response)], 422);
        }
    }

     /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6'
        ]);
        
        if($validator->fails()) {
            return response($validator->errors(), 422);
        } 

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return response(['message' => trans($response), 'token' => $this->token, 'user' => $this->user], 200);

            default:
                return response(['email' => trans($response)], 422);
        }
    }

    /**
    * Change the user password
    *
    * @return \Illuminate\Http\Response
    */
    public function postChange(Request $request)
    {
        $inputs = $request->all();

        $user = $this->user_repo->getById($inputs['_id']);

        $condition = ['password' => 'required|confirmed|min:6'];

        if(!$user['requires_change_password']) {
            $condition['old_password'] = 'required';
        }

        $validator = Validator::make($inputs, $condition);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            if($user['requires_change_password'] || 
                Auth::validate([
                    '_id' => $inputs['_id'], 
                    'password' => $inputs['old_password']
                ])) {

                $this->resetPassword(Auth::user(), $inputs['password']);

                return response([
                    'message' => 'Password was updateed successfully.', 
                    'user' => $this->user
                ], 200); 

            } else {
                return response(['error' => 'Incorrect old password'], 422);
            }
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);
        $user->requires_change_password = false;
        
        $user->save();

        $this->token = JWTAuth::fromUser($user);

        $this->user = $user;
    }


    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
    }
}