<?php
namespace App\Http\Controllers\Api;

use Log;
use JWTAuth;
use Validator;
use App\Http\Controllers\Api\Controller; 
use Illuminate\Http\Request;
use App\Repositories\CommentRepository;
use App\Repositories\NotificationRepository;

class CommentController extends Controller
{
    /**
    * The CommentRepository instance.
    *
    * @var \App\Repositories\CommentRepository
    */
    protected $comment_repo;

    /**
    * The FollowRepository instance.
    *
    * @var App\Repositories\NotificationRepository
    */
    protected $notification_repo;

    /**
    * Creates a new CommentController instance.
    *
    * @param \App\Repositories\CommentRepository $comment_repo
    * @return void
    */
    public function __construct(CommentRepository $comment_repo,  NotificationRepository $notification_repo) 
    {
        $this->comment_repo = $comment_repo;
        $this->notification_repo = $notification_repo;
    }

    /**
    * Creates a new comment.
    *
    * @param Request $request
    * @return Response
    */
    public function post(Request $request) 
    {
        $comment = $request->all();

        $validator = Validator::make($comment, [
            'body' => 'required',
            'user_id' => 'required|exists:mongodb.user,_id',
            'post_id' => 'required|exists:mongodb.post,_id'
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            $comment = $this->comment_repo->save($comment, 
                                                 $comment['post_id'], 
                                                 $comment['user_id']);

            $user = $this->getLoggedInUser($request);
            $this->notification_repo->save([
                'type' => 'post.comment',
                'message' => ' commented on your post',
                'extra' => str_limit($comment['body'], 20),
                'from' => $user->_id,
                'to' => $comment->post->user_id,
                'url' => '/post/' . $comment->post->title . '/' . $comment->post_id . '#comments',
                'fromUser' => $user->shallowUser()
            ]);

            return $comment;
        }
    }

    /**
    * Removes a comment
    *
    * @param Request $request
    * @param string $id
    * @return Response
    */
    public function delete(Request $request, $id) 
    {
        if($this->comment_repo->delete($id, JWTAuth::authenticate()->_id)) {
            return response(['message' => 'Comment successfully deleted'], 200);
        } else {
            return response(['message' => 'There was an error deleting the comment.'], 500);
        }
    }
}