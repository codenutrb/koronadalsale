<?php
namespace App\Http\Controllers\Api;

use Log;
use Validator;
use Redis;
use JWTAuth;
use Carbon\Carbon;
use App\Repositories\MessageRepository;
use App\Http\Controllers\Api\Controller; 
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
    * The MessageRepository instance;
    *
    * @var \App\Repositories\MessageRepository
    */
    protected $message_repo;

    /**
    * Creates a new MessageController instance
    *
    * @param \App\Repositories\MessageRepository
    * @return void
    */
    public function __construct(MessageRepository $message_repo) 
    {
        $this->message_repo = $message_repo;
    }

    /**
    * Saves a message.
    *
    * @param Request $request
    * @return Response
    */
    public function post(Request $request) 
    {
        $message = $request->all(); 

        $validator = Validator::make($message, [
            'body' => 'required',
            'to_user_id' => 'required|exists:mongodb.user,_id' 
        ]);


        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            $user = $this->getLoggedInUser($request);
            
            if(($message = $this->message_repo->save($message, $user->_id))) {
                Log::debug('done sending..' . $message['body']);

                $conversation1 = $this->message_repo
                                      ->getConversationWithUser($user->_id, $message['to_user_id'], true)
                                      ->toArray();

                $conversation2 = $this->message_repo
                                      ->getConversationWithUser($message['to_user_id'], $user->_id, true)
                                      ->toArray();

                $conversation1 = $this->setConversationUser($conversation1, $user->_id);
                $conversation2 = $this->setConversationUser($conversation2, $message['to_user_id']);

                $this->broadcastMessage($conversation1, $user->_id);
                $this->broadcastMessage($conversation2, $message['to_user_id']);

                Log::debug('broadcasting..');
                return response(['message' => 'Message successfully sent'], 200);
            } else {
                return response(['error' => 'There was an error sending message'], 500);
            }
        }
    }

    /**
    * Get messages with other user
    *
    * @param Request $request
    * @param string $with
    * @param integer $skip
    * @return Response
    */
    public function getMessages(Request $request, $with, $skip) 
    {
        $validator = Validator::make(['with' => $with], [
            'with' => 'required|exists:mongodb.user,_id' 
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            $user = $this->getLoggedInUser($request);

            return $this->message_repo
                        ->getMessages($user->_id, $with, $skip)
                        ->toArray();
        }
    }

    /*
    * Publish message on redis
    *
    * @param string $message
    * @param string $userId
    */
    protected function broadcastMessage($message, $userId) 
    {
        Redis::publish('messages:' . $userId, json_encode($message));
    }

    /**
    * Get the messages for user
    * 
    * @param Request $request
    * @param string $with
    * @param string $received
    * @return Response
    */
    public function getConversations(Request $request, $type ='all', $with = null) 
    {

        $validator = Validator::make(['with user' => $with], [
            'with' => 'exists:mongodb.user,_id' 
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            $user = $this->getLoggedInUser($request);

            if($type === 'all') {
                $conversations = $this->message_repo
                                      ->getConversations($user->_id)
                                      ->toArray();

            } else if($type == 'received') {

                $conversations = $this->message_repo
                                      ->getConversations($user->_id)
                                      ->toArray();

            }

            $conversations = array_map(function($conversation) use($user) {
                return $this->setConversationUser($conversation, $user->_id);
            }, $conversations);

            $conversation = null;

            if($with != null && $with !== $user->_id) {
                if(array_key_exists($with, $conversations)) {
                    $conversation = $conversations[$with];
                } else {
                    $conversation = $this->message_repo
                                         ->getConversationWithUser($user->_id, $with, true)
                                         ->toArray();

                    $conversation = $this->setConversationUser($conversation, $user->_id);

                    $conversations[$conversation['_id']] = $conversation;
                }
            } else if(count($conversations) > 0) {
                $conversation = array_values($conversations)[0];
            }

            return [
                'conversations' => $conversations,
                'conversation' => $conversation,
                'me' => $user->shallowUser()
            ];

        }

    }

    /**
    * Set user in conversation
    * 
    * @param Array $conversation
    * @param string $userId
    * @return Array
    */
    protected function setConversationUser($conversation, $userId)
    {
        $_conversation = [
            '_id' => $conversation['_id'],
            'messages' => $conversation['active_messages']->toArray(),
            'user' => $userId == $conversation['user_id_1'] ? 
                            $conversation['user2'] : $conversation['user1'],
        ];

        if($userId === $conversation['user_id_1'] && isset($conversation['user1_last_received'])) {
            $_conversation['last_received'] = $conversation['user1_last_received'];
            $_conversation['has_received'] = true;
        } else if($userId === $conversation['user_id_2'] && isset($conversation['user2_last_received'])) {
            $_conversation['last_received'] = $conversation['user2_last_received'];
            $_conversation['has_received'] = true;
        } 

        if(count($_conversation['messages']) > 0) {
            $_conversation['last_activity'] = (string) $conversation['updated_at'];
        } else {
            $_conversation['last_activity'] = (string) Carbon::now();
        }


        return $_conversation;
    }

    /**
    * Get conversation with user
    *
    * @param Request $request
    * @param string $with
    * @return Response
    */
    public function getConversation(Request $request, $with) 
    {
        $validator = Validator::make(['with' => $with], [
            'with' => 'required|exists:mongodb.user,_id' 
        ]);


         if($validator->fails()) {
            return response($validator->errors(), 422);
         } else {
            $user = $this->getLoggedInUser($request);

            $conversation = $this->message_repo
                                 ->getConversationWithUser($user->_id, $with, true)
                                 ->toArray();

            return $this->setConversationUser($conversation, $user->_id);
         }
    }

    /**
    * Mark the messages as read
    *
    * @param Request $request
    * @param string $conversationId
    * @return Response
    */
    public function markAsRead(Request $request, $with) 
    {
        $validator = Validator::make(['with' => $with], [
            'with' => 'required|exists:mongodb.user,_id' 
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            $user = $this->getLoggedInUser($request);


            $conversation = $this->message_repo
                                 ->markAsRead($user->_id, $with)
                                 ->toArray();

            return $this->setConversationUser($conversation, $user->_id);
        }
    }

    /**
    * Deletes a message.
    *
    * @param Rquest $request
    * @param string $id
    * @return Response
    */
    public function delete(Request $request, $with) 
    {
        $validator = Validator::make(['with' => $with], [
            'with' => 'required|exists:mongodb.user,_id' 
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            $user = $this->getLoggedInUser($request);

            if($this->message_repo->deleteConversation($user->_id, $with)) {
                return response(['message' => 'Message successfully deleted'], 200);
            } else {
                return response(['message' => 'There was an error deleting the message'], 500);
            }
        }
    } 
}