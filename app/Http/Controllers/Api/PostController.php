<?php
namespace App\Http\Controllers\Api;

use Log;
use File;
use Validator;
use JWTAuth;
use Storage;
use App\Http\Controllers\Api\Controller;  
use Illuminate\Http\Request;
use App\Repositories\NotificationRepository;
use App\Repositories\PostRepository;

class PostController extends Controller
{
    /**
    * The PostRepository instance
    * 
    * @var App\Repositories\PostRepository 
    */
    protected $post_repo;

    /**
    * The FollowRepository instance.
    *
    * @var App\Repositories\NotificationRepository
    */
    protected $notification_repo;

    /**
    * Creates a new controller instance
    *
    * @param PostRepository $post_repo 
    * @return void
    */
    public function __construct(PostRepository $post_repo, NotificationRepository $notification_repo) 
    {
        $this->post_repo = $post_repo;
        $this->notification_repo = $notification_repo;
    }

    /**
    * Create a new post
    *
    * @param Request @request
    * @return Response
    */
    public function post(Request $request) 
    {
        Log::debug('Adding new post');
        $post = $request->all();

        $validator = Validator::make($post, [
            'title' => 'required|min:4',
            'price' => 'required|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]{2})?$/',
            'category' => 'required',
            'location.name' => 'required'
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            $user = $this->getLoggedInUser($request);
            Log::debug('user ' . $user);
            // move the images from tmp directory
            $post['photos'] = $this->moveImages($post['photos']);

            return $this->post_repo->save($post, $user->_id);
        }
    }

    /**
    * Update a post
    *
    * @param Request $rquest
    * @return Response
    */
    public function put(Request $request) 
    {
        $post = $request->all();

        $validator = Validator::make($post, [
            'title' => 'required|min:4',
            'price' => 'required|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]{2})?$/',
            'category' => 'required',
            'location.name' => 'required'
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            // move the images from tmp directory
            $post['photos'] = $this->moveImages($post['photos']);

            if(isset($post['oldPhotos'])) {
                $this->removePhotos($post['oldPhotos'], $post['photos']);
            }

            return $this->post_repo->update($post, JWTAuth::authenticate()->_id);
        }
    }

    /**
    * Retrieve the post by $id
    *
    * @param Request $request
    * @param string $id
    * @return Response
    */
    public function get(Request $request, $id) 
    {
        $post = $this->post_repo->get($id);

        return $post;
    }

    /**
    * Retrieve posts
    *
    * @param Request $request
    * @param string $searchText
    * @return Response
    */
    public function getPosts(Request $request, $searchText = null) 
    {
        $inputs = $request->all();
        $where = [];
        
        if($request->has('where')) {
            $where = $request->get('where');
        }

        if($searchText) {
            $where['$text'] = ['$search' => $searchText];
            $inputs['searchText'] = $searchText;
        } else {
            $inputs['searchText'] = '';
        }


        $page = 0;
        if($request->has('p')) {
            $page = $request->get('p') - 1;
        }

        $limit = 12;
        if($request->has('limit')) {
            $limit = $request->get('limit');
        }

        if($inputs['category'] != 'all') {
            $where['category'] = $inputs['category'];
        }

        return ($this->post_repo->getByCondition($where, $page, $limit) +  $inputs); 
    }

    /**
    * Retrieve latest posts
    *
    * @param Request $request
    * @param Response
    */
    public function getLatestPosts(Request $request) 
    {
        return $this->post_repo
                    ->getLatest();
    }

    /**
    * Like a post
    *
    * @param \Illuminate\Http\Request $request
    * @return Response
    */
    public function postLike(Request $request)
    {
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            '_id' => 'required|exists:post'
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            $user = JWTAuth::authenticate();
            $post = $this->post_repo->like($inputs, $user->_id);

            if($post['like']) {
                $this->notification_repo->save([
                    'type' => 'post.like',
                    'message' => ' liked your post',
                    'from' => $user->_id,
                    'to' => $post['post']['user_id'],
                    'url' => '/post/' . $post['post']['_id'] . '/' . str_slug($post['post']['title']),
                    'fromUser' => $user->shallowUser()
                ]);
            }

            return response(['like' => $post], 200);
        }
    }

    /**
    * Deletes a post
    *
    * @param Request $request
    * @param string $id
    * @return Response
    */
    public function delete(Request $request, $id) 
    {
        if($this->post_repo->delete($id, JWTAuth::authenticate()->_id)) {
            return response(['message' => 'Post successfully deleted.'], 200);
        } else {
            return response(['message' => 'There was an error deleting the post.'], 500);
        }
    }

    /**
    * Move the post images from tmp to permanent directory
    *
    * @param $photos
    * @return Array
    */
    protected function moveImages($photos) 
    {
        $new_photos = [];

        foreach($photos as $key => $photo) {
            $new_photo = [];
            foreach($photo as $type => $path) {
                //check if from tmp dir
                if(starts_with($path, '/img/tmp')) {
                    $new_photo[$type] = $this->moveImage($path);
                } else {
                    $new_photo[$type] = $path;
                }
            }
            $new_photos[] = $new_photo; 
        }

        return $new_photos;
    }

    /**
    * Move an image from old to nested directory
    *
    * @param $path
    * @return string
    */
    protected function moveImage($path) 
    {
        $filename = basename($path);

        $newPath = $this->createNestedDirectory($filename) . $filename;

        if(env('STORAGE', 'local') === 'local') {
            if(file_exists(public_path() . '/' . $newPath)) {
                return $newPath;
            } else if(file_exists(public_path() . '/' . $path)) {
                File::move(public_path() . '/' . $path, public_path() . '/' . $newPath);

                return '/' . $newPath;
            } 
        } else {
            Log::debug($path);
            $image = Storage::disk(env('STORAGE', 'local'))->put($newPath, 
                        file_get_contents(public_path() . '/' . $path),
                        'public');

            Log::debug($this->getS3StorageUrl($newPath));
            return $this->getS3StorageUrl($newPath);
        }
    }

    /**
    * Create a nested directory from filename
    *
    * @param $filename
    * @return string
    */
    protected function createNestedDirectory($filename)
    {
        $currentDirectory = 'img/posts/';

        for($i = 0; $i < 3; $i++) {
            $currentDirectory = $currentDirectory . substr($filename, $i * 2, 2) . '/';

            if(env('STORAGE', 'local') === 'local' && !file_exists(public_path() . '/' . $currentDirectory)) {
                mkdir(public_path() . '/' . $currentDirectory);
            }
        }

        Log::debug('New directory: ' . $currentDirectory);
        return $currentDirectory;
    }

    /**
    * Remove deleted post images
    *
    * @param Array oldPhotos
    * @param Array newPhotos
    * @return void
    */
    protected function removePhotos($oldPhotos, $newPhotos) 
    {
        $willRemove = array_filter($oldPhotos, function($v, $k) use($newPhotos) {
            return !in_array($v, $newPhotos);
        }, ARRAY_FILTER_USE_BOTH);

        $removeFromStorage = [];
        foreach($willRemove as $key => $remove) {
            foreach($remove as $key => $photo) {
                if(env('STORAGE', 'local') == 'local') {
                    if(file_exists(public_path() . '/' . $photo) && 
                        !is_dir(public_path() . '/' . $photo)) {

                        unlink(public_path() . '/' . $photo);
                    }
                } else {
                    //remove s3 url from photo url
                    $path = str_replace($this->getS3Url(), '', $photo);
                    if(Storage::disk(env('STORAGE'))->has($path)) {
                        $removeFromStorage[] = $path;
                    }
                }
            }
        }

        if(count($removeFromStorage) > 0) {
            Log::debug('To be removed from storage: ' . json_encode($removeFromStorage));
            Storage::disk(env('STORAGE'))->delete($removeFromStorage);
        }
    }

}
