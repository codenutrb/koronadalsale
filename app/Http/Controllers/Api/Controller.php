<?php

namespace App\Http\Controllers\Api;

use Log;
use JWTAuth;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
    * Gets the token from request or input 
    *
    * @return string 
    */
    public function getToken(Request $request)  
    {
        if($request->has('auth_token')) {
            //Log::debug($request->get('auth_token'));
            return $request->get('auth_token');
        } else {
            return $this->parseAuthHeader($request);
        }
    } 

    /**
    * Get url of s3 bucket
    *
    * @return string
    */
    protected function getS3StorageUrl($file) {
        return Storage::disk('s3')->getDriver()
                                  ->getAdapter()
                                  ->getClient()
                                  ->getObjectUrl(env('AWS_STORAGE_BUCKET'), $file);
    }

    /**
    * Get url of s3 bucket
    *
    * @return string
    */
    protected function getS3Url() {
        $url =  Storage::disk('s3')->getDriver()
                                   ->getAdapter()
                                   ->getClient()
                                   ->getObjectUrl(env('AWS_STORAGE_BUCKET'), '/img');
        return str_replace('/img', '', $url);
    }

    /**
     * Parse token from the authorization header.
     *
     * @param string $header
     * @param string $method
     *
     * @return false|string
     */
    protected function parseAuthHeader($request, $header = 'Authorization', $method = 'bearer')
    {
        $header = $request->headers->get($header);

        if (! starts_with(strtolower($header), $method)) {
            return false;
        }

        return trim(str_ireplace($method, '', $header));
    }

    public function getLoggedInUser(Request $request) {
        $token = $this->getToken($request);
        
        try {
            return JWTAuth::authenticate($token);
        } catch(TokenExpiredException $ex) {
        } catch(TokenInvalidException $ex) {
        } catch(JWTException $ex) {
        }
    }
}
