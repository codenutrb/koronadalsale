<?php

namespace App\Http\Controllers\Api;

use Image;
use Log;
use Notifynder;
use JWTAuth;
use Validator;
use Mail;
use Route;
use Storage;
use Request as Rquest;
use App\Http\Controllers\Api\Controller; 
use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Repositories\FollowRepository;
use App\Repositories\NotificationRepository;

class UserController extends Controller
{
    const AVATAR_DIR = 'img/avatars/';

    /**
    * The UserRepository instance.
    *
    * @var App\Repositories\UserRepository
    */
    protected $user_repo;

    /**
    * The FollowRepository instance.
    *
    * @var App\Repositories\FollowRepository
    */
    protected $follow_repo;

    /**
    * The FollowRepository instance.
    *
    * @var App\Repositories\NotificationRepository
    */
    protected $notification_repo;

    /**
    * Create a new UserController instance.
    * 
    * @param App\Repositories\UserRepository $user_repo
    * @param App\Repositories\FollowRepository $follow_repo
    * @param App\Repositories\NotificationRepository $notification_repo
    * @return void
    */
    public function __construct(UserRepository $user_repo, 
                                FollowRepository $follow_repo, 
                                NotificationRepository $notification_repo) 
    {
        $this->user_repo = $user_repo;
        $this->follow_repo = $follow_repo;
        $this->notification_repo = $notification_repo;
    }

    /**
    * Retrieve the user by id.
    *
    * @param Request $request
    * @param string $id
    * @return Response
    */
    public function get(Request $request, $id)  
    {
        $return = ['user' => $this->getUserWithNetworks($id)];

        if($this->getToken($request)) {
            $return['me'] = $this->getUserWithNetworks($id, $this->getLoggedInUser($request));
        }

        return $return;
    }

    /**
    * Get logged in user
    *
    * @param Request $request
    * @return Response
    */
    public function getMe(Request $request) 
    {
        $user = $this->getLoggedInUser($request);

        return $this->getUserWithNetworks($user->_id, $user)
                    ->toArray();
    }

    /**
    * Get user with followers and following
    *
    * @param string $id
    * @param string $user
    * @return Response
    */
    protected function getUserWithNetworks($id, $user = null)
    {
        if($user === null) {
            $user = $this->user_repo->get($id);
        }

        $return = clone($user);

        $return['followers'] = $user->followers
                                    ->map(function($o) { return $o->_follower->shallowUser(); } )
                                    ->keyBy('_id');

        $return['following'] = $user->following
                                    ->map(function($o) { return $o->_followee->shallowUser(); } )
                                    ->keyBy('_id');

        return $return;
    }

    /**
    * Create a new user
    *
    * @param Request $request
    * @return Response
    */
    public function post(Request $request) 
    {
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'name' => 'required|max:64',
            'password' => 'required|min:6',
            'birthday' => 'date', 
            'email' => 'required|email|unique:' . config('database.default') . '.user',
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422); 
        } else {
            $inputs['avatar'] = $this->getGravatar($inputs['email']);
            $inputs['avatar_original'] = $this->getGravatar($inputs['email'], 300);

            $user = new User($inputs);
            $user = $this->user_repo->save($user);

            if($user) {
                Mail::queue('emails.verify', ['confirmation_code' => $user->confirmation_code], function($message) use($user) {
                    $message->to($user->email, $user->name)->subject('Email Verification');
                    Log::info('Sending email verification.');
                });

                return response(['user' => $user, 'token' => JWTAuth::fromUser($user)]);   
            } else {
                return response(['message' => 'There was an error saving the user.'], 500);
            }

        }
    }

    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $email The email address
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     * @param boole $img True to return a complete IMG tag False for just the URL
     * @param array $atts Optional, additional key/value attributes to include in the IMG tag
     * @return String containing either just a URL or a complete image tag
     * @source http://gravatar.com/site/implement/images/php/
    */
    function getGravatar( $email, $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array() ) {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

    /**
    * Update the user info.
    *
    * @param Request @request
    * @return Response
    */
    public function put(Request $request) 
    {
        $inputs = $request->all();
        $validator = Validator::make($inputs, [
            'name' => 'required|max:64',
            'birthday' => 'date',
            'email' => 'required|email|unique:' . config('database.default') . '.user,email,' . $inputs['_id'] . ',_id',
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            return $this->user_repo->update($inputs, JWTAuth::authenticate($this->getToken($request))->_id);
        }
    }

    /** 
    * Update the use avatar 
    * 
    * @param Request $request
    * @return Response
    */
    public function putAvatar(Request $request) 
    {
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'avatar' => 'required',
            'avatar_original' => 'required',
            '_id' => 'required|exists:' . config('database.default') . '.user,_id'
        ]);

        $userId = JWTAuth::authenticate($this->getToken($request))->_id;

        $inputs['avatar'] = $this->createImage($inputs['avatar'], 'avatar.png', $userId);
        $inputs['avatar_original'] = $this->createImage($inputs['avatar_original'], 'avatar_original.png', $userId);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            return $this->user_repo->update($inputs, $userId);
        }
    }

    /**
    * Verify the email address
    *
    * @param Request $request
    * @param string $confirmation_code
    * @return Response
    */
    public function getVerify(Request $request, $confirmation_code = null)
    {

        $validator = Validator::make(['confirmation_code' => $confirmation_code], [
            'confirmation_code' => 'required|exists:' . config('database.default') . '.user,confirmation_code'
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            $user = $this->user_repo
                         ->getUserByConfirationCode($confirmation_code);

            $user->fill(['is_verified' => true]);
            $user->save();

            return response(['message' => 'Email has been verified.', 'token' => JWTAuth::fromUser($user)], 200);
        }
    }

    /**
    * Follow user 
    *
    * @param Request $request;
    * @param $follower
    * @return Response
    */
    public function getFollow(Request $request, $followee) 
    {
        $follower = JWTAuth::authenticate($this->getToken($request));

        $validator = Validator::make(
            ['followee' => $followee],
            ['followee' => 'required|exists:' . config('database.default') . '.user,_id']
        );

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            if($followee == $follower->_id) {
                return response(['errors' => ['You cannot follow yourself'] ], 422);
            } else {

                // 0 if unfollow user
                // 1 if follow user
                $status = $this->follow_repo->save($follower->_id, $followee);

                if($status['status'] === 1) {
                    $this->notification_repo->save([
                        'type' => 'user.following',
                        'message' => ' started following you',
                        'from' => $follower->_id,
                        'to' => $followee,
                        'url' => '/user/' . $followee . '/followers',
                        'fromUser' => $follower->shallowUser()
                    ]);
                }

                return response([
                    'message' => 'You have added another user in your network',
                    'data' => $status
                ], 200);
            }
        }

    }

    /**
    * Search user base on name
    *
    * @param Request $request
    * @param string $name
    * @return Response
    */
    public function searchUser(Request $request, $name) 
    {
        return $this->user_repo->searchUser($name, $this->getLoggedInUser($request)->_id);
    }

    /**
    * Create an avatar
    *
    * @param string $base64Image
    */
    protected function createImage($base64Image, $filename, $userId) {
        $path = self::AVATAR_DIR . $userId . '/';
        $fullPath = public_path() . '/' . $path;

        Log::debug($fullPath . ' ' . $filename);
        if(!file_exists($fullPath)) {
            mkdir($fullPath);
        }

        Image::make($base64Image)->save($fullPath . $filename);

        if(env('STORAGE', 'local') == 's3') {
             $image = Storage::disk(env('STORAGE', 'local'))->put($path . $filename,
                            file_get_contents($fullPath . $filename),
                            'public');

             return $this->getS3StorageUrl($path . $filename);
        } else {
            return $path . $filename;
        }
    }
}
