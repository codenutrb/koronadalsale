<?php
namespace App\Http\Controllers\Api;

use Auth;
use Log;
use JWTAuth;  
use Socialite;
use Validator;
use App\Http\Controllers\Api\Controller; 
use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\UserRepository;
use Facebook\Facebook;

class AuthController extends Controller
{
    /** 
    * The UserRepository instance
    *
    * @var \App\Repositories\UserRepository $user_repo
    */
    protected $user_repo;

    /**
    * Creates a new AuthController instance
    *
    * @param \App\Repositories\UserRepository $user_repo
    * @return void
    */
    public function __construct(UserRepository $user_repo)
    {
        $this->user_repo = $user_repo; 
    }

    /**
    * Handle the login request of the application 
    *
    * @param Request $request
    * @return Response
    */
    public function post(Request $request) 
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password
        ];

        $validator = Validator::make($login, [
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else {
            if($token = JWTAuth::attempt($login)) {
                $user = JWTAuth::authenticate($token);
                return response(['message' => 'Login successful', 'token' => $token, 'user' => $user]);
            } else {
                return response(['message' => 'Wrong username and password combination'], 401);
            }
        }
    }

    /**
    * Facebook login authentication
    * 
    * @param \Illuminate\Http\Request
    * @return
    */
    public function fbLogin(Request $request)
    {
        $fb_user = $request->all();
        //check if a valid facebook user
        //then find the user using the fb_id
        //add a new user if it doesnt exist
        if(array_key_exists('token', $fb_user) && $this->getFbUser($fb_user['token'])) {
            $user = $this->user_repo
                         ->getByCondition(['fb_id' => $fb_user['fb_id']])['results']
                         ->first();

            $fb_user['avatar'] = str_replace('$fb_id', $fb_user['fb_id'], env('FB_IMAGE_URL')) . '80';
            $fb_user['avatar_original'] = str_replace('$fb_id', $fb_user['fb_id'], env('FB_IMAGE_URL')) . '300';

            Log::debug($fb_user);

            if($user === null) {
                $inputs = [
                    'email' => $fb_user['email'],
                    'name' => $fb_user['name'],
                    'password' => str_random(12),
                    'gender' => $fb_user['gender'],
                    'avatar' => $fb_user['avatar'],
                    'avatar_original' => $fb_user['avatar_original'],
                    'requires_change_password' => true,
                    'fb_id' => $fb_user['fb_id']
                ];

                $user = new User($inputs);
            } else {
                $user->fill($fb_user);
            }

            $user->is_verified = true;
            $user = $this->user_repo->save($user);
            $token = JWTAuth::fromUser($user);

            return response(['message' => 'Login successful', 'token' => $token, 'user' => $user], 200);
        } else {
            return response(['errors' => ['message' => 'Error logging in.'], 500]);
        }
    }

    /**
    * Check for user authentication with token
    *
    * @param Request $request
    * @return Response
    */
    public function authToken(Request $request)
    {
        return response(['message' => 'Authenticated user'], 200);        
    }

    /**
    * Get facebook user with token
    *
    * @param string @token 
    * @return GraphUser
    */
    protected function getFbUser($token) 
    {
        $fb = new Facebook([
            'app_id' => config('services.facebook.client_id'),
            'app_secret' => config('services.facebook.client_secret'),
            'default_graph_version' =>  'v2.5'
        ]); 

        try {
            $response = $fb->get('/me', $token);
            $user = $response->getGraphUser();
            
            Log::debug($user);
            return $response->getGraphUser();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            Log::error('Graph returned an error: ' . $e->getMessage()); 
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            Log::error('Facebook SDK returned an error: ' . $e->getMessage());
        } catch(Exception $e) {

        }
    }


}
