<?php namespace App\Http\Controllers;

use Request;

class CommentController extends Controller
{
    /**
    * Add a comment
    *
    * @return Response
    */
    public function post()
    {
        $inputs = Request::all(); 

        $api = $this->callApi('/api/comment', 'POST', $inputs);

        switch ($api->getStatusCode()) {
            case 200:
                $post = $this->callApi('/api/post/' . $api->original['post_id'], 'GET');
                return response($this->comments($post->original), 200);
            
            default:
                return response($api->original, $api->getStatusCode());
        }
    }

    /**
    * Get comments from post
    *
    * @param \App\Models\Post
    * @return array
    */
    public function comments($post) 
    {
        $comments = [];

        foreach($post['comments'] as $key => $comment) {
            $comments[$key] = $comment->toArray();
            $comments[$key]['user'] = $comment->shallowUser(['_id', 'name', 'avatar']);
        }

        return $comments;
    }
}