<?php namespace App\Http\Controllers;

class NotificationController extends Controller
{
    /**
    * Get notifications for user
    *
    * @return Response
    */
    public function getNotifications() 
    {
        return $this->callApi('/api/notifications');
    }

    /**
    * Mark notification as read 
    * 
    * @param string $id
    * @return Response
    */
    public function getRead($id = null)
    {
        return $this->callApi('/api/notification/read/' . $id, 'GET');
    }
}