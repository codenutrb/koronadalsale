<?php

namespace App\Http\Controllers;

use Request;
use Route;
use Log;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** 
    * Call internal API
    *
    * @param string $url
    * @param string $method
    * @param Array $inputs
    * @param string $token
    * @return Response
    */
    protected function callApi($url, $method = 'GET', $inputs = []) 
    {
        // add auth_token on every request
        if(Request::session()->has('auth_token')) {
            $inputs['auth_token'] = Request::session()->get('auth_token');
        }

        $api_request = Request::create($url, $method, $inputs);
        $originalInput = Request::input();

        Request::replace($api_request->input());

        $response = Route::dispatch($api_request);

        Request::replace($originalInput);

        return $response;
    }

    /**
    * Set the session variables
    *
    * @param string $token
    * @param \App\Model\User $user
    * @return void
    */
    protected function startSession($token, $user) 
    {
        Log::debug($token);
        Request::session()->put('auth_token', $token);  
        Request::session()->put('user', $user);  
    }


    /**
    * Retrieve posts according to query
    *
    * @param Array $inputs
    * @param string $searchText
    * @return Response
    */
    public function retrievePosts($inputs = [], $searchText = null, $category='all') 
    {
        if(!isset($inputs['p'])) {
            $inputs['p'] = 1;
        }

        if(!isset($inputs['limit'])) {
            $inputs['limit'] = 12;
        }

        $inputs['category'] = $category;
        
        $api = $this->callApi('/api/posts/' . $searchText, 'GET', $inputs);

        return $api->original;
    }

}
