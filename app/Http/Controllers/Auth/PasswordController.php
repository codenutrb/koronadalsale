<?php

namespace App\Http\Controllers\Auth;

use Log;
use Request;
use JWTAuth;
use App\Http\Controllers\Controller;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /** 
    * Get the email password page
    *
    * @return Response
    */
    public function getEmail()
    {
        return view('auth.email');
    } 

    /**
    * Create reset password
    *
    * @return Response
    */
    public function postEmail()
    {
        $inputs = Request::only('email');

        $api = $this->callApi('api/password/email', 'POST', $inputs);

        switch($api->getStatusCode()) {
            case 200:
                return back()->with('message', $api->original['message']);

            default:
                return back()->withInput($inputs)->withErrors($api->original);
        }
    }

    /**
    * Get the reset password page
    * @param string $token
    *
    * @return response
    */
    public function getReset($token = null) 
    {
        return view('auth.reset')->with('token', $token);
    }

    /**
    * Reset the password
    *
    * @return Response
    */
    public function postReset()
    {
        $inputs = Request::all();

        $api = $this->callApi('api/password/reset', 'POST', $inputs);

        switch($api->getStatusCode()) {
            case 200:
                $this->startSession($api->original['token'], $api->original['user']);
                return redirect()->route('index');

            default:
                return redirect()->back()->withInput($inputs)->withErrors($api->original);
        }
    }

    /**
    * Get the change password page
    *
    * @return Response
    */
    public function getChange()
    {
        $user = JWTAuth::authenticate(Request::session()->get('auth_token')); 
        Request::session()->put('profile-active-menu', 'profile');
        return view('auth.change', ['user' => $user]);
    }

    /**
    * Change user password
    *
    * @return Response
    */
    public function postChange()
    {
        $inputs = Request::all();

        $api = $this->callApi('api/password/change', 'POST', $inputs);

        switch($api->getStatusCode()) {
            case 200:
                Request::session()->put('user', $api->original['user']);

                return redirect()->route('user.profile', ['id' => $inputs['_id']])->with('message', $api->original['message']);

             case 401:
                return redirect()->route('login')->withErrors($api->original);
                
            default:
                return redirect()->back()->withErrors($api->original);
        }
    }
}
