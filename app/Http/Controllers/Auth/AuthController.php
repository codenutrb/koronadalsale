<?php

namespace App\Http\Controllers\Auth;

use Socialite;
use Validator;
use Request;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Route;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
    * Get login page
    *
    * @return Response
    */
    protected function login() 
    {
        $api = $this->callApi('/api/auth/token', 'POST'); 

        switch ($api->getStatusCode()) {
            case 200:
                return redirect()->route('index');
            
            default:
                Request::session()->put('active_header', 'login');

                return view('auth.login');
        }
    }

    /**
    * Facebook login authentication
    *
    * @return Response
    */
    public function fbLogin() 
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
    * Facebook login callback
    *
    * @return Response
    */
    public function fbLoginCallback()
    {
        Request::session()->flush();

        $fb_user = Socialite::driver('facebook')->stateless()->user();
        
        $inputs = [
            'fb_id' => $fb_user->id,
            'name' => $fb_user->name,
            'email' => $fb_user->email,
            'token' => $fb_user->token,
            'gender' => $fb_user->user['gender']
        ];

        $api = $this->callApi('/api/auth/fb_login', 'POST', $inputs);

        switch($api->getStatusCode()) {

            case 200:
                $this->startSession($api->original['token'], $api->original['user']);
                return redirect()->intended('/');

            default: 
                return redirect()->back()->withErrors($api->original);
        }
    }

    /** 
    * Login user
    *
    * @return Response
    */
    protected function post()
    {
        $inputs = Request::all();

        $api = $this->callApi('/api/auth', 'POST', $inputs);

        switch($api->getStatusCode()) {

            case 200:
                $this->startSession($api->original['token'], $api->original['user']);
                return redirect()->intended('/');

            default:
                return back()->withInput(Request::except('password'))->withErrors($api->original);
        }
    }

    /**
    * Logout user
    *
    * @return Response
    */
    protected function logout(Request $request) 
    {
        Request::session()->flush();

        return redirect()->route('login');
    }
}
