<?php namespace App\Http\Controllers;

use Log;
use Mail;
use Request;
use Validator;
use GuzzleHttp\Client;

class HomeController extends Controller
{

    /** 
    * Get contact us page
    *
    * @return Response
    */
    public function getContactUs()
    {
        return view('contact-us');
    }

    /**
    * Get terms and conditions page
    *
    * @return Response
    */
    public function getTermsAndConditions()
    {
        return view('terms-and-conditions');
    }


    /**
    * Process contact us message
    *
    * @return Response
    */
    public function postContactUs() 
    {
        $inputs = Request::all();

        $validator = Validator::make($inputs, [
            'name' => 'required',
            'email' => 'email|required',
            'body' => 'required'
        ]);

        $success = true;

        if(!isset($inputs['g-recaptcha-response']) || 
           !$this->validateCaptcha($inputs['g-recaptcha-response'])) {

            $validator->getMessageBag()->add('captcha', 'Please verify that you are not a robot.');

            $success = false;
        }

        if(!$success || $validator->fails()) {
            return back()->withErrors($validator->errors())
                         ->withInput($inputs);
        } else {
            Mail::queue('emails.contact-us', $inputs, function($_message) use($inputs) {
                $_message->from($inputs['email'], $inputs['name'])
                         ->to(env('APP_EMAIL', 'info@koronadalsale.com'), 'Koronadal Sale')
                         ->subject('Message from ' . $inputs['name']);
            });

            return back()
                   ->with('message', 'Your message has been successfully sent to our team.');
        }
    }

    /**
    * Verify recaptcha with google
    *
    * @param @string gRecaptcha
    * @return Boolean
    */
    protected function validateCaptcha($gRecaptcha) 
    {
        $client = new Client();

        $res = $client->request('POST', env('RECAPTCHA_URL'), [
            'form_params' => [
                        'secret' => env('RECAPTCHA_SECRET'),
                        'response' => $gRecaptcha
                      ]
        ]);

        $body = json_decode((string) $res->getBody(), true);

        return $body['success'];
    }

}