<?php namespace App\Http\Controllers;

use Request;
use Storage;
use File;
use Log;

class PostController extends Controller
{

    protected $categories = [
        '' => 'Please select',
        'cars' => 'Cars',
        'motorcycles' => 'Motorcycles',
        'phones' => 'Phones',
        'clothes' => 'Clothes',
        'gadgets' => 'Gadgets',
        'real-estate' => 'Real Estate',
        'foods' => 'Foods',
        'services' => 'Services',
        'pets' => 'Pets',
        'others' => 'Others'
    ];

    protected $categoryIcons = [
        'cars' => 'car',
        'motorcycles' => 'motorcycle',
        'phones' => 'mobile-phone',
        'clothes' => 'black-tie',
        'gadgets' => 'headphones',
        'real-estate' => 'home',
        'foods' => 'cutlery',
        'services' => 'briefcase',
        'pets' => 'paw',
        'others' => 'recycle'
    ];


    public function getIndex() 
    {
        Request::session()->remove('active_header');

        $posts = $this->callApi('/api/posts/latest')
                      ->original;

        Log::debug($posts);
        return view('index', [
            'posts' => $posts,
            'categories' => array_slice($this->categories, 1),
            'categoryIcons' => $this->categoryIcons
        ]);
    }

    /**
    * Get the post page
    * 
    * @param string $id
    * @return Response
    */
    public function get($id, $title)
    {
        $api = $this->callApi('/api/post/' . $id, 'GET');

        switch ($api->getStatusCode()) {
            case 200:
                return view('posts.post', ['post' => $api->original]);
            
            default:
                return response($api->original, $response->getStatusCode());
        }
    }

    /**
    * Get posts
    *
    * @return Response
    */
    public function getPosts()
    {
        Request::session()->put('active_header', 'all_posts');

        return view('posts.posts', $this->retrievePosts(Request::all()) + ['categories' => $this->categories]); 
    }

    /**
    * Search posts
    *
    * @return Response
    */
    public function postSearch()
    {
        $inputs = Request::all();

        $category = 'all';

        if(isset($inputs['category'])) {
            $category = $inputs['category'];
        }

        return redirect()->route('post.search', [
            'searchText' => $inputs['searchText'], 
            'category' => $category,
            'p' => 1
        ]);
    }

    /**
    *  Search for posts
    *
    * @param $searchText
    * @return Response
    */
    public function getSearch($category = 'all', $searchText = null) 
    {
        $inputs = Request::all();
        
        Request::session()->put('active_header', 'all_posts');

        return view('posts.posts', $this->retrievePosts($inputs, $searchText, $category) + ['categories' => $this->categories]);
    }

    /**
    * Get new post page
    *
    * @return Response
    */
    public function getNew() 
    {
        Request::session()->put('active_header', 'post');

        return view('posts.new', ['categories' => $this->categories]);
    }

    /**
    * Get the edit post page
    *
    * @param string $id
    * @return Response
    */
    public function getEdit($id) 
    {
        $api = $this->callApi('/api/post/' . $id, 'GET');

        switch ($api->getStatusCode()) {
            case 200:
                return view('posts.edit', ['categories' => $this->categories, 'post' => $api->original]);            

            case 401:
                return redirect()->route('login')->withErrors($api->original);

            default:
                return response($api->original, 422);
        }
    }

    /**
    * Create a new post
    *
    * @return Response
    */
    public function postCreate()
    {
        $inputs = Request::all();

        $api = $this->callApi('api/post', 'POST', $inputs);

        switch ($api->getStatusCode()) {
            case 200:
                return response($api->original, 200);

            case 401:
                return response($api->original, $api->getStatusCode());

            default:
                return response($api->original, $api->getStatusCode());
        }
    }


    /**
    * Update the post
    *
    * @return Response
    */
    public function postUpdate()
    {
        $inputs = Request::all();

        $api = $this->callApi('api/post', 'PUT', $inputs);

        switch ($api->getStatusCode()) {
            case 200:
                return response($api->original, 200);
            
            case 401:
                return response($api->original, $api->getStatusCode());

            default:
                return response($api->original, $api->getStatusCode());
        }
    }

    /** 
    * Like a post
    *
    * @return Response
    */
    public function postLike()
    {
        $inputs = Request::all();

        $api = $this->callApi('api/post/like', 'POST', $inputs);

        switch ($api->getStatusCode()) {
            case 200:
                return response($api->original['like'], $api->getStatusCode());            

            default:
                return response($api->original, $api->getStatusCode());            
        }
    }

}