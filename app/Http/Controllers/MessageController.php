<?php namespace App\Http\Controllers;

use Log;
use Request;

class MessageController extends Controller
{
    /**
    * Get the messages page
    *
    * @return Response
    */
    public function getConversations($type = 'all', $with = null) 
    {
        $api = $this->callApi('api/conversations/' . $type . '/' . $with);

        if($api->getStatusCode() === 200) {
            $data = $api->original;

            if(Request::ajax()) {
                return $data; 
            } else{
                $data['messaging_url'] = env('MESSAGING_URL');
                return view('messages.index', $data);
            }
        } else {
            return $api;
        }
    }

    /**
    * Get the message with user
    *
    * @param string $userId
    * @return Response
    */
    public function getConversation($with) 
    {
        return $this->callApi('api/conversation/' . $with);
    }

    /**
    * Send a message
    *
    * @return Response
    */
    public function postMessage()
    {
        $inputs = Request::all();

        return $this->callApi('/api/message/', 'POST', $inputs);
    }

    /**
    * Get messages with other user
    *
    * @param string $with
    * @param string $skip
    * @return Response
    */
    public function getMessages($with, $skip = 0)
    {
        return $this->callApi('/api/messages/' . $with . '/' . $skip);
    }

    /** 
    * Mark message as read
    *
    * @param string $with
    * @return Response
    */
    public function markAsRead($with) 
    {
        return $this->callApi('api/conversation/' . $with . '/read', 'PUT');
    }

    /**
    * Delete a conversation
    *
    * @param string $with
    * @return Response
    */
    public function delete($with)
    {
        return $this->callApi('api/conversation/' . $with, 'DELETE');
    }
}