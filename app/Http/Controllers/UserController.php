<?php
namespace App\Http\Controllers;

use Log;
use Request;
use App\Models\User;

class UserController extends Controller
{
    /**
    * Get User Profile
    *
    * @param string $id
    * @return Response
    */
    public function get($id) 
    {
        $api = $this->callApi('api/user/' . $id, 'GET');
        Request::session()->put('profile-active-menu', 'profile');
        return view('user.about', $api->original);
    }

    /**
    * Get the current user
    *
    * @return Response
    */
    public function getMe()
    {
        return $this->callApi('api/user/me', 'GET');
    }

    /**
    * Get notifications for user
    *
    * @return Response 
    */
    public function getNotifications() 
    {
        $id = Request::session()->get('user')['_id'];
        Request::session()->put('profile-active-menu', '');

        $api = $this->callApi('api/user/' . $id, 'GET');
        return view('user.notifications', $api->original); 
    }

    /**
    * Get the edit profile page
    *
    * @return Response
    */
    public function getEdit() 
    {
        $id = Request::session()->get('user')['_id'];

        $user = $this->callApi('api/user/' . $id, 'GET')->original;
        if(Request::old()) {
            $user->fill(Request::old());
        } 

        Request::session()->put('profile-active-menu', 'profile');
        return view('user.edit', ['user' => $user]['user']);
    }

    /** 
    * Update the user
    *
    * @return Response
    */
    public function postEdit() 
    {
        $inputs = Request::all();
        $api = $this->callApi('api/user', 'PUT', $inputs);

        switch($api->getStatusCode()) {
            case 200:
                Request::session()->put('user', $api->original);
                return redirect()->route('user.profile', [$api->original['_id']])->with('message', 'User information was updated successfully');

            case 401:
                return redirect()->route('login')->withErrors($api->original);
                
            default:
                return back()->withInput($inputs)->withErrors($api->original);
        }
    }

    /**
    * Get the register user page
    *
    * @return Response
    */
    public function getPost() 
    {
        Request::session()->put('active_header', 'register');

        return view('user.create'); 
    }

    /**
    * Create a new user
    *
    * @return Response
    */
    public function post() 
    {
       
        $inputs = Request::all();

        $api = $this->callApi('/api/user/', 'POST', $inputs);

        switch($api->getStatusCode()) {

            case 200:
                $this->startSession($api->original['token'], $api->original['user']);
                return redirect()->route('index');

            default:
                return back()->withInput($inputs)->withErrors($api->original);
        }
    }

    /**
    * Follow user 
    *
    * @param string $id
    * @return Response
    */
    public function getFollow($followee)
    {
        return $this->callApi('/api/user/follow/' . $followee, 'GET');
    }

    /**
    * Get the followers page
    *
    * @param string $page
    * @return Response
    */
    public function getFollowers($id) 
    {
        $api = $this->callApi('api/user/' . $id, 'GET');

        Request::session()->put('profile-active-menu', 'followers');

        return view('user.follows', $api->original);
    }

    /**
    * Get the followeings page
    *
    * @param string $page
    * @return Response
    */
    public function getFollowing($id) 
    {
        $api = $this->callApi('api/user/' . $id, 'GET');

        Request::session()->put('profile-active-menu', 'following');

        return view('user.follows', $api->original);
    }


    /**
    * Verify the email
    * 
    * @param string $confirmation_code
    * @return Response
    */
    public function getVerify($confirmation_code = null) 
    {
        $api = $this->callApi('/api/user/verify/' . $confirmation_code, 'GET');

        switch($api->getStatusCode()) {
            case 200:
                Request::session()->put('auth_token', $api->original['token']);
                return redirect()->route('index');

            default:
                return back()->withInput($inputs)->withErrors($api->original);
        } 
    }

    /**
    * Search for user
    *
    * @param string $name
    * @return Response
    */
    public function searchUser($name) {
        return $this->callApi('api/user/search/' . $name);
    }

    /**
    * Get user posts
    *
    * @return Response
    */
    public function getPosts($id) 
    {
        $user = $this->callApi('api/user/' . $id, 'GET')->original;

        Request::session()->put('profile-active-menu', 'posts');

        $input = Request::all();
        $input['where'] = ['user_id' => $id];
        
        $posts = $this->retrievePosts($input);

        return view('user.posts', $user + $posts);
    }
}