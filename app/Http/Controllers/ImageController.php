<?php namespace App\Http\Controllers;

use Image;
use Log;
use Request;
use SplFileInfo;
use Validator;

class ImageController extends Controller
{

    const TMP_DIR = '/img/tmp/';
 

    /** 
    * Upload an image
    *
    * @return Response
    */
    public function postImage()
    {
        Log::debug('Uploading image');

        $validator = Validator::make(Request::all(), [
            'file' => 'required|max:10000' 
        ]);

        $file = Request::file('file');
        if($validator->fails()) {
            return response($validator->errors(), 422);
        } else if($file->isValid()) {
            $originalFilename = $file->getClientOriginalName();
            Log::debug($file->getRealPath());

            $newFilename = $this->createUniqueFilename($this->getFileExtension($originalFilename));
            $path = public_path() . SELF::TMP_DIR . $newFilename;

            $image = Image::make($file->getRealPath())->
                            orientate()->
                            save($path, 60);

            return response([
                'original' => self::TMP_DIR . $newFilename, 
                'small' => $this->resizeImage($path, 300, 150, function($constraint) {
                        $constraint->aspectRatio();
                    })['path'],
                'medium' => $this->resizeImage($path, 600, 350, function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })['path'],
                'thumbnail' => $this->resizeImage($path, 80, 80, null, true)['path']
            ], 200);

        } else {
            return response(['Error uploading file.'], 500);
        }
    }

    /**
    * Update profile picture
    * 
    * @return Response
    */
    public function putAvatar()
    {
        $inputs = Request::input();
        $path = public_path() . $inputs['image'];

        $image = Image::make(public_path() . $inputs['image'])->
                        crop(intval($inputs['width']), intval($inputs['height']), intval($inputs['x']), intval($inputs['y']))->
                        resize(300, 300)->
                        encode('data-url');

        $thumbnail = Image::make(public_path() . $inputs['image'])->
                        crop(intval($inputs['width']), intval($inputs['height']), intval($inputs['x']), intval($inputs['y']))->
                        resize(80, 80)->
                        encode('data-url');

        $api = $this->callApi('/api/user/avatar', 'PUT', [
            '_id' => Request::session()->get('user')['_id'],
            'avatar_original' => (string) $image,
            'avatar' => (string) $thumbnail
        ]);

        switch ($api->getStatusCode()) {
            case 200:
                Request::session()->put('user', $api->original);
                
                return response(['message' => 'Profile picture was successfully updated', '_id' => $api->original['_id']], 200);
            
            default:
                return response(['errors' => $api->original], $api->getStatusCode());
        }
    }

    /**
    * Creates thumbnail
    *
    * @param string $path
    * @return void
    */
    public function resizeImage($path, $width, $height, $aspectRatio = null, $fit = false) {
        $ext = $this->getFileExtension($path);
        $basename = (new SplFileInfo($path))->getBasename('.' . $ext);

        $filename = $basename . '-' . $width . 'x' . $height . '.' . $ext;

        $newImagePath = self::TMP_DIR . $filename;

        if($fit) {
            $image = Image::make($path)->fit($width);
        } else {
            $image = Image::make($path)->resize($width, $height, $aspectRatio);
        }

        $image->save(public_path() . $newImagePath);

        return ['data' => $image->encode('data-url'), 'path' => $newImagePath];
    }

    /**
    * Create a random filename
    *
    * @return string
    */
    protected function createUniqueFilename($extension) 
    {
        return md5(str_random(8)) . '.' . $extension; 
    }

    /**
    * Get the file extension
    *
    * @param string $filename
    * @return string
    */
    protected function getFileExtension($filename) 
    {
        $info = new SplFileInfo($filename);
        return $info->getExtension();
    }

}