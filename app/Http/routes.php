<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'PostController@getIndex']);
Route::get('user/create', 'UserController@getPost');

Route::get('/terms-and-conditions', 'HomeController@getTermsAndConditions');
Route::get('/contact-us', 'HomeController@getContactUs');
Route::post('/contact-us', 'HomeController@postContactUs');

Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@login']);
Route::get('fb_login', 'Auth\AuthController@fbLogin');
Route::get('fb_login_cb', 'Auth\AuthController@fbLoginCallback');
Route::get('logout', 'Auth\AuthController@logout');
Route::get('login/forgot', 'Auth\AuthController@forgot');
Route::post('login', ['uses' => 'Auth\AuthController@post', 'as' => 'login']);

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::get('password/change', ['uses' => 'Auth\PasswordController@getChange']);
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::post('password/reset', 'Auth\PasswordController@postReset');
Route::post('password/change', ['uses' => 'Auth\PasswordController@postChange', 'middleware' => 'auth']);

Route::get('user/me', 'UserController@getMe');
Route::get('user/search/{name}', ['uses' => 'UserController@searchUser', 'middleware' => ['auth']]);
Route::get('user/notifications', ['uses' => 'UserController@getNotifications', 'middleware' => 'auth']);
Route::get('user/edit', 'UserController@getEdit');
Route::get('user/verify/{confirmation_code}', 'UserController@getVerify');
Route::get('user/{id}', ['as' => 'user.profile', 'uses' => 'UserController@get']);
Route::get('user/{id}/posts', 'UserController@getPosts');
Route::get('user/{id}/followers', 'UserController@getFollowers');
Route::get('user/{id}/following', 'UserController@getFollowing');
Route::get('user/follow/{id}', 'UserController@getFollow');
Route::post('user', 'UserController@post');
Route::post('user/edit', 'UserController@postEdit');
Route::put('/user/avatar', ['uses' => 'ImageController@putAvatar', 'middleware' => 'auth']);

Route::get('post/new', 'PostController@getNew');
Route::get('post/edit/{id}', 'PostController@getEdit');
Route::get('post/{id}/{title}', ['uses' => 'PostController@get', 'as' => 'post']);
Route::post('post/update', ['uses' => 'PostController@postUpdate', 'as' => 'post.update']);
Route::post('post/like', ['uses' => 'PostController@postLike']);
Route::post('post/', 'PostController@postCreate');
Route::post('posts/search', ['uses' => 'PostController@postSearch']);
Route::get('posts/search/{category}/{searchText?}', ['uses' => 'PostController@getSearch', 'as' => 'post.search']);

Route::post('image/', ['uses' => 'ImageController@postImage', 'middleware' => 'auth']);
Route::post('comment/', ['uses' => 'CommentController@post']);

Route::get('notifications', ['uses' => 'NotificationController@getNotifications', 'middleware' => 'auth']);
Route::get('notification/read/{id?}/', ['uses' => 'NotificationController@getRead', 'middleware' => 'auth']);

Route::get('conversations/{type?}/{with?}', ['uses' => 'MessageController@getConversations', 'middleware' => 'auth' ]);
Route::get('conversation/{with}', ['uses' => 'MessageController@getConversation', 'middleware' => 'auth' ]);
Route::get('messages/{with}/{skip?}', ['uses' => 'MessageController@getMessages', 'middleware' => 'auth']);
Route::delete('conversation/{with}', ['uses' => 'MessageController@delete', 'middleware' => 'auth' ]);

Route::put('conversation/{with}/read', ['middleware' => ['auth'], 'uses' => 'MessageController@markAsRead']);
Route::post('message/', ['uses' => 'MessageController@postMessage', 'middleware' => 'auth']);

// routes for API 
Route::post('api/auth/', 'Api\AuthController@post');
Route::post('api/auth/fb_login', 'Api\AuthController@fbLogin');
Route::post('api/auth/token', ['uses' => 'Api\AuthController@authToken', 'middleware' => 'api.auth']);

Route::post('api/password/email', 'Api\PasswordController@postEmail');
Route::post('api/password/reset', 'Api\PasswordController@postReset');
Route::post('api/password/change', ['uses' => 'Api\PasswordController@postChange', 'middleware' => ['api.auth']]);

Route::get('api/user/me', ['uses' => 'Api\UserController@getMe', 'middleware' => ['api.auth']]);
Route::get('api/user/search/{name}', ['uses' => 'Api\UserController@searchUser', 'middleware' => ['api.auth']]);
Route::get('api/user/{id}', 'Api\UserController@get');
Route::get('api/user/verify/{confirmation_code}', 'Api\UserController@getVerify');
Route::get('api/user/follow/{id}', ['uses' => 'Api\UserController@getFollow', 'middleware' => ['api.auth'] ]);
Route::post('api/user/', 'Api\UserController@post');
Route::put('api/user/', ['uses' => 'Api\UserController@put', 'middleware' => ['api.auth']]);
Route::put('api/user/avatar', ['uses' => 'Api\UserController@putAvatar', 'middleware' => ['api.auth']]);

Route::get('api/posts/latest', ['uses' => 'Api\PostController@getLatestPosts']);
Route::get('api/posts/{searchText?}', ['uses' => 'Api\PostController@getPosts']);
Route::get('api/post/{id}', ['uses' => 'Api\PostController@get']);
Route::post('api/post/', ['uses' => 'Api\PostController@post', 'middleware' => ['api.auth']]);
Route::post('api/post/like', ['uses' => 'Api\PostController@postLike', 'middleware' => ['api.auth']]);
Route::put('api/post/', ['uses' => 'Api\PostController@put', 'middleware' => ['api.auth']]);
Route::delete('api/post/{id}', ['uses' => 'Api\PostController@delete', 'middleware' => ['api.auth']]);

Route::post('api/comment/', ['uses' => 'Api\CommentController@post', 'middleware' => ['api.auth']]);
Route::delete('api/comment/{id}', ['middleware' => ['jwt.auth'], 'uses' => 'Api\CommentController@delete']);

Route::get('api/conversations/{type?}/{with?}', ['uses' => 'Api\MessageController@getConversations', 'middleware' => ['api.auth']]);
Route::get('api/conversation/{with}', ['uses' => 'Api\MessageController@getConversation', 'middleware' => ['api.auth']]);
Route::get('api/messages/{with}/{skip}', ['uses' => 'Api\MessageController@getMessages', 'middleware' => ['api.auth']]);

Route::put('api/conversation/{with}/read', ['middleware' => ['api.auth'], 'uses' => 'Api\MessageController@markAsRead']);
Route::post('api/message/', ['middleware' => ['api.auth'], 'uses' => 'Api\MessageController@post']);

Route::delete('api/conversation/{with}', ['middleware' => ['api.auth'], 'uses' => 'Api\MessageController@delete']);

Route::get('api/notifications', ['uses' => 'Api\NotificationController@getNotifications', 'middleware' => ['api.auth'] ]);
Route::get('api/notification/read/{id?}/', [ 'uses' => 'Api\NotificationController@getRead', 'middleware' => ['api.auth'] ]);
