<?php namespace App\Repositories;

use Log;
use Redis;
use App\Models\Notification;

class NotificationRepository extends BaseRepository 
{
    /**
    * Creates a new PostRepository instance. 
    *
    * @param App\Models\Post @post
    * @return void
    */
    public function __construct(Notification $notification) 
    {
        $this->model = $notification;
    }

    /**
    * Get the notifications
    *
    * @param string $userId
    * @return Array
    */
    public function getNotifications($userId) 
    {
        return [
            'results' => $this->model
                              ->where('to', '=', $userId)
                              ->with('fromUser')
                              ->orderBy('created_at', 'desc')
                              ->get()
                              ->keyBy('_id')
                              ->toArray(),
            'myId' => $userId
        ];
    }

    /**
    * Save the post.
    *
    * @param Array $inputs
    * @return App\Models\Notification
    */
    public function save($inputs) 
    {
        if($inputs['to'] !== $inputs['from']) {
            $notification = new $this->model($inputs);

            $notification->save();

            $notification['from_user'] = $inputs['fromUser'];
            Redis::publish('notifications:' . $notification->to, 
                           json_encode($notification));

            return $notification;
        }
    }

    /**
    * Update notification as read
    *
    * @param string $id
    * @param string $userId
    * @param Array $Input
    * @return mix
    */
    public function update($id, $userId = null, $input = ['has_read' => true]) {
        if($id == null) {
            $ret = $this->model
                        ->where('to', '=', $userId)
                        ->where(function($q) {
                            $q->orWhere('has_read', '=', false);
                            $q->orWhereNull('has_read');
                        })
                        ->update($input);

            return ['updates' => $ret];
        } else {
            $notification = $this->getByCondition([
                '_id' => $id, 
                'to' => $userId
            ]);

            $notification->fill($input);
            $notification->save();

            $fromUser = $notification->fromUser->shallowUser();
            unset($notification->fromUser);
            $notification['from_user'] = $fromUser;

            return $notification->toArray();
        }
    }

}