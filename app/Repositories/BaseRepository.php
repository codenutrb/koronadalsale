<?php
namespace App\Repositories;

use Log;

abstract class BaseRepository {

    /**
    *The Model instance.
    *
    *@var Illuminate\Database\Eloquent\Model
    */
    protected $model;

    /**
    *Get number of records.
    *
    *@return array
    */
    public function getNumber() 
    {
        $total = $this->model->count();
        return $total;
    }

    /**
    * Get Model by id.
    *
    * @param Array $where
    * @return \Model
    */
    public function getByCondition($where = [], $offset = null, $limit = 12, $orWhere = [])
    {
        $model = $this->model->where('deleted_at', null);
        if(!array_key_exists('_id', $where)) {
           
            if(count($where) > 0) {
                $model->whereRaw($where);
            }

            if(count($orWhere) > 0) {
                $model->orWhere($orWhere);
            }

            $total = $model->count();
            
            if(isset($offset)) {
                $model->skip($offset * $limit)->take($limit);
            }

            return ['results' => $model->orderBy('updated_at', 'desc')->get(), 'total' => $total];
        } else {
            return $model->where($where)->first();
        }
    }

    /**
    * Get model by id and use owner.
    *
    * @param string $id
    * @param string $user_id
    * @return \App\Models\User
    */
    public function getByIdAndOwner($id, $user_id = null) 
    {
        if($id === $user_id) {
            return $this->getById($id);
        } else {
            return $this->getByCondition(['_id' => $id, 'user_id' => $user_id]);
        }
    }

    /**
    * Get model by id
    *
    * @param string $id
    * @return \App\Models\User
    */
    public function getById($id) 
    {
        return $this->getByCondition(['_id' => $id]); 
    }

    /**
    * Deletes a model.
    *
    * @param string $id
    * @param string $user_id
    * @return boolean
    */
    public function delete($id, $user_id = null)
    {
        $model = $this->getByIdAndOwner($id, $user_id);
        if(isset($model)) { 
            return $model->delete();
        }
    }

}