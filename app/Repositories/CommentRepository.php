<?php
namespace App\Repositories;

use App\Models\Comment;
use App\Models\Post;

class CommentRepository extends BaseRepository
{

    /**
    * Creates a new CommentRepository instance.
    *
    * @param \App\Models\Comment $comment
    * @return void
    */
    public function __construct(Comment $comment) 
    {
        $this->model = $comment;
    }

    /**
    * Creates a new comment.
    *
    * @param Array $inputs
    * @param string $user_id
    * @param string $post_id
    * @return \App\Models\Comment 
    */
    public function save($inputs, $post_id = null, $user_id = null) 
    {
        $comment = new $this->model($inputs);

        if($user_id !== null) {
            $comment->user_id = $user_id;
            $comment->post_id = $post_id;

            // update post
            $post = Post::find($post_id);
            $post->updated_at = time();
            $post->save();
        }


        $comment->save();

        return $comment;
    }
}