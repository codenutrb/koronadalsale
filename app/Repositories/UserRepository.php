<?php
namespace App\Repositories;

use Log;
use App\Models\User;

class UserRepository extends BaseRepository 
{
    /**
    * Creates a new UserRepository instance.
    *
    * @param App\Models\User $user
    * @return void
    */
    public function __construct(User $user) 
    {
        $this->model = $user;
    }

    /**
    * Get user.
    *
    * @param string $id
    * @return App\Models\User
    */
    public function get($id)
    {
        $user = $this->getById($id);

        return $user;
    }

    /**
    * Get user by confirmation code
    * 
    * @param string $confirmationCode
    * @return App\Models\User
    */
    public function getUserByConfirationCode($confirmationCode) 
    {
        return $this->model
                    ->where('confirmation_code', '=', $confirmationCode)
                    ->firstOrFail();
    }

    /**
    * Save the user.
    *
    * @param Array $inputs
    * @return App\Models\User
    */
    public function save($user) 
    {
        if(!isset($user['_id'])) {
            $user->password = bcrypt($user->password);
            $user->confirmation_code = str_random(30);
        }

        $user->save();

        return $user;
    }

    /**
    * Update the user.
    *
    * @param Array $inputs
    * @return App\Models\User
    */
    public function update($inputs, $user_id = null) 
    {
        $user = $this->getByIdAndOwner($inputs['_id'], $user_id);

        $user->fill($inputs);

        $user->save();

        return $user;
    }

    /**
    * Search user
    *
    * @param string $name
    * @return App\Models\User
    */
    public function searchUser($name, $userId = '') 
    {
        return $this->model
                    ->where('name', 'LIKE', $name . '%')
                    ->where('_id', '!=', $userId)
                    ->take(10)
                    ->get()
                    ->map(function($o) { return $o->shallowUser(); } )
                    ->toArray();
    }

}