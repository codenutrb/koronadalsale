<?php
namespace App\Repositories;

use App\Models\Post;

class PostRepository extends BaseRepository 
{
    /**
    * Creates a new PostRepository instance. 
    *
    * @param App\Models\Post @post
    * @return void
    */
    public function __construct(Post $post) 
    {
        $this->model = $post;
    }

    /** 
    * Get a post
    *
    * @param string $id
    * @return App\Models\Post
    */
    public function get($id) 
    {
        return $this->getById($id);
    }

    /**
    * Search for post
    *
    * @param Array
    */
    public function search($searchText, $page) 
    {
        return $this->getByCondition(['$text' => ['$search' => $searchText]]);
    }

    /**
    * Save the post.
    *
    * @param Array $inputs
    * @param string $user_id 
    * @return App\Models\Post
    */
    public function save($inputs, $user_id = null) 
    {
        $post = new $this->model($inputs);

        if($user_id) {
            $post->user_id = $user_id;
        }

        $post->save();

        return $post;
    }

    /**
    * Update a post.
    *
    * @param Array $input
    * @param string $user_id
    * @return App\Models\Post
    */
    public function update($inputs, $user_id = null) 
    {
        $post = $this->getByIdAndOwner($inputs['_id'], $user_id);
        if(isset($post)) {
            $post->fill($inputs); 
            $post->update();

            return $post;
        }
    }

    /**
    * Like a post
    *
    * @params $inputs
    * @return array 
    */
    public function like($inputs, $user_id = null) 
    {
        $post = $this->getById($inputs['_id']);
        if(isset($post)) {
            $likes = [];

            if(isset($post['user_likes'])) {
                foreach($post['user_likes'] as $key => $user) {
                    $likes[$user] = $user;
                }
            }

            if(array_key_exists($user_id, $likes)) {
                unset($likes[$user_id]);
                $ret = false;
            } else {
                $likes[$user_id] = $user_id;
                $ret = true;
            }

            $post['user_likes'] = array_values($likes);
            $post->update();

            return ['post' => $post->toArray(),
                    'count' => count($post['user_likes']), 
                    'like' => $ret];
        } 
    }

    /**
    * Get latest post
    * 
    * @return App\Models\Post
    */
    public function getLatest() 
    {
        return $this->model
                    ->select(['_id', 'user_id', 'title', 'photos'])
                    ->with(['user' => function($query){
                        $query->select('_id', 'name');
                    }])
                    ->orderBy('created_at', 'desc')
                    ->take(5)
                    ->get();
    }
}