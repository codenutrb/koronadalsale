<?php
namespace App\Repositories;

use Log;
use Carbon\Carbon;
use App\Models\Message;
use App\Models\Conversation;

class MessageRepository extends BaseRepository 
{
    /**
    * The  Conversation instance
    *
    * @var \App\Models\Conversation $conversation
    */
    protected $conversation;

    /**
    * Creates a new MessageRepository instance.
    *
    * @param \App\Models\Message $message
    * @param \App\Models\Conversation $conversation
    * @return void
    */
    public function __construct(Message $message, Conversation $conversation)
    {
        $this->model = $message;
        $this->conversation = $conversation;
    }    

    /**
    * Save the message.
    *
    * @param Array $inputs
    * @param string $userId
    * @return \App\Models\Message
    */
    public function save($inputs, $userId = null) 
    {
        $message = new $this->model($inputs);

        if($userId !== null) {
            $message->from_user_id = $userId;
        }

        $conversation = $this->getConversationWithUser(
            $message->from_user_id, 
            $message->to_user_id
        );

        // set conversation props on first message
        if($conversation->user_id_1 === $userId) {
            $conversation->user2_last_received = (string) Carbon::now();
        } else if($conversation->user_id_2 === $userId) {
            $conversation->user1_last_received = (string) Carbon::now();
        }

        $conversation->user1_message_count += 1;
        $conversation->user2_message_count += 1;

        $conversation->save();
        $message->conversation_id = $conversation->_id;
        $message->save();

        return $message;
    }

    /** 
    * Get messages for user with other user
    *
    * @param string $userId
    * @param string $with
    * @param integer $skip
    * @return App\Models\Message
    */
    public function getMessages($userId, $with, $skip) 
    {
        if($userId > $with) {
            $deleteProp = 'user2_has_deleted';
        } else {
            $deleteProp = 'user1_has_deleted';
        }

        $messages = $this->model
                         ->where(function($p) use($userId, $with) {
                             $p->orWhere(function($q) use($userId, $with) {
                                $q->where('from_user_id', '=' , $userId);
                                $q->where('to_user_id', '=', $with);
                             });
                             $p->orWhere(function($q) use($userId, $with) {
                                $q->where('from_user_id', '=', $with);
                                $q->where('to_user_id', '=', $userId);
                             });
                         })
                         ->where($deleteProp, '!=', true)
                         ->orderBy('created_at', 'desc')
                         ->skip($skip)
                         ->take(10)
                         ->get();

        return $messages;
    }

    /**
    * Get the conversations for user
    *
    * @param string $id
    * @return array
    */
    public function getConversations($userId, $relations = ['user1', 'user2'])
    {
        $conversations = $this->conversation 
                              ->orWhere(function($q) use($userId) {
                                 $q->where('user_id_1', '=', $userId);
                                 $q->where('user1_message_count', '>', 0);
                              })
                              ->orWhere(function($q) use($userId) {
                                 $q->where('user_id_2', '=', $userId);
                                 $q->where('user2_message_count', '>', 0);
                              })
                              ->with($relations)
                              ->orderBy('updated_at', -1)
                              ->get();

        $conversations->map(function($conversation) use($userId) {
            $conversation['active_messages'] = $conversation->activeMessages($userId);
            return $conversation;
        });

        return $conversations->keyBy('_id'); 
    }

    /**
    * Get conversation with other user
    *
    * @param string $userId1
    * @param string $userId2
    * @return \App\Models\Conversation
    */
    public function getConversationWithUser($userId1, $userId2, $withRelations = false) 
    {
        $userId = $userId1;
        if($userId1 > $userId2) {
            $temp = $userId1;
            $userId1 = $userId2;
            $userId2 = $temp;
        } 

        $conversation = $this->conversation->firstOrCreate([
            'user_id_1' => $userId1,
            'user_id_2' => $userId2
        ]);

        if($withRelations) {
            $conversation->user1;
            $conversation->user2;
            $conversation['active_messages'] = $conversation->activeMessages($userId);
        }

        return $conversation;
    }

    /**
    * Mark messages as read
    *
    * @param string $conversationId
    * @return \App\Models\Conversation
    */
    public function markAsRead($userId, $with) 
    {
        $this->model
             ->orWhere(function($q) use($with, $userId) {
                $q->where('from_user_id', '=', $with);
                $q->where('to_user_id', '=', $userId);
             })
             ->orWhere(function($q) use($with, $userId) {
                $q->where('from_user_id', '=', $userId);
                $q->where('to_user_id', '=', $with);
             })
             ->where('has_read', '!=', true)
             ->update(['has_read' => true]);

        return $this->getConversationWithUser($userId, $with, true);
    }

    public function deleteConversation($userId, $with) 
    {
        $conversation = $this->getConversationWithUser($userId, $with);

        if($conversation->user_id_1 === $userId) {
            $deleteProp = 'user1_has_deleted';
            $conversation->user1_last_received = null;
            $conversation->user1_message_count = 0;
        } else if($conversation->user_id_2 === $userId) {
            $deleteProp = 'user2_has_deleted';
            $conversation->user2_last_received = null;
            $conversation->user2_message_count = 0;
        }

        $conversation->save();

        if(isset($deleteProp)) {
            return $this->model
                        ->where('conversation_id', '=', $conversation->_id)
                        ->update([$deleteProp => true]);
        } else {
            return 0;
        }

    }
    
}