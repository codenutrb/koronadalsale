<?php namespace App\Repositories;

use Carbon;
use Log;
use App\Models\Message;
use App\Models\Conversation;

class ConversationRepository extends BaseRepository
{
    /**
    * The message instance
    *
    * @var App\Models\Message
    */
    protected $message;

    /**
    * Creates a new ConversationRepository instance.
    *
    * @param \App\Models\Message $message
    * @return void
    */
    public function __construct(Conversation $conversation, Message $message)
    {
        $this->model = $conversation;
        $this->message = $message;
    }    

    /**
    * Get conversation with id
    *
    * @param string $id
    * @return App\Models\Conversation
    */
    public function getConversation($id, $with = ['messages', 'user1', 'user2']) 
    {
        $conversation = $this->model;

        foreach($with as $key => $value) {
            $conversation = $conversation->with($value);
        }

        return $conversation->find($id);
    }

    /*
    * Get conversation with user
    *
    * @param string $user1
    * @param string $user2
    * @return App\Models\Conversation
    */
    public function getConversationWithUser($userId1, $userId2) 
    {
        $conversation = $this->findOrCreateConversation($userId1, $userId2);

        return $this->getConversation($conversation->_id);
    }

    /**
    * Find conversations
    *
    * @param string $userId1
    * @param string $userId2
    * @param boolean $create
    * @return App\Models\Conversation
    */
    public function findOrCreateConversation($userId1, $userId2) {
        // swap - builtin function please :(
        if($userId1 > $userId2) {
            $t = $userId1;
            $userId1 = $userId2;
            $userId2 = $t;
        }

        return $this->model->firstOrCreate([
            'user_id_1' => $userId1, 
            'user_id_2' => $userId2
        ]);
    }

    /**
    * Mark messages as read
    *
    * @param string $conversationId
    * @return \App\Models\Conversation
    */
    public function markAsRead($conversationId, $userId) 
    {
        $this->message
             ->where('has_read', '!=', true)
             ->where('conversation_id', '=', $conversationId)
             ->update(['has_read' => true]);

        $conversation = $this->getConversation($conversationId);
        return $conversation;
    }
 
}