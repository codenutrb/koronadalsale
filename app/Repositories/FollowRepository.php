<?php namespace App\Repositories;

use Log;
use App\Models\Follow;

class FollowRepository extends BaseRepository
{

    /**
    * Creates a new UserRepository instance.
    *
    * @param App\Models\Follow $follow
    * @return void
    */
    public function __construct(Follow $follow) 
    {
        $this->model = $follow;
    }

    /**
    * Save the user.
    *
    * @param Array $inputs
    * @return App\Models\User
    */
    public function save($follower, $followee) 
    {
        $follow = $this->getByCondition([
            'follower' => $follower,
            'followee' => $followee
        ]);

        if($follow['total'] > 0) {
            $follow = $follow['results'][0]->delete();

            return ['status' => 0];
        } else {
            $follow = new Follow([
                'follower' => $follower,
                'followee' => $followee
            ]);

            $follow->save();

            return [
                'status' => 1, 
                'user' => $follow->_followee->shallowUser()
            ];
        }
    }
}