var sse             = require('connect-sse')();
var express         = require('express');
var config          = require('config');
var cookieParser    = require('cookie-parser');
var redis           = require('redis');
var vhost           = require('vhost');
var cors            = require('cors');
var _auth           = require('./utils')();

var main = express();

var corsOptions = {
    origin: config.get('laravel_url'),
    credentials: true
};

main.use(cookieParser());
main.use(cors(corsOptions));

main.get('/messages/:userId', sse, _auth, function (req, res) {
    var subscriber = redis.createClient();
    subscriber.subscribe('messages:' + req.params.userId);

    subscriber.on('message', function(channel, message) {
        console.log(res.statusCode);
        if(res.statusCode === 200) {
            res.json({
                status: 200,
                channel: channel,
                message: message
            });
        } else {
            res.json({
                status: res.statusCode,
                message: 'Invalid session'
            });
        }
    });
});

main.get('/notifications/:userId', sse, _auth, function(req, res) {
    var subscriber = redis.createClient();
    subscriber.subscribe('notifications:' + req.params.userId);

    subscriber.on('message', function(channel, message) {
        if(res.statusCode === 200) {
            res.json({
                status: 200,
                channel: channel,
                message: message
            });
        } else {
            res.json({
                status: res.statusCode,
                message: 'Invalid session'
            });
        }

    });

});

var app = module.exports = express();

var url = config.get('url');

app.use(cookieParser());
app.use(vhost(url, main));
app.use(cors(corsOptions));

var server = app.listen(config.get("port"), function() {
    var host = server.address().address;
    var port = server.address().port;

    console.log('App has started istening at http://%s:%s', host, port);
});
