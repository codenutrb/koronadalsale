var crypto          = require('crypto');
var unserialize     = require('php-unserialize').unserialize;
var config          = require('config');
var redis           = require('redis');

var self = {
    getLaravelSessionId: function(req) {
        if(req.cookies.JSESSIONID) {
            var cookie = JSON.parse(new Buffer(req.cookies.JSESSIONID, 'base64'));
            var iv = new Buffer(cookie.iv, 'base64');
            var value = new Buffer(cookie.value, 'base64');
            var key = config.get('laravel_key');

            var decipher = crypto.createDecipheriv(config.get('cipher'), key, iv);
            var sessionId = decipher.update(value) + decipher.final();

            return unserialize(sessionId);
        }
    },

    getUserId: function(s) {
        var re = new RegExp('"MongoId":[0-9]+:{[a-zA-Z0-9]+}');
        var m = re.exec(s.toString());

        var re2 = new RegExp('{[a-zA-Z0-9]+}');
        var m2 = re2.exec(m);

        return m2[0].replace(/\{|\}/g, '');
    },

    validateSession: function (res, sessionId, userId, cb) {
        var client = redis.createClient();
        var _this = this;

        client.get(config.get('laravel_prefix') + ':' + sessionId, function(_err, _data) {
            if(_data) {
              cb(res, userId, _this.getUserId(_data));
            } else {
              cb(res);
            }
        });
    },

    authenticate: function (res, userId, _userId) {
        console.log(userId, _userId);
        if(!userId || !_userId || userId !== _userId) {
            res.status(401);
        }
    }
};

module.exports = function() {
  return function (req, res, next) {
    var sessionId = self.getLaravelSessionId(req);
    self.validateSession(res, sessionId, req.params.userId, self.authenticate);
    next();
  };
};
