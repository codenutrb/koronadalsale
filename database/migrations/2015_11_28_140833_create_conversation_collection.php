<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversation', function($collection) {
            $collection->integer('message_count')->default(0);
            
            $collection->integer('user1_received')->default(0);
            $collection->integer('user2_received')->default(0);

            $collection->boolean('user1_has_new')->default(false);
            $collection->boolean('user2_has_new')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
