var alt = new Alt();

var objectToArray = function(obj) {
    if(typeof obj !== 'undefined') {
        return Object.keys(obj).map(function(o) {
            return obj[o];
        });
    } else {
        return [];
    }
};

var latestCmp = function(a, b) {
    var date1 = new Date(a.created_at);
    var date2 = new Date(b.created_at);
    if(date2 < date1) {
        return -1;
    } else if(date2 > date1) {
        return 1;
    } else {
        return 0;
    }
};

var sortByProp = function(array, prop) {
    var cmp = function(a, b) {
        var cmp1 = typeof a[prop] !== 'undefined' ? new Date(a[prop].replace(/-/g, "/")) : new Date();
        var cmp2 = typeof b[prop] !== 'undefined' ? new Date(b[prop].replace(/-/g, "/")) : new Date();

        if(cmp2 < cmp1) {
            return -1;
        } else if(cmp2 > cmp1) {
            return 1;
        } else {
            return 0;
        }
    };

    return array.sort(cmp);
};

var hasRead = function(conversation, me) {
    var messages = sortByProp(objectToArray(conversation.messages), 'created_at');

    if(messages.length > 0) {
        return messages[0].from_user_id === me._id || messages[0].has_read;
    } else {
        return true;
    }
};

var cloneObject = function(obj) {
    return $.extend({}, true, obj);
};

var listenOnMessages =  function(url, cb) {
    var source = new EventSource(url, {withCredentials:true});
    source.addEventListener('message', function(e) {
        var data = JSON.parse(e.data);
        if(data.status === 200) {
            var payload = JSON.parse(data.message);
            cb(payload);
            if(data.channel.startsWith('notifications')) {
                $.notify.addStyle('notify', {
                    html:
                    '<div class="clearfix">' +
                        '<a href="' + payload.url + '" class="dark-link div-link notif-popover" >' +
                            '<div class="panel-body panel-profile notif-new">' +
                                 '<div class="pull-left">' +
                                    '<img src="' + payload.from_user.avatar + '" alt="" height="40" width="40" />' +
                                '</div>' +
                                '<div class="pull-right notif-pop">' +
                                    '<strong>' + payload.from_user.name + '</strong>' +
                                    '<span>' + payload.message + '</span>' +
                                    '<span>' + (payload.extra ? ' - ' + payload.extra : '') + '</span>' +
                                    '<br/>' +
                                    '<span><small>' + moment(payload.created_at).fromNow() + '</small></span>' +
                                '</div>' +
                            '</div>' +
                        '</a>' +
                    '</div>'
                });

                $.notify({
                    title: payload.from_user.name + ' ' + payload.message
                },
                {
                    clickToHide: false,
                    style: 'notify',
                    autoHide: true,
                });
            }
        } else {
            window.location = '/login';
        }
    }, false);

    source.addEventListener('open', function(e) {

    }, false);

    source.addEventListener('error', function(e) {
        console.log(e);
    }, false);
};