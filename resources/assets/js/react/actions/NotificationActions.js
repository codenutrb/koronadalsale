var notificationActions = alt.createActions({

    getNotifications: function() {
        var self = this;

        $.get('/notifications', function(response) {
            self.dispatch(response);
        }).fail(function(response) {
            if(status === 401) {
                window.location = '/login';
            }
        }).done(function() {

        });
    },

    markAsRead: function(notification) {
        if(!notification.has_read) {
            var self = this;
            $.get('/notification/read/' + notification._id, function(response) {
                window.location = response.url;
                if(window.location.hash) {
                    window.location.reload();
                }
            }).error(function(response) {
                if(status === 401) {
                    window.location = '/login';
                }
            }).done(function(response) {

            });
        } else {
            window.location = notification.url;
        }
    },

    markAllAsRead: function() {
        var self = this;

        $.get('/notification/read', function(response) {
            self.dispatch(response);
        }).error(function(response) {
            if(status === 401) {
                window.location = '/login';
            }
        }).done(function(response) {

        });
    },

    updateNotifications: function(notification) {
        this.dispatch(notification);
    }

});