var userActions = alt.createActions({
    initUser: function(user, me, myId) {
        this.dispatch({user: user, me: me, myId: myId});
    },

    followUser: function(id, user) {
        this.dispatch({id: id, user: user});
    },

    getMe: function() {
        var self = this;

        $.get('/user/me', function(response) {
            self.dispatch(response);
        }).fail(function(response) {
            if(response.status === 401) {
                window.location = '/logout';
            }
        }).done(function(response) {

        });
    }

});