var messageActions = alt.createActions({
    initMessage: function(conversations, me, conversation) {
        this.dispatch({
            conversations: conversations,
            me: me,
            conversation: conversation
        });
    },

    getConversations: function(type) {
        var self = this;

        $.get('/conversations/' + type, function(response) {
            self.dispatch(response);
        }).fail(function(response) {
            if(response.status === 401) {
                window.location = '/login';
            }
        }).done(function() {

        });
    },

    sendMessage: function(body, toUserId, conversationId) {
        var self = this, deferred = $.Deferred();
        console.log(body, toUserId);
        $.post('/message', {
            body: body,
            to_user_id: toUserId,
        }, function(response) {
            deferred.resolve(response);
        }).fail(function(response) {
            if(response.status === 401) {
                window.location = '/login';
            }
        }).done(function() {
            self.dispatch();
        });
        return deferred.promise();
    },

    getConversation: function(toUserId) {
        var self = this, deferred = $.Deferred();
        $.get('/conversation/' + toUserId, function(response) {
            deferred.resolve(response);
        }).fail(function(response) {
            if(response.status === 401) {
                window.location = '/login';
            }
        }).done(function() {

        });
        return deferred.promise();
    },

    markAsRead: function(conversationId) {
        var self = this;
        $.ajax({
            url: '/conversation/' + conversationId + '/read',
            method: 'PUT',
            success: function(response) {
                self.dispatch(response);
            },
            fail: function(response) {
                if(response.status === 401) {
                    window.location = '/login';
                }
            },
            done: function() {

            }
        });
    },

    updateConversations: function(conversation) {
        this.dispatch(conversation);
    },

    selectConversation: function(payload) {
        this.dispatch(payload);
    },

    deleteConversation: function(id, withUser) {
        var self = this;

        $.ajax({
            url: '/conversation/' + withUser,
            type: 'DELETE',
            success: function(response) {
                self.dispatch(id);
                $('#deleteModal').modal('hide');
            },
            fail: function(response) {
                if(response.status === 401) {
                    window.location = '/login';
                }
            },
            done: function() {
                console.log('done delete');
            }
        });
    },

    getMessages: function(conversation) {
        var self = this;

        $.get('/messages/' + conversation.user._id + '/' + conversation.messages.length,
        function(response) {
            self.dispatch(response);
        }).fail(function(response) {
            if(response.status === 401) {
                window.location = '/login';
            }
        }).done(function() {

        });
    }

});