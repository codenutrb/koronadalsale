var MessageComponent = React.createClass({
    getInitialState: function() {
        return MessageStore.state;
    },

    componentWillMount: function() {
        messageActions.initMessage(
            this.props.conversations,
            this.props.me,
            this.props.conversation
        );
    },

    componentDidMount: function() {
        MessageStore.listen(this.onChange);
       
        this.onSelectConversation(this.state.conversation._id);

        this.listen();

        var self = this;

        $('.send-input').on('keydown', function(e) {
            if(e.keyCode == 13) {
                self.onSendMessage($(this).val());
            }
        });

        $('.send-btn').on('click', function(e) {
            self.onSendMessage($('.send-input').val());
        });

        $('#new-message').on('click', function(e) {
            self.setState({
                newMessage: true,
                conversation: {messages: []}
            });

            self.updateComponent();

             $('#new-message-input').autoComplete({
                minChars: 3,
                autoFocus: true,
                source: function(term, response) {
                    $.get('/user/search/' + term, function(data) {
                        response(data);
                    }).fail(function(response) {

                    }).done(function() {

                    });
                },

                renderItem: function(item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="autocomplete-suggestion" data-name="' + item.name + '" data-id="' + item._id + '" data-val="' + search + '">' +
                           '<img src="' + item.avatar + '" height="40" width="40" class="img-rounded"/>' + 
                           '<span class="autocomplete-name"><strong>' + item.name + '</strong></span>' +
                           '</div>';
                },

                onSelect: function(e, term, item) {
                    var id = item.attr('data-id');
                    $('#new-message-input').val(item.attr('data-name'));

                    //get conversation here
                    messageActions.getConversation(id).done(function(conversation) {
                        self.onSelectConversation(conversation._id, conversation);
                    });
                }
            });

            $('#new-message-input').focus();
        });

        $('.message-box-wrapper').scroll(function() {
            if($(this).scrollTop() === 0 &&
                typeof self.state.conversation._id !== 'undefined' &&
                self.state.conversation.all_messages !== true) {

                messageActions.getMessages(self.state.conversation);
                $(this).scrollTop(5);
            }
        });

        $('#delete-message').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            messageActions.deleteConversation(self.state.conversation._id, self.state.conversation.user._id);
        });

    },

    componentWillUnmount: function() {
        MessageStore.unlisten(this.onChange);
    },

    onChange: function(state) {
        this.setState(state);
    },

    componentDidUpdate: function() {
        if(this.state.updateComponent === true) {
            this.updateComponent(this.state.conversation._id);
        }
    },

    onSelectConversation: function(id, conversation) {
        if(typeof id !== 'undefined') {
            var _conversation = this.state.conversations[id] || conversation;
            messageActions.selectConversation(_conversation);

            if(!hasRead(this.state.conversation, this.state.me)) {
                messageActions.markAsRead(this.state.conversation.user._id);
            }

            this.updateComponent(id);
        }
    },

    updateComponent: function(id) {
        if(typeof id !== 'undefined') {
            $('.dark-link').removeClass('dark-link-active');
            $('#' + id).addClass('dark-link-active');
            $('.send-input').focus();

            $('.message-box-wrapper').css('height', 'auto');
            if($('.message-box-wrapper').height() > $('.msg-wrap').height()) {
                $('.message-box-wrapper').css('height', '100%');
                $('.message-box-wrapper').scrollTop($('.message-box-wrapper')[0].scrollHeight);
            }
        } else {
            $('.dark-link').removeClass('dark-link-active');
        }
    },

    onSendMessage: function(body) {
        if(body.length <= 0) {
            return;
        }

        this.setState({
            sending: true
        });

        var self = this;
        messageActions.sendMessage(
            body,
            this.state.conversation.user._id,
            this.state.conversation._id
        ).done(function(response) {
            self.onSelectConversation(self.state.conversation._id);
        });

        $('.send-input').val('');
        $('.send-input').focus();
    },

    listen: function() {
        listenOnMessages(
            this.props.messagingUrl + this.state.me._id, 
            messageActions.updateConversations
        );
    },

    render: function() {
        var conversations, messages, removeButton;
        var self = this;

        var conversationsArray = objectToArray(this.state.conversations);

        if(conversationsArray !== 'undefined' && conversationsArray.length > 0) {
            conversations = sortByProp(conversationsArray, 'last_activity').map(function(conversation) {
              
                return(
                    <Conversation conversation={conversation} 
                                  onSelectConversation={this.onSelectConversation} 
                                  user={conversation.user}
                                  me={this.state.me}/>
                );
            }, this);

        } 

        var currentUsername, newMessage="\u00a0";

        if(typeof self.state.newMessage !== 'undefined' && self.state.newMessage) {
            newMessage = (
                <div className="form-horizontal">
                    <div className="form-group">
                        <label className="col-xs-2 col-sm-2 col-md-2 to-align">To</label>
                        <div className="col-xs-10 col-sm-10 col-md-10 no-side-padding">
                            <input type="text" className="form-control" id="new-message-input" tabIndex="2"/>
                        </div>
                    </div>
                </div>
            );
        } else if(typeof self.state.conversation !== 'undefined' &&
                  self.state.conversation !== null &&
                  typeof self.state.conversation.user !== 'undefined') {
            currentUsername = (
                <a className="btn btn-link convo-user" href={"/user/" + self.state.conversation.user._id}>
                    {self.state.conversation.user.name}
                </a>
            )

            if(typeof self.state.conversation !== 'undefined' && 
                objectToArray(self.state.conversation.messages).length > 0) {
                removeButton = <button data-toggle="modal" data-target="#deleteModal" type="button" className="btn btn-default" ><i className="fa fa-remove"> <span className="hidden-xs">Delete</span></i></button>
            }
        }

        if(typeof self.state.conversation !== 'undefined' &&
            self.state.conversation !== null) {

            var messagesArray = sortByProp(objectToArray(this.state.conversation.messages), 'created_at');

            messages = messagesArray.reverse().map(function(message) {
                return <Message message={message}  me={this.state.me} user={this.state.conversation.user}/>
            }, this);
        }

        return(
            <div className="message-box-container">
                <div className="col-md-12">
                    <div className="conversation-wrap col-xs-2 col-sm-3 col-md-4 panel-profile">
                        <div className="col-md-12 panel-profile message-header left-message-header">
                            <h4 className="hidden-xs"><strong>Messages</strong></h4>
                            <h4 className="visible-xs"><strong><i className="fa fa-lg fa-envelope"></i></strong></h4>
                        </div>
                        {conversations}
                    </div>
                    <div className="message-wrap col-xs-10 col-sm-9 col-md-8 panel-profile">
                        <div className="col-md-12 panel-profile no-side-padding message-header no-bottom-border">
                            <div className="col-xs-8 col-sm-8 col-md-8 no-side-padding">
                                {currentUsername} 
                                {newMessage}
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-4 no-side-padding">
                                <div className="btn-group pull-right" role="group">
                                    <button type="button" className="btn btn-primary" id="new-message" tabIndex="-1"><i className="fa fa-plus"></i> <span className="hidden-xs">New</span></button>
                                    {removeButton}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 msg-wrap">
                            <div className="message-box-wrapper">
                                {messages}
                            </div>
                        </div>
                        <div className="col-md-12 no-side-padding send-div">
                            <MessageBox sending={this.state.sending} disabled={this.state.newMessage || this.state.sending || this.state.conversations.length <= 0} /> 
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                         <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id="myModalLabel">Confirm delete messages</h4>
                          </div>
                          <div className="modal-body">
                            <p>
                                Clicking delete will permanently delete your messages.
                            </p>
                          </div>
                          <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary" id="delete-message" >Delete</button>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

var Conversation = React.createClass({
    componentDidMount: function() {
        var self = this;

        $('#' + this.props.conversation._id).on('click', function(e) {
            self.props.onSelectConversation(self.props.conversation._id);

            e.preventDefault();
            e.stopPropagation();
        });
    }, 

    render: function() {
        var lastMessage = ''; 

        var messages = sortByProp(objectToArray(this.props.conversation.messages), 'created_at');

        if(typeof this.props.conversation !== 'undefined' && 
            messages.length > 0) {
            lastMessage = messages[0].body;
        }

        return(
            <div className={"media media-border col-xs-12 col-md-12 no-side-padding no-margin " + (!hasRead(this.props.conversation, this.props.me) ? "unread-message" : "")}>
                <a id={this.props.conversation._id} className="dark-link convo-box">
                    <div className="media-padding">
                        <div className="media-left">
                            <img className="media-object img-circle" src={this.props.user.avatar} height="50" width="50" />
                        </div>
                        <div className="media-body hidden-xs">
                            <small className="pull-right time">
                                <i className="fa fa-clock-o"></i> {moment(this.props.conversation.last_activity).fromNow()} 
                            </small>
                            <strong>{this.props.user.name}</strong><br/>
                            <small>{lastMessage.substr(0, 20)}</small><br/>
                        </div>
                    </div>
                </a>
            </div>
        );
    }
});

var Message = React.createClass({

    render: function() {
        var message;

        if(this.props.me._id === this.props.message.from_user_id) {
            message = (
                <div className="media media-padding col-xs-12 col-md-12">
                    <div className="media-body right-align">
                        <div className="col-md-10 pull-right">
                            <div className="pull-right message-bubble my-message"> 
                                {this.props.message.body}
                                <br/>
                                <span className="time" >{moment(this.props.message.created_at).fromNow()}</span>
                            </div>
                        </div>
                    </div>
                </div> 
            );
        } else {
            message = (
                <div className="media media-padding col-xs-12 col-md-12 ">
                    <div className="media-left">
                        <img className="media-object img-circle" src={this.props.user.avatar} height="50" width="50" />
                    </div>
                    <div className="media-body">
                        <div className="col-md-10 pull-left no-side-padding">
                            <div className="pull-left message-bubble"> 
                                {this.props.message.body}
                                <br/>
                                <span className="time" >{moment(this.props.message.created_at).fromNow()}</span>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        
        return(
            <div >
                {message}
            </div>
        );
    }
});

var MessageBox = React.createClass({
   
    render: function() {
        var send, disabled;

        if(typeof this.props.sending !== 'undefined' &&
            this.props.sending === true) {
            send = <i className="fa fa-lg fa-spinner fa-spin"></i>;
        } else {
            send = <i className="fa fa-lg fa-send-o"></i>;
        }

        if(typeof this.props.disabled !== 'undefined' && 
            this.props.disabled === true) {
            disabled = 'disabled';
        } else {
            disabled = '';
        }

        return(
            <div className="input-group message-box">
                <input type="text" className={"form-control send-input " + disabled} disabled={disabled} tabIndex="1"/>
                <span className="input-group-btn">
                    <button className={"btn btn-default send-btn " + disabled} tabIndex="6">{send}</button>
                </span>
            </div>
        );
    }
});

var GetMessageNotifications = React.createClass({
     getInitialState: function() {
        return MessageStore.state;
    },

    componentDidMount: function() {
        MessageStore.listen(this.onChange);

        if(!window.location.pathname.startsWith('/conversations')) {
            messageActions.getConversations('received');
            listenOnMessages(this.props.messagingUrl + this.props.me._id, messageActions.updateConversations);
        }
    },

    componentWillUnmount: function() {
        MessageStore.unlisten(this.onChange);
    }, 

    onChange: function(state) {
        this.setState(state);
    },

    render: function() {
        return <span></span>;
    }
});

var MessageNotification = React.createClass({
    getInitialState: function() {
        return MessageStore.state;
    },

    componentDidMount: function() {
        MessageStore.listen(this.onChange);
    },

    componentWillUnmount: function() {
        MessageStore.unlisten(this.onChange);
    }, 

    onChange: function(state) {
        this.setState(state);
    },

    componentDidUpdate: function() {
        var notifications = this.conversationToNotification();

        if(document.getElementById('message-count') !== null) {
            ReactDOM.render(<NotificationCount key="sidebar-1" notifications={notifications} />, document.getElementById('message-count'));
        }

    },

    conversationToNotification: function() {
        var notifications = {};
        var conversationsArray = sortByProp(objectToArray(this.state.conversations), 'last_received');

        for(var i = 0; i < conversationsArray.length; ++i) {
            if(conversationsArray[i].has_received) {
                notifications[conversationsArray[i]['_id']] = {
                    _id: conversationsArray[i]._id,
                    message: ' sent you a message',
                    created_at: conversationsArray[i].last_received,
                    from_user: conversationsArray[i].user,
                    updated_at: conversationsArray[i].last_received,
                    has_read: hasRead(conversationsArray[i], this.state.me), 
                    url: '/conversations/all/' + conversationsArray[i].user._id
                };
            }
        }

        return notifications;
    },

    render: function() {
        var notifications = this.conversationToNotification();

        return <NotificationPopover notifications={notifications} 
                                    notification_src={this.props.notification_src} 
                                    type={this.props.type} 
                                    data="messages" />;
    }
});
