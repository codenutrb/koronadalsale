var FollowCount = React.createClass({
    getInitialState: function() {
        return UserStore.state;
    },

    componentWillMount: function() {
    },

    componentDidMount: function() {
        UserStore.listen(this.onChange);
        this.setCount();
    },

    componentWillUnmount: function() {
        UserStore.unlisten(this.onChange);
    },

    setCount: function() {
        this.setState({
            followers: Object.keys(this.state.user.followers).length,
            following: Object.keys(this.state.user.following).length
        });
    },

    onChange: function(state) {
        this.setState(state);
        this.setCount();
    },

    render: function() {
        var count = this.props.type === 'followers' ? this.state.followers : this.state.following;
        var html = <react></react>;

        if(count > 0) {
            if(this.props.page === this.props.type) {
                html = <span className="badge notif-circle">{(count > 0 ? count : ' ')}</span>;
            } else {
                html = <span className="badge badge-count">{(count > 0 ? count : ' ')}</span>;
            }
        }

        return(
            <react>{html}</react>
        );
    } 
});

var FollowButton = React.createClass({
    getInitialState: function() {
        return UserStore.state
    },

    follow: function(id) {
        var self = this;

        $.get('/user/follow/' + id, function(response) {

            if(response.data.status === 1) {
                userActions.followUser(id, response.data.user); 
            } else {
                userActions.followUser(id);
            }

        }).fail(function(response) {
            if(response.status === 401) {
                window.location = '/login';
            }
        }).done(function() {

        });
    },

    componentDidMount: function() {
        UserStore.listen(this.onChange);

        var self = this;
        $('#follow-btn-' + this.props.user._id).on('click', function(e) {
            var id = $(this).attr('data-id');
            self.follow(id);
        });

    }, 

    componentWillUnmount: function() {
        UserStore.unlisten(this.onChange);
    },

    onChange: function(state) {
        this.setState(state);
    },

    render: function() {
        var followButton;
        var buttonClass;

        if(this.props.type === 'sidebar') {
            buttonClass = 'btn btn-block profile-btn';
        } else {
            buttonClass = 'btn btn-sm follow-btn';
        }

        if(UserStore.imFollowing(this.props.user._id)) {
            followButton =  <button id={"follow-btn-" + this.props.user._id} data-id={this.props.user._id} 
                                className={buttonClass + " btn-default unfollow-btn " + (this.props.myId == this.props.user._id ? 'disabled' : '')}>
                            </button>;
        } else {
            followButton =  <button id={"follow-btn-" + this.props.user._id} data-id={this.props.user._id} 
                                className={buttonClass + " btn-primary " + (this.props.myId == this.props.user._id ? 'disabled' : '')}>
                                <i className="fa fa-user-plus"></i> Follow
                            </button>;
        }

        return(
            <react>{followButton}</react>
        ); 
    }
});
