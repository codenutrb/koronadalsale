var User = React.createClass({
    getInitialState: function() {
        return UserStore.state;
    },

    componentDidMount: function() {
        UserStore.listen(this.onChange);
    },

    componentWillUnmount: function() {
        UserStore.unlisten(this.onChange);
    },

    componentWillMount: function() {
        userActions.initUser(this.props.user, this.props.me, this.props.myId);
    },

    onChange: function(state) {
        this.setState(state);
    },

    render: function() {
        var isFollowersPage = this.props.url.indexOf('followers', this.props.url.length - 'followers'.length);
        return(
            <div>
            </div>
        );
    }
});

var UserList = React.createClass({
    getInitialState: function() {
        return {
            users: {},
            user: UserStore.state.user,
            me: UserStore.state.me,
            myId: UserStore.state.myId
        };
    },

    componentDidMount: function() {
        UserStore.listen(this.onChange);
        this.setUsers(); 
    },

    componentWillUnmount: function() {
        UserStore.unlisten(this.onChange);
    },

    setUsers: function() {
         if(this.props.page === 'followers') {
            this.setState({
                users: this.state.user.followers
            });
        } else {
            this.setState({
                users: this.state.user.following
            });
        }
    },

    onChange: function(state) {
        this.setState(state);
        this.setUsers();
    },

    render: function() {
        var self = this;

        var userNodes = Object.keys(this.state.users).map(function(key) {
            return <_User user={self.state.users[key]} key={self.state.users[key]._id} myId={self.state.myId} />
        });
        
        return (
            <div className="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                {userNodes}
            </div>
        );
    }

});

var _User = React.createClass({
    render: function() {
        var userUrl = '/user/' + this.props.user._id + '/posts/';
        var avatar;

        if(typeof this.props.user !== 'undefined' && typeof this.props.user.avatar_original !== 'undefined') {
            avatar = <img src={this.props.user.avatar_original} className="img-responsive" />
        } else {
            avatar = <img src="/img/avatar-default.png" className="img-responsive" />
        }
        return (
            <div className="col-xs-6 col-sm-6 col-md-4 col-lg-3 post-container" >
                <div className="posts-content">
                    <div className="follow-user easy-block-v1">
                        {avatar}
                    </div>
                    <div className="post">
                        <div className="user-title">
                            <h6><a className="color-dark" href={userUrl}>{this.props.user.name}</a></h6>
                            <FollowButton user={this.props.user} myId={this.props.myId} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

});
