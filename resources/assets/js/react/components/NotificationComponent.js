var GetNotifications = React.createClass({
    componentDidMount: function() {
        notificationActions.getNotifications();
        listenOnMessages(this.props.notificationsUrl + this.props.myId, notificationActions.updateNotifications);
    },

    render: function() {
        return <span></span>;
    }
});

var Notification = React.createClass({
    getInitialState: function() {
        return NotificationStore.state;
    },

    componentDidMount: function() {
        NotificationStore.listen(this.onChange);
    },

    componentWillUnMount: function() {
        NotificationStore.unlisten(this.onChange);
    },

    onChange: function(state) {
        this.setState(state);
    },

    render: function() {
        var notifications;

        if(typeof this.state.notifications !== 'undefined') {
            notifications = this.state.notifications;
        }
        return(
            <NotificationPopover notifications={notifications} notification_src={this.props.notification_src} type={this.props.type} data="notifications"/>
        );
    }
});

var NotificationPopover = React.createClass({
    componentDidMount: function() {
        var self = this;

        $('html').on('click', function(e) {
            $(self.props.notification_src).popover('hide');
        });

        $(self.props.notification_src).popover({
            html: true,
            title: (self.props.data === 'notifications' ? 'Notifications <a href="#" class="pull-right" id="read-all-notifs">Mark all as read</a>' : 
                                                          'Messages <a href="/conversations" class="pull-right">New Message</a>'),
            trigger: 'manual',
            placement: 'bottom',
            content: function() {
                return $("#notifications-" + self.props.data).html();
            }
        }).on('click', function(e) {
            $('.notif-popover').not(self.props.notification_src).popover('hide');
            $(self.props.notification_src).popover('toggle');
            $(".dropdown").removeClass("open");

            $('.notif-normal, .notif-mobile').on('click', function(e) {
                if(self.props.data === 'notifications') {
                    var id = $(this).attr('data-id');
                    if(!self.props.notifications[id].has_read) {
                        e.stopPropagation();
                        e.preventDefault();

                        notificationActions.markAsRead(self.props.notifications[id]);
                    }
                }
            });

            $('#read-all-notifs').on('click', function(e) {
                e.stopPropagation();
                e.preventDefault();

                notificationActions.markAllAsRead();
            });

            e.stopPropagation();
        });
    },

    componentDidUpdate: function() {
        var content = $("#notifications-" + this.props.data).html();
        var popover = $(this.props.notification_src).data('bs.popover');

        popover.options.content = content;
        popover.tip().find('.popover-content').html(content);
       
    },

    render: function() {
        var notifications = this.props.notifications;
        var icon;
        if(this.props.data === 'notifications') {
            icon = 'fa-globe';
        } else {
            icon = 'fa-envelope';
        }

        return(
            <span>
                <span>
                    <i className={"fa fa-lg " + icon}> </i> <NotificationCount notifications={notifications} data={this.props.data}/>
                </span>
                <div id={"notifications-" + this.props.data} style={{display: 'none'}}>
                    <Notifications notifications={notifications} 
                                   notification_src={this.props.notification_src} 
                                   type={this.props.type} />
                </div>
            </span>
        );
    }
});

var Notifications = React.createClass({
    render: function() {
        var notificationArray = sortByProp(objectToArray(this.props.notifications), 'created_at'), notifications;
        var viewAll;

        if(notificationArray.length > 0) {
            notifications = notificationArray.slice(0, 5).map(function(notification) {
                return(
                    <NotificationItem notification={notification} type={this.props.type} />
                );
            }, this);

            if(notificationArray.length > 5) {
                viewAll = (
                    <a href="/user/notifications">
                        <div className="col-xs-12 col-sm-12 col-md-12 panel-notif no-side-padding">
                            <div className="panel-body panel-profile notif-body center">
                                <strong>View all</strong>
                            </div>
                        </div>
                    </a>
                );
            } 
            
        } else {
            viewAll = (
                <div className="col-xs-12 col-sm-12 col-md-12 panel-notif no-side-padding">
                    <div className="panel-body panel-profile notif-body center">
                        <strong>Nothing to show here</strong>
                    </div>
                </div>
            );
        }

        return(
            <div>
                {notifications}
                {viewAll}
            </div>
        );
    }
});

var NotificationPage = React.createClass({
     getInitialState: function() {
        return NotificationStore.state
    },

    componentDidMount: function() {
        NotificationStore.listen(this.onChange);

        var self = this;
        $('#loadMore').on('click', function(e) {
            self.setState({
                to: self.state.to + self.state.limit
            });
        });

        $('#read-all-notifs-page').on('click', function(e) {
            e.stopPropagation();
            e.preventDefault();

            notificationActions.markAllAsRead();
        });
    },

    componentWillUnMount: function() {
        NotificationStore.unlisten(this.onChange);
    },

    onChange: function(state) {
        this.setState(state);
    },

    render: function() {
        var notificationArray = sortByProp(objectToArray(this.state.notifications), 'created_at');

        var notifications = notificationArray.slice(0, this.state.to).map(function(notification) {
            return(
                <NotificationItem notification={notification} type={this.props.type} />
            );
        }, this);

        return(
            <div>
                <div className="panel-notif">
                    <div className="col-md-12 panel-profile panel-heading overflow-h">
                        <h2 className="panel-title heading-sm pull-left"><i className="fa fa-globe"></i> Your Notifications</h2>
                        <a href="#" id="read-all-notifs-page" className="pull-right">Mark all as read</a>
                    </div>
                </div>
               
                <div className="col-xs-12 col-sm-12 col-md-12 no-side-padding">
                    {notifications} 
                </div>
                <a id="loadMore">
                    <div className={"col-xs-12 col-sm-12 col-md-12 panel-notif no-side-padding " + (this.state.to < notificationArray.length ? "" : "hidden")}>
                        <div className="panel-body panel-profile notif-body center">
                          <strong> Load More </strong>
                        </div>
                    </div>
                </a>
                <div className={"col-xs-12 col-sm-12 col-md-12 panel-notif no-side-padding " + (notificationArray.length <= 0 ? "" : "hidden")}>
                    <div className="panel-body panel-profile notif-body center">
                        Nothing to show here 
                    </div>
                </div>
            </div> 
        );
    }
});

var NotificationItem = React.createClass({
    componentDidMount: function() {
        var self = this;
        if(this.props.type === 'page') {
            $('.notif-page[data-id="' + this.props.notification._id + '"]').on('click', function(e) {
                if(!self.props.notification.has_read) {
                    e.preventDefault();
                    e.stopPropagation();

                    notificationActions.markAsRead(self.props.notification);
                }
            });
        }

    }, 

    render: function() {
        var notification;

        if(typeof this.props.notification !== 'undefined') {
            notification = (
                <div className="col-md-12 panel-notif no-side-padding">
                    <a href={this.props.notification.url} className={"dark-link div-link notif-" + this.props.type} data-id={this.props.notification._id}>
                        <div className={"panel-body panel-profile " + (this.props.type !== 'page' ? "popover-notif-body " : "notif-body ") + (!this.props.notification.has_read ? 'notif-unread' : '')}>
                             <div className="media-left">
                                <img src={(this.props.notification.from_user ? this.props.notification.from_user.avatar : '')} alt="" height="40" width="40" />
                            </div>
                            <div className={"media-body " + (this.props.type !== 'page' ? 'sm-media-body' : '')}>
                                <strong>{(this.props.notification.from_user ? this.props.notification.from_user.name : '')}</strong>
                                <span>{this.props.notification.message}</span>
                                <span>{(this.props.notification.extra ? '- ' +  this.props.notification.extra : '')}</span>
                                <br/>
                                <span><small>{moment(this.props.notification.updated_at).fromNow()}</small></span>
                            </div>   
                        </div>
                    </a>
                </div>
            );
        }

        return(
            <div>
                {notification}
            </div>
        );
    }
});


var NotificationCount = React.createClass({
    render: function() {
        var span;
      
        if(typeof this.props.notifications !== 'undefined') {
            var unread = 0;
            var notifications = objectToArray(this.props.notifications);
            if(notifications.length > 0) {
                unread = notifications.map(function(notification) {
                    return notification.has_read ? 0 : 1;
                }).reduce(function(a, b) {
                    return a + b;
                });

            }

            if(unread > 0) {
                span = <span className="badge notif-circle" >{unread}</span>; 
            }
        }
        return(
            <span className="notif-pos">{span}</span>
        ); 
    }
});

if(typeof document !== 'undefined' && 
    document.getElementById('notification-panel') !== null) {

    ReactDOM.render(
        <NotificationPage type="page" data="notifications" />,
        document.getElementById('notification-panel')
    );
}