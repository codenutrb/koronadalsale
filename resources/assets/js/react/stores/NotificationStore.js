var NotificationStore = alt.createStore({
    displayName: 'notificationStore',

    bindListeners: {
        getNotifications: notificationActions.getNotifications,
        updateNotifications: notificationActions.updateNotifications,
        markAllAsRead: notificationActions.markAllAsRead
    },

    state: {
        notifications: {},
        to: 10,
        limit: 10
    },

    getNotifications: function(notifications) {
        this.setState({
            notifications: notifications['results'],
            myId: notifications['myId']
        });
    },

    updateNotifications: function(notification) {
        var notifications = $.extend(true, {}, this.state.notifications);
        notifications[notification._id] = notification;

        this.setState({
            notifications: notifications
        });
    },

    markAllAsRead: function(updates) {
        var notifications = $.extend(true, {}, this.state.notifications);

        for(var key in notifications) {
            notifications[key].has_read = true;
        }

        this.setState({
            notifications: notifications
        });
    }
});