var MessageStore = alt.createStore({
    displayName: 'messageStore',

    bindListeners: {
        initMessage: messageActions.initMessage,
        getConversations: messageActions.getConversations,
        markAsRead: messageActions.markAsRead,
        updateConversations: messageActions.updateConversations,
        deleteConversation: messageActions.deleteConversation,
        selectConversation: messageActions.selectConversation,
        getMessages: messageActions.getMessages
    },

    state: {
        initial: true,
        conversations: {},
        conversation: {messages:[], user:{}}
    },

    publicMethods: {
        mergeMessages: function(oldMessages, newMessages) {
            oldMessages = objectToArray(oldMessages);
            newMessages = objectToArray(newMessages);

            if(typeof oldMessages.map === 'function') {
                var newMessagesId = newMessages.map(function(message) {
                    return message._id;
                }).sort();

                var toAdd = oldMessages.filter(function(message) {
                    return newMessagesId.indexOf(message._id) < 0;
                });

                return newMessages.concat(toAdd);
            } else {
                return newMessages;
            }
        }
    },

    initMessage: function(payload) {
        this.setState({
            initial: false,
            conversations: payload.conversations,
            me: payload.me,
        });

        if(payload.conversation !== null) {
            this.setState({
                conversation: payload.conversation
            });
        }
    },

    getConversations: function(response) {
        this.setState({
            initial: false,
            conversations: response.conversations,
            me: response.me
        });
    },

    setConversation: function(conversation, setConversation, updateComponent) {
       
        if(conversation._id === this.state.conversation._id ||
            setConversation === true) {

            if(conversation._id == this.state.conversation._id) {
                conversation.messages = MessageStore.mergeMessages(
                    cloneObject(this.state.conversation.messages),
                    cloneObject(conversation.messages)
                );
            }

            conversation.all_messages = false;
            this.setState({
                conversation: conversation,
            });
        }

        var conversations = cloneObject(this.state.conversations);
        conversations[conversation._id] = conversation;

        this.setState({
            conversations: conversations,
            sending: false,
            newMessage: false,
            updateComponent: true
        });

    },

    updateConversations: function(conversation) {
        if(typeof conversation !== 'undefined') {
            this.setConversation(conversation, false);
        }
    },

    markAsRead: function(conversation)  {
        this.setConversation(conversation);
    },

    selectConversation: function(conversation) {
        this.setConversation(conversation, true);
    },

    deleteConversation: function(id) {
        var conversations = cloneObject(this.state.conversations);
        conversations[id].messages = {};

        var conversation = cloneObject(this.state.conversation);
        conversation.messages = {};

        this.setState({
            conversations: conversations,
            conversation: conversation
        });
    },

    getMessages: function(messages) {
        var conversations = cloneObject(this.state.conversations);
        var conversation = cloneObject(this.state.conversation);

        if(messages.length > 0) {
            var newMessages = MessageStore.mergeMessages(
                cloneObject(conversation.messages),
                cloneObject(messages)
            );

            conversation.messages = newMessages;
            conversations[conversation._id].messages = newMessages;
        } else {
            conversation.all_messages = true;
        }

        this.setState({
            conversations: conversations,
            conversation: conversation,
            updateComponent: false
        });
    }

});