// warning: this code is ugly - refractor later

var UserStore = alt.createStore({
    displayName: 'UserStore',

    bindListeners: {
        initUser: userActions.initUser,
        followUser: userActions.followUser,
        getMe: userActions.getMe
    },

    state: {
        user: {},
        me: {},
        myId: ''
    },

    publicMethods: {

        imFollowing: function(id) {
            if(this.getState().me) {
                return (id in this.state.me.following);
            }
        },

        imFollower: function(id) {
            if(this.getState().user) {
                return (id in this.state.user.followers);
            }
            return -1;
        },

    },

    initUser: function(payload) {
        this.setState({
            user: payload.user,
            me: payload.me,
            myId: payload.myId
        });
    },

    getMe: function(me) {
        this.setState({
            me: me
        });
    },

    followUser: function(payload) {
        var me = $.extend({}, this.state.me);
        var user;

        if(payload.user) {
            me.following[payload.id] = payload.user;

            if(payload.id === this.state.user._id) {
                user = $.extend({}, this.state.user);
                user.followers[this.state.me._id] = this.state.me;

                this.setState({
                    user: user
                });
            }
        } else {
            delete me.following[payload.id];

            if(payload.id === this.state.user._id) {
                user = $.extend({}, this.state.user);
                delete user.followers[this.state.me._id];

                this.setState({
                    user: user
                });
            }
        }

        this.setState({
            me: me
        });

        if(this.state.me._id === this.state.user._id) {
            this.setState({
                user: me
            });
        }
    }
});