console.log('Hello there, fellow coder! :)');

window.onload = function() {
    $('#birthdaypicker').datetimepicker({
        format: 'MM/DD/YYYY',
        allowInputToggle: true,
        ignoreReadonly: true,
        icons: {
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
        }
    });
    $("#lightSlider").lightSlider({
        gallery:true,
        thumbItem: 6,
        item:1,
        enableTouch:true,
        controls: false,
        adaptiveHeight: true,
        adaptiveWitdh: true,
        onSliderLoad: function() {
            $('#lightSlider').removeClass('cS-hidden');
        }
    });

    if(typeof $('.msg-wrap').html() !== 'undefined') {
        $('.msg-wrap').scrollTop($('.msg-wrap')[0].scrollHeight);
    }

};
$('#price').maskMoney();
autosize($('textarea'));
//startswith polyfill
if (!String.prototype.startsWith) {
  String.prototype.startsWith = function(searchString, position) {
    position = position || 0;
    return this.indexOf(searchString, position) === position;
  };
}

function share_fb(url) {
  window.open('https://www.facebook.com/sharer/sharer.php?u='+url,'facebook-share-dialog',"width=626,height=436");
}