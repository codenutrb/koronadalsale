angular.module('userController', []).controller('UserController', ['$scope', '$http', '$window', function($scope, $http, $window) {
    $scope.followers = {};
    $scope.following = {};

    $scope.user = {
        location: {
            name: '',
            geometry: {}
        }
    };

    $scope.users = {

    };

    $scope.loadingPlaces = false;

    $scope.followUser = function(id, me, follow) {
        $http.get('/user/follow/' + id).
        success(function(response) {
            console.log(response, $scope.following, $scope.user);
            if(follow === 0) {
                if(response.data.status === 0) {
                    delete $scope.followers[me];
                } else {
                    $scope.followers[me] = response.data.follow;
                }
            } else {
                if(response.data.status === 0) {
                    delete $scope.following[id];
                } else {
                    $scope.following[id] = response.data.follow;
                }
            }
        }).error(function(response, statusCode) {
            if(statusCode == 401) {
                $window.location = '/login';
            }
        });
    };

    var autocomplete, addressComponents = ['city', 'state', 'region', 'country'];

    $window.initAutocomplete = function() {
        if(typeof google === 'object' && typeof google.maps === 'object') {
            console.log('test fillInAddress');
            var input = document.getElementById('glocation');
            autocomplete = new google.maps.places.Autocomplete(input, {
                types: ['(cities)'],
                componentRestrictions: { country: "ph" }
            });

            autocomplete.addListener('place_changed', fillInAddress);

            google.maps.event.addDomListener(input, 'keydown', function(e) {
                if(e.keyCode == 13) {
                    e.preventDefault();
                    return false;
                }
            });

            google.maps.event.addDomListener(input, 'blur', function() {
                var place = autocomplete.getPlace();
                console.log($scope.user.location, $scope.original_location);
                if((place === undefined || $scope.user.location.name !== place['formatted_address']) &&
                    $scope.user.location.name !== $scope.original_location) {
                    clearLocation();
                }
            });
        }
    };

    function fillInAddress() {
        var place = autocomplete.getPlace();

        if('address_components' in place) {
            $scope.user['location']['name'] = place['formatted_address'];
            for(var i = 0; i < addressComponents.length; ++i) {
                $scope.user['location'][addressComponents[i]] = place['address_components'][i]['long_name'];
            }
            $scope.user['location']['geometry']['lat'] = place.geometry.location.lat();
            $scope.user['location']['geometry']['lng'] = place.geometry.location.lng();
             $scope.$apply();
        } else {
            clearLocation();
        }
        console.log($scope.user.location);
    }


    function clearLocation() {
        $scope.user.location = {
            geometry: {},
            name: ''
        };
        $scope.$apply();
    }
}]);