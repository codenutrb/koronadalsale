angular.module('imageController', ['ngFileUpload', 'ngCropper']).controller('ImageController',
    ['$scope', '$http', '$window', '$timeout', 'Upload', 'Cropper',
    function($scope, $http, $window, $timeout, Upload, Cropper) {

    $scope.avatar = {};

    $scope.initModal = function() {
        $scope.processing = false;
        $scope.uploadedAvatar = null;
    };

    $scope.onUpload = function($files, $file, $newFiles) {
        if($file) {
            startUpload();

            $file.upload = Upload.upload({
                url: '/image',
                method: 'POST',
                data: {file: $file}
            });

            $file.upload.then(function(response) {
                $timeout(function() {
                    $scope.avatar = response.data;
                    doneUpload();
                });
            }, function(response) {
                doneUpload();

                if(response.status == 401) {
                    $window.location = '/login';
                } else {
                    $scope.errors = [response.data];
                }

            }, function(evt) {
                $file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };

    $scope.cropImage = function() {
        $scope.processing = true;
        $http.put('/user/avatar/', {
            'image': $scope.avatar.original,
            'height': data.height,
            'width': data.width,
            'x': data.x,
            'y': data.y
        }).success(function(response) {
            $window.location = '/user/' + response['_id'];
        }).error(function(response, statusCode) {
            $scope.processing = false;
            if(statusCode == 401) {
                $window.location = '/login';
            }
        });
    };

    $scope.cropper = {};
    $scope.cropperProxy = 'cropper.first';

    $scope.clear = function(degrees) {
        if(!$scope.cropper.first) return;
        $scope.cropper.first('clear');
    };

    $scope.options = {
        maximize: true,
        aspectRatio: 1 / 1,
        crop: function(dataNew) {
            data = dataNew;
        }
    };

    $scope.showEvent = 'show';
    $scope.hideEvent = 'hide';

    var doneUpload = function() {
        $timeout(showCropper);
        $scope.processing = false;
    };

    var startUpload = function() {
        $scope.avatar = {};
        $scope.errors = null;
        $timeout(hideCropper);
        $scope.processing = true;
    };

    function showCropper() { console.log('show'); $scope.$broadcast($scope.showEvent); }
    function hideCropper() { $scope.$broadcast($scope.hideEvent); }
}]);