angular.module('dropzone', []).directive('dropzone', [function($window) {
    return function(scope, element, attrs) {
        var config, dropzone;

        config = scope[attrs.dropzone];

        dropzone = new Dropzone(element[0], config.options);
        angular.forEach(config.eventHandlers, function(handler, event) {
            dropzone.on(event, handler);
        });

        scope.$watch('post._id', function(newValue, oldValue) {
            if(newValue) {
                var image, images, mockFile;

                images = scope.post.photos;

                angular.forEach(images, function(value, key) {
                    image = '/img/posts/' + newValue + '/' + value;
                    url = 'http://' + window.location.host + image;
                    mockFile = {
                        name: value
                    };
                    dropzone.emit('addedfile', mockFile);
                    dropzone.emit('thumbnail', mockFile, url);
                    dropzone.createThumbnailFromUrl(mockFile, url);
                    dropzone.emit('complete', mockFile);
                });

                dropzone.options.maxFiles -= images.length;
            }
        });
    };
}]);
