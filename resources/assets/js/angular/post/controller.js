angular.module('postController', ['ngFileUpload']).controller('PostController',
    ['$scope', '$http', '$window', '$timeout', 'Upload',
    function($scope, $http, $window, $timeout, Upload) {

    $scope.files = [];

    $scope.post = {
        photos: [],
        location: {
            geometry: {}
        }
    };

    $scope.selectPhotos = function(index) {
        if(index === $scope.files.length) {
            $timeout(function() {
                angular.element('#inputFiles').trigger('click');
            });
        }
    };

    $scope.addPhotos = function($files, $file, $newFiles) {
        $scope.files = $scope.files.concat($newFiles).filter(function(obj) {
            return obj;
        });

        if($scope.files.length > 6) {
            $scope.files = $scope.files.slice(0, 6);
        }

        uploadFiles($newFiles);
    };

    $scope.removePhoto = function(index) {
        $scope.files.splice(index, 1);
    };

    $scope.save = function() {
        fillPhotos();
        $('#submit-button').button('loading');

        $http.post('/post', $scope.post).
        success(function(response) {
            $('#submit-button').button('reset');
            $window.location = '/post/' + response['_id'] + '/' + response['title'];
        }).
        error(function(response, statusCode) {
            $('#submit-button').button('reset');
            if(statusCode == 422) {
                $scope.errors = response;
            } else if(statusCode == 401) {
                $window.location = '/login/';
            }
        });
    };

    $scope.update = function() {
        fillPhotos();
        $('#submit-button').button('loading');

        $http.post('/post/update', $scope.post).
        success(function(response) {
            $('#submit-button').button('reset');
            $window.location = '/post/' + response['_id'] + '/' + response['title'];
        })
        .error(function(response, statusCode) {
            $('#submit-button').button('reset');
            if(statusCode == 422) {
                $scope.errors = response;
            } else if(statusCode == 401) {
                $window.location = '/login/';
            }
        });
    };

    $scope.likePost = function(postId) {
        $http.post('/post/like', {'_id': postId}).
        success(function(response) {
            $scope.like = response;
        }).
        error(function(response, statusCode) {
            if(statusCode == 422) {
                $scope.errors = response;
            } else if(statusCode == 401) {
                $window.location = '/login/';
            }
        });
    };

    var uploadFiles = function(files) {
        if(files) {
          var filesCount = files.length;
          var alreadDone = 0;
          $('#submit-button').button('loading');

          angular.forEach(files, function(file) {
              var promise = new Promise(function(resolve, reject) {
                $scope.uploadFile(file, resolve, reject);
              });
              promise.then(function(val) {
                filesCount -= 1;
                stillUploading(filesCount);
              });
          });
        }
    };

    var stillUploading = function(filesCount) {
      if(filesCount <= 0) {
        $('#submit-button').button('reset');
      }
    }

    $scope.uploadFile = function(file, resolve, reject) {
        delete file.result;

        file.upload = Upload.upload({
            url: '/image',
            method: 'POST',
            data: {file: file}
        });

        file.upload.then(function(response) {
            $timeout(function() {
                file.result = response.data;
                resolve('successful');
            });
        }, function(response) {
            file.result = {'errors': response.data };
            console.log(file.result['error']);
            resolve('error');
            if(response.status === 401) {
                $window.location = '/login/';
            }
        }, function(evt) {
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    var fillPhotos = function() {
        $scope.post.photos = [];
        for(var file in $scope.files) {
            if($scope.files[file].result &&
                $scope.files[file].result.original) {

                $scope.post.photos.push($scope.files[file].result);
            }
        }
    };

    var autocomplete, addressComponents = ['city', 'state', 'region', 'country'];

    (function initAutocomplete() {
        if(typeof google === 'object' && typeof google.maps === 'object') {
            var input = document.getElementById('glocation');
            autocomplete = new google.maps.places.Autocomplete(input, {
                types: ['(cities)'],
                componentRestrictions: { country: "ph" }
            });

            autocomplete.addListener('place_changed', fillInAddress);
            google.maps.event.addDomListener(input, 'keydown', function(e) {
                if(e.keyCode == 13) {
                    e.preventDefault();
                    return false;
                }
            });
            google.maps.event.addDomListener(input, 'blur', function() {
                var place = autocomplete.getPlace();
                if((place === undefined || $scope.post.location.name !== place['formatted_address']) &&
                    $scope.post.location.name !== $scope.original_location) {
                    clearLocation();
                }
            });
        }
    })();

    function fillInAddress() {
        var place = autocomplete.getPlace();

        if('address_components' in place) {
            $scope.post['location']['name'] = place['formatted_address'];
            for(var i = 0; i < addressComponents.length; ++i) {
                $scope.post['location'][addressComponents[i]] = place['address_components'][i]['long_name'];
            }
            $scope.post['location']['geometry']['lat'] = place.geometry.location.lat();
            $scope.post['location']['geometry']['lng'] = place.geometry.location.lng();
        } else {
            clearLocation();
        }
    }

    function clearLocation() {
        $scope.post.location = {
            geometry: {},
            name: ''
        };
        $scope.$apply();
    }

}]);
