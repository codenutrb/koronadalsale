
var app = angular.module('app', ['ngRoute',
                                 'ngAnimate',
                                 'angular-loading-bar',
                                 'angularMoment',
                                 'postController',
                                 'commentController',
                                 'imageController',
                                 'userController'
                                ]);

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
}]);

app.filter('joinBy', function() {
    return function(input, delimiter) {
        if(input instanceof Array) {
            return (input || []).join(delimiter || '<br>');
        } else {
            return input;
        }
    };
});

app.filter('range', function() {
    return function(input, total) {
        total = parseInt(total);
        for(var i = 0; i < total; i++) {
            input.push(i);
        }
        return input;
    };
});

app.filter('keys', function() {
    return function(input) {
        if(typeof input === 'object') {
            return Object.keys(input);
        }
    };
});

app.filter('indexOf', function() {
    return function(input, value) {
        if(typeof Array.prototype.indexOf === 'function') {
            return input.indexOf(value);
        } else {
            var i = -1, index = -1;

            for(i = 0; i < input.length; i++) {
                if(input[i] === value) {
                    index = i;
                    break;
                }
            }

            return index;
        }
    };
});