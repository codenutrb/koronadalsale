angular.module('commentController', []).controller('CommentController', ['$scope', '$http', '$window', function($scope, $http, $window) {
    var initialResult = 5;
    
    $scope.comment = {};
    $scope.comments = [];

    $scope.paginate = {
        perpage: initialResult,
        results: initialResult
    };

    $scope.save = function() {
        $http.post('/comment', $scope.comment).
        success(function(response) {
            $scope.comments = response;
            $scope.comment.body = null;
            $scope.paginate.results = initialResult;
        }).
        error(function(response, statusCode) {
            $scope.errors = response;
            if(statusCode == 401) {
                 $window.location = '/login/';
            }
        });
    };
}]);