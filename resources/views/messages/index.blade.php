@extends('layouts.master')

@section('title', 'Inbox')

@section('content')
    {!! React::render('MessageComponent', ['conversations' => $conversations, 'conversation' => $conversation,  'me' => $me, 'messagingUrl' => $messaging_url], ['class' => 'row'])  !!}
@endsection
