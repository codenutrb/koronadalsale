@extends('layouts.master')

@section('title', 'Sign In')

@section('content')
<section class="access">
    <br>
    <div class="container no-side-padding">
         <div class="col-lg-5 col-md-5 content-inner-detail no-side-padding">
            <form role="form" action="{!! URL::to('password/reset/') !!}" method="POST">
                {!! Form::token() !!}
                <input type="hidden" name="token" value="{!! $token !!}">
                <legend>Get new password</legend>
                @include('components.errors') 
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control input-lg" id="email" name="email" placeholder="Email" value="{!! old('email') !!}" required>
                </div>
                <div class="form-group">
                    <label for="email">Password</label>
                    <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <label for="password">Confirm Password</label>
                    <input type="password" class="form-control input-lg" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Reset password</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection