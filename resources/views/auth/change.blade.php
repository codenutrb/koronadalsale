@extends('user.profile')

@section('profile-content')
<div class="col-xs-12 col-sm-8 col-md-9 no-side-padding">
    <form class="form-horizontal" action="{!! URL::to('password/change/') !!}" method="POST">
        <div class="col-xs-12 col-sm-12 col-md-12">
            @include('components.errors')
        </div>
        <input type="hidden" name="_id" value="{{ $user['id'] }}">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-profile no-side-padding">
                <div class="panel-heading overflow-h">
                    <h2 class="panel-title heading-sm pull-left"><i class="fa fa-lock"></i> Change Password</h2>
                </div>
               
                <div class="panel-body">
                     @if(!Request::session()->get('user')['requires_change_password'])
                     <div class="form-group  @if($errors && $errors->has('old_password')) {{ 'has-error' }} @endif">
                        <label for="old_password" class="col-md-4 control-label">Old Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="old_password" required>
                        </div>
                    </div>
                    @endif
                    <div class="form-group @if($errors && $errors->has('password')) {{ 'has-error' }} @endif">
                        <label for="name" class="col-md-4 control-label">New Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password" required>
                        </div>
                    </div>
                    <div class="form-group @if($errors && $errors->has('password')) {{ 'has-error' }} @endif">
                        <label for="password_confirmation" class="col-md-4 control-label">Confirm New Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-profile no-side-padding">
                <div class="panel-heading overflow-h">
                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
            </div>
        </div>
    <form>
</div>
@endsection