@extends('layouts.master')

@section('title', 'Sign In')

@section('content')
<div class="container no-side-padding">
    @if(session('message'))
    <div class="alert alert-info">
        {!! session('message') !!}
    </div>
    @else
    <div class="col-lg-5 col-md-5 content-inner-detail no-side-padding">
        <form role="form" action="{!! URL::to('password/email') !!}" method="POST">
            {!! Form::token() !!}
            <legend>Forgot your password?</legend>
            @include('components.errors') 
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control input-lg" id="email" name="email" placeholder="Email" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Send</button>
            </div>
        </form>
    </div>
    @endif
</div>
@endsection