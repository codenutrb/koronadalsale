@extends('layouts.master')

@section('title', 'Sign In')

@section('content')
<div class="container no-side-padding">
    <div class="col-lg-5 col-md-6 col-sm-8 content-inner-detail no-side-padding">
        <form role="form" action="{!! URL::to('login/') !!}" method="POST">
            {!! csrf_field() !!}
            <legend>Log in</legend>
            @include('components.errors') 
            @include('components.messages') 
            <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control input-lg" id="email" value="{!! old('email') !!}" name="email" placeholder="Email" required autofocus>
            </div>
            <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Log in</button>
            </div>
            <div class="form-group">
                <a class="btn btn-success btn-lg btn-block " href="{!! URL::to('fb_login/') !!}">
                    <i class="fa fa-facebook-official btn-icon">&nbsp;&nbsp;</i>Login with Facebook
                </a>
            </div>
            <a href="{!! URL::to('password/email') !!}" class="btn btn-link">Forgot your password?</a>
        </form>
    </div>
</div>
@endsection