<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="robots" content="index, follow" />  
    
    @yield('meta')
    <title>Koronadal Sale - @yield('title')</title>
    <!--<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>-->
    <!-- styles -->
    <link rel="stylesheet" type="text/css" href="{{Cdn::asset('stylesheets/vendor.css')}}">
    <link rel="stylesheet" type="text/css" href="{{Cdn::asset('stylesheets/styles.css')}}">
    
    @if(env('APP_ENV') !== 'local')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    @else
    <link rel="stylesheet" type="text/css" href="{{Cdn::asset('stylesheets/font-awesome.min.css')}}">
    @endif

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{Html::script('/js/html5shiv.js')}}"></script>
    <![endif]-->

    <!-- fav and touch icons -->
    <link rel="shortcut icon" href="#" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="#" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="#" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="#" />
    <link rel="apple-touch-icon-precomposed" href="#" />

</head>
<body>
    <div class="container container-body">
        @include('components.header') 

        <div class="contents body">
            @yield('content')
        </div>

        <div> 
            <hr/> 
            @include('components.footer')
        </div>
    </div>
</body>

@if(env('APP_ENV') !== 'local')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
@else
<script type="text/javascript" src="{{Cdn::asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{Cdn::asset('js/bootstrap.min.js')}}"></script>
@endif

<script type="text/javascript" src="{{Cdn::asset('js/react.js')}}"></script>
<script type="text/javascript" src="{{Cdn::asset('js/vendor.js')}}"></script>
<script type="text/javascript" src="{{Cdn::asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{Cdn::asset('js/components.js')}}"></script>

@if(!str_contains(Request::url(), '/conversations'))
<script type="text/javascript" src="{{Cdn::asset('js/angular.js')}}"></script>
@endif

@yield('js')
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-71444027-1', 'auto');
  ga('send', 'pageview');
</script>
</html>