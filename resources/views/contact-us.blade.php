@extends('layouts.master')

@section('title', 'Contact Us')

@section('content')
<div class="container no-side-padding">
    <div class="col-lg-8 col-md-8 col-sm-8 content-inner-detail no-side-padding">
        <form role="form" action="{!! URL::to('contact-us/') !!}" method="POST">
            {!! csrf_field() !!}
            <legend>Contact Us</legend>
            @include('components.errors') 
            @include('components.messages') 
            <div class="form-group">
                <label for="name">Name</label>
                    <input type="text" class="form-control input-lg" id="name" value="{!! old('name') !!}" name="name" placeholder="Name" required autofocus>
                </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control input-lg" id="email" value="{!! old('email') !!}" name="email" placeholder="Email" required>
            </div>
              <div class="form-group">
                <label for="body">Message</label>
                <textarea class="form-control input-lg" id="body" name="body" placeholder="Message" rows="6" required>{!! old('body') !!}</textarea>
            </div>
            <div class="form-group">
                <div class="g-recaptcha" data-sitekey="6LfbsRQTAAAAAAt-Fqk1F6J0K5jKP3HyMzskayuv"></div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection