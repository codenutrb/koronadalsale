@extends('user.profile')

@section('profile-content')
<div class="col-xs-12 col-sm-8 col-md-9 no-side-padding" ng-init="initAutocomplete()" ng-cloak>
    <form class="form-horizontal" action="{!! URL::to('user/edit/') !!}" method="POST">
        <input type="hidden" ng-init="user.location={{ json_encode($user['location']) }}">
        <input type="hidden" ng-value="user['location']['state']" name="location[state]">
        <input type="hidden" ng-value="user['location']['city']" name="location[city]">
        <input type="hidden" ng-value="user['location']['region']" name="location[region]">
        <input type="hidden" ng-value="user['location']['geometry']['lat']" name="location[geometry][lat]">
        <input type="hidden" ng-value="user['location']['geometry']['lng']" name="location[geometry][lng]">
        <input type="hidden" ng-init="original_location=user['location']['name']" />
        <div class="col-xs-12 col-sm-12 col-md-12">
            @include('components.errors')
        </div>
        <input type="hidden" name="_id" value="{{ $user['id'] }}">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-profile no-side-padding">
                <div class="panel-heading overflow-h">
                    <h2 class="panel-title heading-sm pull-left"><i class="fa fa-user"></i> Basic Information</h2>
                </div>
               
                <div class="panel-body">
                     <div class="form-group  @if($errors && $errors->has('name')) {{ 'has-error' }} @endif">
                        <label for="name" class="col-md-2 control-label">Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ $user['name'] }}" name="name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-md-2 control-label">Birthday</label>
                        <div class="col-md-6">
                            <div class="input-group date col-md-12" id="birthdaypicker">
                                <input type="text" class="form-control" value="{{ $user['birthday'] }}" name="birthday">
                                 <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-md-2 control-label">Location</label>
                        <div class="col-md-6">
                            <div class="col-md-12 no-side-padding" ng-class="loadingPlaces ? 'input-group' : ''">
                                <input type="text" id="glocation" class="form-control" name="location[name]" ng-model="user.location.name">
                                <span class="input-group-addon" ng-show="loadingPlaces">
                                    <i class="fa fa-spinner fa-spin"></i> 
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-md-2 control-label">Gender</label>
                        <div class="col-md-6"> 
                            <select class="form-control" value="{{ $user['gender'] }}" name="gender">
                                <option value="">Please select</option>
                                <option value="male" @if($user['gender'] == 'male') selected @endif>Male</option>
                                <option vaule="female" @if($user['gender'] == 'female') selected @endif>Female</option>
                                <option value="others" @if($user['gender'] == 'others') selected @endif>Others</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-profile no-side-padding">
                <div class="panel-heading overflow-h">
                    <h2 class="panel-title heading-sm pull-left"><i class="fa fa-phone-square"></i> Contacts Details</h2>
                </div>
                <div class="panel-body">
                     <div class="form-group @if($errors && $errors->has('email')) {{ 'has-error' }} @endif">
                        <label for="name" class="col-md-2 control-label">Email</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" value="{{ $user['email'] }}" name="email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-md-2 control-label">Phone</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ $user['phone'] }}" name="phone" >
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-profile no-side-padding">
                <div class="panel-heading overflow-h">
                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
            </div>
        </div>
    <form>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ-B9onE9ONHRJUV4ech1a3SGrZYY0C_s&signed_in=true&libraries=places&callback=initAutocomplete" async defer></script>
@endsection