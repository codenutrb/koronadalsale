@extends('layouts.master')

@section('title', 'User Profile')

@section('content')
<div class="row" ng-controller="UserController">
    @include('user.sidebar')
    @yield('profile-content')
</div>
<div class="modal fade" id="update-avatar-modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" ng-controller="ImageController">
    <div class="modal-dialog" role="document">
        <fieldset ng-disabled="processing">
        <div class="modal-content">
            <div class="modal-body">
                @include('components.ajax-errors')
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="btn btn-default btn-upload col-xs-12 col-sm-12 col-md-12" 
                             ngf-select 
                             ngf-pattern="'image/*'"
                             ngf-fix-orientation="true"
                             accept="image/*" 
                             ngf-change="onUpload($files, $file, $newFiles)"
                             ngf-validate="{size: {max: '10MB'} }"
                             ngf-max-size="10MB">

                            <i ng-class="processing ? 'fa fa-spinner fa-spin': 'fa fa-plus'"></i> 
                            <span ng-if="processing"> Uploading
                            </span>
                            <span ng-if="!processing"> Upload 
                            </span>
                        </div>   
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                         <button class="btn btn-primary btn-block" type="button" ng-disabled="!avatar.original" ng-click="cropImage()"> <i class="fa fa-save"></i> Save </button>  
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <button class="btn btn-success btn-block" type="button" data-dismiss="modal"> <i class="fa fa-ban"></i> Cancel </button> 
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 crop">
                        <div class="cropArea">
                            <img ng-if="avatar.original" ng-src="@{{avatar.original}}" 
                                 ng-cropper
                                 ng-cropper-proxy="cropperProxy"
                                 ng-cropper-show="showEvent"
                                 ng-cropper-hide="hideEvent"
                                 ng-cropper-options="options" />
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        </fieldset>
    </div>
</div>
@endsection

