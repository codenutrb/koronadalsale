@extends('user.profile')

@section('title', 'User connections')

@section('profile-content')
    {!! React::render('UserList', ['page' => Request::session()->get('profile-active-menu')]) !!}
@endsection

