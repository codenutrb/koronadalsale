<div>
    {!! React::render('User', [ 'user' => $user, 'me' => isset($me) ? $me : null, 'url' => Request::url(), 'myId' => Request::session()->get('user')['_id']]) !!}
    <div class="col-xs-12 col-sm-4 col-md-3">
        <div class="profile-sidebar" id="profile-pic">
            <!-- SIDEBAR USERPIC -->
            <div class="media-object thumbnail profile-userpic my-thumbnail">
                @if($user['avatar_original'])
                {!! Html::image($user['avatar_original']) !!} 
                @else
                {!! Html::image('img/avatar-default.png') !!} 
                @endif
                @if(Request::session()->get('user')['_id'] === $user['_id'])
                <div class="easy-block-v1-badge update-profile-pic" ><a href="#" data-toggle="modal" data-target="#update-avatar-modal" ng-click="initModal()">Change Profile Picture</a></div>
                @endif
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                    <a href="{!! URL::to('user/' . $user['_id'] . '/posts') !!}">{{ $user['name'] }}</a>
                </div>
            </div>
            <!-- END SIDEBAR USER TITLE -->
            <!-- SIDEBAR BUTTONS -->
            <div class="profile-userbuttons" >
                @if(Request::session()->get('user')['_id'] == $user['_id'])
                    <a href="{!! URL::to('conversations/') !!}" type="button" class="btn btn-primary btn-block">Messages <span id="message-count"><span></a>
                    <a href="{!! URL::to('user/edit') !!}" class="btn btn-primary btn-block">Update Profile</a>
                    <a href="{!! URL::to('password/change') !!}" class="btn btn-primary btn-block">Change password</a>
                @else
                    {!! React::render('FollowButton', ['user' => $user, 'type' => 'sidebar', 'myId' => Request::session()->get('user')['_id']], ['tag' => 'react']) !!}
                    <a href="{!! URL::to('conversations/all/' . $user->_id) !!}" type="button" class="btn btn-primary btn-block">Message</a>
                @endif
            </div>
            <!-- END SIDEBAR BUTTONS -->
            <!-- SIDEBAR MENU -->
            <input type="hidden" ng-model="profile_active_menu" ng-init="profile_active_menu='{{Request::session()->get('profile-active-menu')}}'" />
            <div class="profile-usermenu">
                <ul class="nav">
                    <li ng-class="profile_active_menu == 'posts' ? 'active' : ''">
                        <a href="{{ URL::to('user/' . $user['id'] . '/posts') }}">
                            <i class="fa fa-reorder"></i>
                            Posts 
                        </a>
                    </li>
                    <li ng-class="profile_active_menu == 'profile' ? 'active': ''">
                        <a href="{{ URL::to('user/' . $user['_id']) }}">
                            <i class="fa fa-user"></i>
                            Profile
                        </a>
                    </li>
                    <li ng-class="profile_active_menu == 'followers' ? 'active': ''">
                        <a href="{!! URL::to('/user/' . $user['_id'] . '/followers') !!}">
                            <i class="fa fa-user-plus"></i>
                            Followers {!! React::render('FollowCount', ['type' => 'followers', 'page' => Request::session()->get('profile-active-menu')], ['tag' => 'span']) !!}
                        </a>
                    </li>
                    <li ng-class="profile_active_menu == 'following' ? 'active': ''">
                        <a href="{!! URL::to('/user/' . $user['_id'] . '/following') !!}">
                            <i class="fa fa-users"></i>
                            Following {!! React::render('FollowCount', ['type' => 'following', 'page' => Request::session()->get('profile-active-menu')], ['tag' => 'span']) !!}
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END MENU -->
        </div>
    </div>
</div>

