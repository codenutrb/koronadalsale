@extends('user.profile')

@section('profile-content')
<div class="col-xs-12 col-sm-8 col-md-9 no-side-padding">
    <div class="col-xs-12 col-sm-12 col-md-12">
        @include('components.messages')
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-profile no-side-padding">
            <div class="panel-heading overflow-h">
                <h2 class="panel-title heading-sm pull-left"><i class="fa fa-user"></i> Basic Information</h2>
            </div>
            <div class="panel-body">
                 <ul class="list-unstyled social-contacts-v2">
                    <li><span class="label-name">Name</span>&nbsp;{{ $user['name'] }}</li>
                    <li><span class="label-name">Birthday</span>&nbsp;{{ $user['birthday'] }} </li>
                    <li><span class="label-name">Location</span>&nbsp;{{ $user['location.name'] }} </li>
                    <li><span class="label-name">Gender</span>&nbsp;{{ studly_case($user['gender']) }} </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-profile no-side-padding">
            <div class="panel-heading overflow-h">
                <h2 class="panel-title heading-sm pull-left"><i class="fa fa-phone-square"></i> Contacts Details</h2>
            </div>
            <div class="panel-body">
                 <ul class="list-unstyled social-contacts-v2">
                    <li><i class="fa fa-envelope label-name"></i>  {{ $user['email'] }}</li>
                    <li><i class="fa fa-phone label-name"></i> {{ $user['phone'] }} </li>
                </ul>
            </div>
        </div>
    </div> 
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-profile no-side-padding">
            <div class="panel-heading overflow-h">
                <h2 class="panel-title heading-sm pull-left"><i class="fa fa-history"></i> History</h2>
            </div>
            <div class="panel-body">
                 <ul class="list-unstyled social-contacts-v2">
                    <li><span class="label-name">Joined</span>&nbsp;{{ (new Date($user['created_at']))->format('F d, Y') }} </li>
                    <li><span class="label-name">Last Activity</span>&nbsp;{{ (new Date($user['updated_at']))->format('F d, Y') }} </li>
                </ul>
            </div>
        </div>
    </div> 
   
</div>
@endsection