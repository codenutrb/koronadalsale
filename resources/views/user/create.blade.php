@extends('layouts.master')

@section('title', 'Register new user')

@section('content')
<div class="container no-side-padding">
    <div class="col-lg-5 col-md-6 col-sm-8 content-inner-detail no-side-padding">
        <form role="form" action="{!! URL::to('user/') !!}" method="POST" class="no-side-padding">
            <legend>Sign up</legend>
            @include('components.errors') 
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control input-lg" id="email" name="email" placeholder="Email" value="{!! old('email') !!}" email required autofocus>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password" required>
            </div>
            <div class="form-group">
                <label for="fullname">Full Name</label>
                <input type="text" class="form-control input-lg" id="name" name="name" placeholder="Full Name" value="{!! old('name') !!}" required>
            </div>
            <!--<div class="checkbox">
                <label>
                  <input type="checkbox" required> Accept to our terms & condition
                </label>
            </div>-->
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Sign up</button>
            </div>
            <div class="form-group">
                <a class="btn btn-success btn-lg btn-block " href="{!! URL::to('fb_login/') !!}">
                    <i class="fa fa-facebook-official btn-icon">&nbsp;&nbsp;</i>Sign up with Facebook
                </a>
            </div> 
        </form>
    </div>
</div>
@endsection