@if ($errors !== null && count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        @foreach($errors->all() as $error)                    
            @if(is_array($error))
                {!! join('<br>', $error) !!}
            @else
                {!! $error !!}
            @endif
            <br>
        @endforeach
    </div>
@endif