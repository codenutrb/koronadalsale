<div class="alert alert-danger" role="alert" ng-show="errors" ng-cloak>
    <section ng-repeat="error in errors">
        {{ error | joinBy }}
    </section>
</div>
