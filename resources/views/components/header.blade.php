<nav class="navbar navbar-custom navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            @if(Request::session()->has('user'))
                <?php $_user = Request::session()->get('user') ?>
                {!! React::render('GetNotifications', ['notificationsUrl' => env('NOTIFICATIONS_URL'), 'myId' => $_user->_id]) !!}
                {!! React::render('GetMessageNotifications', ['messagingUrl' => env('MESSAGING_URL'), 'me' => $_user->shallowUser() ]) !!}
            @endif
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-menus" aria-expanded="false" aria-contros="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if(Request::session()->has('user'))
                <a id="notification-btn" class="navbar-toggle btn-primary notif-popover visible-xs mobile-header-button" >
                    {!! React::render('Notification', ['notification_src' => '#notification-btn', 'type' => 'normal'], ['tag' => 'span']) !!}
                </a>
                <a id="message-notification-btn" class="navbar-toggle btn-primary notif-popover visible-xs mobile-header-button" type="button">
                    {!! React::render('MessageNotification', ['notification_src' => '#message-notification-btn', 'type' => 'normal'], ['tag' => 'span']) !!}
                </a>
            @endif
            
            <a class="navbar-brand" href="{!! URL::to('/') !!}">Koronadal Sale</a>
        </div>
        <div class="col-lg-6 col-md-5 col-sm-3 col-xs-12 no-side-padding collapse header-search pull-left navbar-menus">
            <form action="{!! URL::to('/posts/search/') !!}" method="POST" class="navbar-form navbar-left full-width" role="search">
                @if(isset($category))
                <input type="hidden" name="category" value="{!! $category !!}" />
                @else
                <input type="hidden" name="category" value="all" />
                @endif
                <div class="input-group full-width" >
                    <input type="text" id="searchInput" class="form-control" name="searchText" value="{{ isset($searchText) ? $searchText : '' }}" placeholder="Search items" >
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">
                            <span class="fa fa-search"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>

        <div id="navbar" class="navbar-collapse collapse navbar-menus">
            
            <ul class="nav navbar-nav navbar-right header-right-margin">
                <li class="divider-vertical"></li>
                <li><a href="{!! URL::to('posts/search/all') !!}">All items</a></li>
                <li class="divider-vertical"></li>
                <li>
                    <a href="{!! URL::to('post/new') !!}">
                        Post an item
                    </a>
                </li>
                <li class="divider-vertical"></li>
                @if(Request::session()->has('user'))
                    <li class="hidden-xs">
                        <a class="nav-link notif-popover" id="message-notification-link" role="button">
                            {!! React::render('MessageNotification', ['notification_src' => '#message-notification-link', 'type' => 'normal'], ['tag' => 'span']) !!}
                        </a>
                    </li>
                    <li class="divider-vertical"></li>
                    <li class="hidden-xs">
                        <a class="nav-link notif-popover" id="notification-link" role="button" >
                            {!! React::render('Notification', ['notification_src' => '#notification-link', 'type' => 'normal'], ['tag' => 'span']) !!}
                        </a>
                    </li>
                    <li class="divider-vertical"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-lg fa-user hidden-xs"></i> <span class="visible-xs">{{ Request::session()->get('user')['name'] }}<span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{!! URL::to('user/' . Request::session()->get('user')['_id']) !!}"><i class="fa fa-user"></i> Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="{!! URL::to('logout/') !!}"><i class="fa fa-power-off"></i> Logout</a></li> </ul> </li>
                @else
                    <li class="@if(Request::session()->get('active_header') === 'login') {!! 'active' !!} @endif"><a href="{!! URL::to('login/') !!}">Log in</a></li>
                    <li class="divider-vertical"></li>
                    <li class="@if(Request::session()->get('active_header') === 'register') {!! 'active' !!} @endif"><a href="{!! URL::to('user/create') !!}">Register</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
