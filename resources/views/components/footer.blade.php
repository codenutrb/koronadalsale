<!-- the footer -->
<footer>
        <div class="col-xs-8 col-sm-8 col-md-8 no-side-padding">
            <a class="dark-link" href="{!! URL::to('contact-us') !!}">Contact Us</a>
            &nbsp;|&nbsp;
            <a class="dark-link" href="{!! URL::to('terms-and-conditions') !!}">Terms and Conditions</a>
        </div>
        <div class="col-xs-4 col-xs-4 col-md-4 no-side-padding hidden-xs">
            <p class="pull-right">Copyright &copy; Koronadal Sale 2015</p>
        </div>
</footer>