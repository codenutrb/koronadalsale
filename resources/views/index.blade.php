@extends('layouts.master')

@section('title', 'Online buy and sell')

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="col-md-12">
            <h5>
                Latest posts 
            </h5>
        </div> 
        <div class="col-md-12 post-carousel-container">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    @for($i = 0; $i < count($posts); ++$i) 
                    @if($i === 0)
                    <li data-target="#myCarousel" data-slide-to="{{$i}}" class="active"></li>
                    @else
                    <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
                    @endif
                    @endfor
                </ol>

                <div class="carousel-inner post-carousel center" role="listbox">
                    @for($i = 0; $i < count($posts); ++$i)
                    @if($i === 0)
                    <div class="item active post-carousel-item center">
                        <a class="white-link" href="{!! URL::to('post/' . $posts[$i]['_id']) . '/' . str_slug($posts[$i]['title']) !!}">
                        @if(count($posts[$i]['photos']) > 0)
                            <img class="center-image" src="{!! $posts[$i]['photos'][0]['original'] !!}">
                        @else
                            <img class="center-image" src="/img/no-image.gif" />
                        @endif
                        </a>
                         <div class="carousel-caption">
                            <a class="white-link" href="{!! URL::to('post/' . $posts[$i]['_id']) . '/' . str_slug($posts[$i]['title']) !!}">
                                <h3>{!! $posts[$i]['title'] !!}</h3>
                            </a>
                            <a class="white-link" href="{!! URL::to('user/' . $posts[$i]['user']['_id'] . '/posts') !!}">
                                <p>by: {!! $posts[$i]['user']['name'] !!} </p>
                            </a>
                        </div>
                    </div>
                    @else
                    <div class="item post-carousel-item center">
                        <a class="white-link" href="{!! URL::to('post/' . $posts[$i]['_id']) . '/' . str_slug($posts[$i]['title']) !!}">
                        @if(count($posts[$i]['photos']) > 0)
                            <img class="center-image" src="{!! $posts[$i]['photos'][0]['original'] !!}">
                        @else
                            <img class="center-image" src="/img/no-image.gif" />
                        @endif
                        </a>
                        <div class="carousel-caption">
                             <a class="white-link" href="{!! URL::to('post/' . $posts[$i]['_id']) . '/' . str_slug($posts[$i]['title']) !!}">
                                <h3>{!! $posts[$i]['title'] !!}</h3>
                            </a>
                            <a class="white-link" href="{!! URL::to('user/' . $posts[$i]['user']['_id'] . '/posts') !!}">
                                <p>by: {!! $posts[$i]['user']['name'] !!} </p>
                            </a>
                        </div>
                    </div>
                    @endif
                    @endfor
                </div>

            </div>
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="col-md-12">
            <h5>
                Browse by category 
            </h5>
        </div> 
        <div class="col-md-12 no-side-padding">
        @foreach($categories as $key => $_category) 
            <div class="col-xs-6 col-sm-6 col-md-4 post-container no-right-padding">
                <a class="dark-link" href="{!! URL::to('posts/search/' . $key) !!}">
                    <div class="posts-content no-border-radius">
                        <div class="post">
                            <div class="center">
                                <i class="fa fa-4x fa-{{$categoryIcons[$key]}}"></i>
                            </div>
                            <div class="post-title center">
                                <h4 class="red-link">
                                    {{ $_category }}
                                </h4>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
        </div>
    </div>
</div>
@endsection