@extends('layouts.master')

@section('title', 'Add new listing')

@section('content')
<div class="row" ng-controller="PostController">
    <div class="col-xs-12 col-sm-12 col-md-12 no-side-padding">
      
        <div class="col-xs-12 col-sm-12 col-md-6" >
            <div class="panel panel-profile no-side-padding" >
                <div class="panel-heading overflow-h">
                    <h2 class="panel-title heading-sm pull-left"><i class="fa fa-photo"></i> Photos</h2>
                </div>
                @include('posts.upload-photos') 
            </div>
        </div>
      
        <form ng-submit="save()">
            <input type="hidden" id="photos" name="photos" ng-model="post.photos">
            <input type="hidden" id="city" name="location[city]" ng-model="post.location.city">
            <input type="hidden" id="state" name="location[state]" ng-model="post.location.state">
            <input type="hidden" id="state" name="location[region]" ng-model="post.location.region">
            <input type="hidden" id="state" name="location[country]" ng-model="post.location.country">

            <input type="hidden" id="lat" name="location[geometry][lat]" ng-model="post.location.geometry.lat">
            <input type="hidden" id="lng" name="location[geometry][lng]" ng-model="post.location.geometry.lng">

            <div class="col-xs-12 col-sm-12 col-md-6">

                <div class="panel panel-profile no-side-padding">
                    <div class="panel-heading overflow-h">
                        <h2 class="panel-title heading-sm pull-left"><i class="fa fa-archive"></i> Post a new item</h2>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-sm-12 col-md-12 no-side-padding">
                            @include('components.ajax-errors')
                        </div>
                        <div class="form-group" ng-class="(errors && errors['title']) ? 'has-error' : ''">
                            <label for="name" class="control-label">Title</label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}" ng-model="post.title" required>
                        </div>
                        <div class="form-group" ng-class="(errors && errors['category']) ? 'has-error' : ''">
                            <label for="name" class="control-label" >Category</label>
                            {!! Form::select('category', $categories, old('category'), ['class' => 'form-control', 'required', 'ng-model' => 'post.category']) !!}
                        </div>
                        <div class="form-group" ng-class="(errors && errors['price']) ? 'has-error' : ''">
                            <label for="name" class="control-label">Price</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon1">&#8369;</span>
                                <input type="text" class="form-control" name="price" value="{{ old('price') }}" id="price" required ng-model="post.price">
                                <span class="input-group-addon" id="sizing-addon1">PHP</span>
                            </div>
                        </div>
                        <div class="form-group" ng-class="(errors && errors['glocation']) ? 'has-error' : ''">
                            <label for="name" class="control-label">City / Location</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="glocation" id="glocation" autocomplete = "off" required ng-model="post.location.name">
                            </div>
                        </div>
                        <div class="form-group" ng-class="(errors && errors['description']) ? 'has-error' : ''">
                            <label for="name" class="control-label">Description</label>
                            <textarea class="form-control" name="description" rows="10" value="{{ old('description') }}" ng-model="post.description"></textarea>
                        </div>
                    </div>
                </div>
                 <div class="col-xs-12 col-sm-12 col-md-12 no-side-padding">
                <div class="panel panel-profile no-side-padding">
                    <div class="panel-heading overflow-h">
                        <button data-loading-text="Loading..." id="submit-button" class="btn btn-primary btn-block" type="submit">Submit</button>
                    </div>
                </div>
            </div>
            </div>
            
           
        </form>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ-B9onE9ONHRJUV4ech1a3SGrZYY0C_s&libraries=places"></script>
@endsection
