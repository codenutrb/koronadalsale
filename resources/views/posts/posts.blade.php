@extends('layouts.master')

@section('title', 'Online buy and sell')

@section('content')
<div class="row">
    @if(Request::is('posts/search*'))
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div>
                <h5>Show results for:</h5>
            </div>
           <div class="panel panel-profile">
             <span class="panel-title heading-sm">
            </span>
                <div class="panel-heading overflow-h">
                    <span class="panel-title heading-sm pull-left"> <a href="/posts/search/all/{{$searchText}}">All Categories</a></span>
                </div>

                <div class="panel-body">
                    <ul class="search-categories">
                        @foreach($categories as $key => $_category) 
                        @if($key != '')
                        <li>
                            @if($category !== $key)
                            <a href="/posts/search/{{$key}}/">
                                {{ $categories[$key] }} 
                            </a> 
                            @else
                                {{ $categories[$key] }} 
                            @endif
                        </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
            </div>
         </div>
    </div>
    @endif
    <div class="col-xs-12 col-sm-12 @if(Request::is('posts/search*')) col-md-9 no-left-padding @else col-md-12 @endif">
        @include('posts.index') 
    </div>
</div>
@endsection