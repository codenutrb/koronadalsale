<div class="col-xs-12 col-sm-12 col-md-6 no-side-padding">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-profile no-side-padding">
            <div class="panel-heading overflow-h">
                <h2 class="panel-title heading-sm pull-left"><i class="fa fa-photo"></i> Photos</h2>
            </div>
            <div class="panel-body">
              <div class="sliderContainer clearfix" >
                    <ul id="lightSlider" class="gallery list-unstyled cS-hidden">
                      @if(count($post['photos']) > 0)
                      @foreach($post['photos'] as $key => $photo) 
                        <li data-thumb="{{$photo['thumbnail']}}">
                        {!! Html::image($photo['medium'], '') !!}
                        </li>
                      @endforeach
                      @else
                      <li>
                        {!! Html::image('/img/no-image.gif', '', ['class' => 'post-image']) !!}
                      </li>
                      @endif
                    </ul>
                  </div>
            </div>
        </div>
    </div>
    
    @include('posts.comments')
</div>
