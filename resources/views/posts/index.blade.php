@if(count($results) > 0)
<div class="col-md-12">
@if(Request::is('posts/search*'))
<h5><a href="/posts/search/{{$category}}/{{$searchText}}">@if($category=='all')All Categories @else {{ $categories[$category] }} @endif </a>
<i class="fa fa-chevron-right small-text"></i>  @if($searchText)"{{ $searchText }}" @endif 
<span class="normal-font-weight">( {{ $total }} @if($total > 1)Results @else Result @endif)<span></h5>
@else
<h5>Latest posts</h5>
@endif
</div>
@if(isset($results))
@foreach($results as $key => $post)    
<div class="col-xs-6 col-sm-4 col-md-3 post-container" ng-controller="PostController">
    <div class="posts-content">
        <div class="easy-block-v1">
            <a href="{!! URL::to('post/' . $post['_id'] . '/' . str_slug($post['title'])) !!}">
            @if(count($post['photos']) > 0) 
                {!! Html::image(URL::to($post['photos'][0]['small']), '', ['class' => 'img-responsive']) !!}
            @else
                {!! Html::image(URL::to('img/no-image.gif'), '', ['class' => 'img-responsive', 'style' => 'margin: 0 auto;']) !!}
            @endif
            <div class="easy-block-v1-badge rgba-dark-grey">&#8369;{{$post['price']}}</div>
            </a>
        </div>
        <div class="post">
            <div class="post-title">
                <a class="color-dark" href="{!! URL::to('post/' . $post['_id'] . '/' . str_slug($post['title'])) !!}">
                    <strong>
                        {{ $post['title'] }}
                    </strong>
                </a>
            </div>
            <ul class="list-unstyled post-info">
                <li>
                    <i class="fa fa-user"></i>
                    <strong>
                    <a href="{{ URL::to('user/' . ($post['user']['_id']) . '/posts')}}"> 
                        {{ $post['user']['name'] }} 
                    </a>
                </strong>
                </li>
                <li >
                    <i class="fa fa-map-marker"></i>
                    {{ $post['location']['state'] }}
                </li>
                 <li>
                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ (new Date($post['created_at']))->format('F d, Y') }} </span>
                </li>
            </ul>
        </div>
        <div class="posts-share">
            <input type="hidden" ng-init="like.like={{ $post['user_likes'] && in_array(Request::session()->get('user')['_id'], $post['user_likes']) ? 'true' : 'false' }}" />
            <input type="hidden" ng-init="like.count={{ $post['user_likes'] ? count($post['user_likes']) : 0 }}" />
            <ul class="list-inline pull-left">
                <li><a href="{!! URL::to('post/' . $post['_id'] . '/' . str_slug($post['title'])) !!}#comments"><i class="fa fa-lg fa-comments"></i> @if(count($post['comments']) > 0) {{ count($post['comments']) }} @endif</li></a>
                <li ng-cloak>
                    <a ng-click="likePost('{{ $post['_id'] }}')">
                        <i class="fa fa-lg" ng-class="like.like ? 'fa-heart' : 'fa-heart-o' "></i>
                        <span ng-show="like.count > 0">@{{ like.count }}</span>
                    </a> 
                </li>
               
            </ul>
        </div>
    </div>
</div>
@endforeach
@endif
@if($total > $limit)
<div class="col-xs-12 col-sm-12 col-md-12">
<nav>
    <ul class="pagination">
        @if(ceil(1.0 * $total / $limit) > 4)
            <li class="@if($p == 1) disabled @endif">
                <a href="{{ Request::url() . '?p=1' }}">
                    <span aria-hidden="true">First</span>
                </a> 
            </li>
        @endif
        <li class="@if($p == 1) disabled @endif">
            <a href="@if($p > 1){{ Request::url() . '?p=' . ($p - 1) }}@endif" aria-label="Previous">
                <span aria-hidden="true">Previous</span>
            </a>
        </li>
        @for($i = min(max(0, $p - 4), max(0,  ceil(1.0 * $total / $limit) - 5)); $i < min(max($p + 1, 5), ceil(1.0 * $total / $limit)); $i++)
        <li class="@if($p == $i + 1) active @endif">
            <a href="{{ Request::url() . '?p=' . ($i + 1) }}">{{$i + 1}}</a>
        </li>
        @endfor
        <li class="@if($p == ceil(1.0 * $total / $limit)) disabled @endif">
            <a href="@if($p < ceil(1.0 * $total / $limit)) {{ Request::url() . '?p=' . ($p + 1) }} @endif" aria-label="Next">
                <span aria-hidden="true">Next</span>
            </a>
        </li>
        @if(ceil(1.0 * $total / $limit) > 4)
        <li class="@if($p == ceil(1.0 * $total / $limit)) disabled @endif">
            <a href="{{ Request::url() . '?p=' . (ceil(1.0 * $total / $limit)) }}">
                <span aria-hidden="true">Last</span>
            </a>   
        </li>
        @endif
    </ul>
</nav>
</div>
@endif
@else
<div class="col-xs-12 col-sm-12 col-md-12" >
    <div class="jumbotron center">
        <h2>Nothing to show here</h2> 
    </div>
@endif