

<div class="col-xs-12 col-sm-12 col-md-6 no-side-padding">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-profile no-side-padding">
            <div class="panel-heading overflow-h">
                <h2 class="panel-title heading-sm pull-left"><i class="fa fa-tag"></i> {{ $post['title'] }}</h2>
                <h2 class="panel-title heading-sm pull-right">
                    <div class="fb-share-button" data-href="{!! Request::url() !!}" data-layout="button"></div>
                    @if($post['user_id'] === Request::session()->get('user')['_id'])
                    &nbsp;<a href="{{ URL::to('/post/edit/' . $post['_id']) }}"><i class="fa fa-edit"></i> Edit</a>
                    @endif
                </h2>
            </div>

            <div class="panel-body">
                 <ul class="list-unstyled social-contacts-v2">
                    <li>
                        <span class="label-name post-label">
                            <i class="fa fa-user"></i>
                        </span>&nbsp;
                        <a href="{!! URL::to('user/' . $post['user']['_id'] . '/posts') !!}">{{ $post['user']['name'] }}</a>
                    </li>
                    <li>
                        <span class="label-name post-label">
                            &#8369;</i>
                        </span>&nbsp;{{ $post['price'] }}
                    </li>
                    
                    <li>
                        <span class="label-name post-label">
                            <i class="fa fa-map-marker"></i>
                        </span>&nbsp;
                        {{ $post['location']['name'] }}
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-profile no-side-padding">
            <div class="panel-heading overflow-h">
                <h2 class="panel-title heading-sm pull-left"><i class="fa fa-clone"></i> Description</h2>
            </div>
            <div class="panel-body">
                <p>{{ $post['description'] }}</p>
            </div>
        </div>
    </div>
 
     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-profile no-side-padding">
            <div class="panel-heading overflow-h">
                <h2 class="panel-title heading-sm pull-left"><i class="fa fa-user"></i> About the seller</h2>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled social-contacts-v2">
                    <li><span class="label-name">Email</span>&nbsp; {{ $post['user']['email'] }} </li>
                    <li><span class="label-name">Phone</span>&nbsp; {{ $post['user']['phone'] }} </li>
                    <li><span class="label-name">Date joined</span>&nbsp; {{ (new Date($post['user']['created_at']))->diffForHumans() }} </li>
                    <li><span class="label-name">Last activity</span>&nbsp; {{ (new Date($post['user']['created_at']))->diffForHumans() }} </li>
                </ul>
            </div>
        </div>
    </div>
    <input type="hidden" ng-init="like.like={{ isset($post['user_likes']) && in_array(Request::session()->get('user')['_id'], $post['user_likes']) ? 'true' : 'false' }}">  
    <input type="hidden" ng-init="like.count={{ isset($post['user_likes']) ? count($post['user_likes']) : 0 }}">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-profile no-side-padding">
            <div class="panel-body overflow-h" ng-cloak>
                @if($post->user_id != Request::session()->get('user')['_id']) 
                <a href="{!! URL::to('conversations/all/' . $post->user_id) !!}" class="btn btn-primary btn-lg btn-block" type="button">Contact seller</a>
                @endif
                <div class="btn-group btn-group-justified like-btn-group">
                    <a class="btn btn-success btn-lg btn-block" ng-click="likePost('{{ $post['_id'] }}')">
                        <span ng-if="like.like">Unlike this post</span>
                        <span ng-if="!like.like">Like this post</span>
                    </a>
                    <a class="btn btn-default btn-lg btn-block" style="width: .25%">
                         <span ng-if="like.count > 0">@{{ like.count }}</span>
                    </a>
                </div>
                
            </div>
        </div>
    </div>
</div>

@section('js')
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
@endsection