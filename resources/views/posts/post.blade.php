@extends('layouts.master')

<?php
    if(isset($post['price'])) {
        $title = 'Php ' . $post['price'] . ' - ';
    }
    $title .= $post['title'];
?>

@section('meta')
    <meta name="Description" content="{!! $post['title'] !!}" />
    <meta property="og:url" content="{!! Request::url() !!}" />
    <meta property="og:type"  content="website" />
    <meta property="og:title"  content="{!! $title !!}" />
    <meta property="og:description" content="{{ $post['description' ]}}" />

    @foreach($post['photos'] as $photo)
    @if(starts_with($photo['medium'], '/img/'))
    <meta property="og:image"   content="{{ url() . $photo['medium']}}" />
    @else
    <meta property="og:image"   content="{{ $photo['medium']}}" />
    @endif 
    @endforeach
@endsection

@section('title', $post['title'])

@section('content')
    <div class="row" ng-controller="PostController">
        @include('posts.post-left-panel')        
        @include('posts.post-right-panel')        
    </div>
@endsection