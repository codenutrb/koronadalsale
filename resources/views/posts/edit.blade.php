@extends('layouts.master')

@section('title', 'Add new post')

@section('content')
<div class="row" ng-controller="PostController">
    <div class="col-xs-12 col-sm-12 col-md-12 no-side-padding">
      
         <div class="col-xs-12 col-sm-12 col-md-6" >
            <div class="panel panel-profile no-side-padding" >
                <div class="panel-heading overflow-h">
                    <h2 class="panel-title heading-sm pull-left"><i class="fa fa-photo"></i> Photos</h2>
                </div>
                @include('posts.upload-photos')
            </div>
        </div>
      
      
        <form ng-submit="update()">
            <input type="hidden" id="_id" name="_id" ng-init="post._id='{{$post['_id']}}'" ng-model="post._id">
            <input type="hidden" id="user_id" name="user_id" ng-init="post.user_id='{{$post['user_id']}}'" ng-model="post.user_id">
            @if(isset($post['photos']))
            @foreach($post['photos'] as $key => $photo)
                <input type="hidden" ng-model="files[{{$key}}]" ng-init="files[{{$key}}].result={{json_encode($photo)}}">
                <input type="hidden" ng-model="post.oldPhotos[{{$key}}]" ng-init="post.oldPhotos[{{$key}}]={{json_encode($photo)}}">
            @endforeach
            @endif
            <input type="hidden" id="city" name="location[city]" ng-model="post.location.city" ng-init="post.location.city='{{ isset($post['location']['city']) ? $post['location']['city'] : '' }}'">
            <input type="hidden" id="state" name="location[state]" ng-model="post.location.state" ng-init="post.location.state='{{ isset($post['location']['state']) ? $post['location']['state'] : '' }}'">
            <input type="hidden" id="state" name="location[region]" ng-model="post.location.region" ng-init="post.location.region='{{ isset($post['location']['region']) ? $post['location']['region'] : '' }}'">
            <input type="hidden" id="state" name="location[country]" ng-model="post.location.country" ng-init="post.location.country='{{ isset($post['location']['country']) ? $post['location']['country'] : '' }}'">

            <input type="hidden" id="lat" name="location[geometry][lat]" ng-model="post.location.geometry.lat" ng-init="post.location.lat='{{ isset($post['location']['geometry']['lat']) ? $post['location']['geometry']['lat'] : '' }}'">
            <input type="hidden" id="lng" name="location[geometry][lng]" ng-model="post.location.geometry.lng" ng-init="post.location.lng='{{ isset($post['location']['geometry']['lng']) ? $post['location']['geometry']['lng'] : '' }}'">
             <input type="hidden" ng-init="original_location='{{ isset($post['location']['name']) ? $post['location']['name'] : '' }}'">

            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="panel panel-profile no-side-padding">
                    <div class="panel-heading overflow-h">
                        <h2 class="panel-title heading-sm pull-left"><i class="fa fa-archive"></i> Update item</h2>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-sm-12 col-md-12 no-side-padding">
                            @include('components.ajax-errors')
                        </div>
                        <div class="form-group" ng-class="(errors && errors['title']) ? 'has-error' : ''">
                            <label for="name" class="control-label">Title</label>
                            <input type="text" class="form-control" name="title" ng-init="post.title='{{$post->title}}'" ng-model="post.title" required>
                        </div>
                        <div class="form-group" ng-class="(errors && errors['category']) ? 'has-error' : ''">
                            <label for="name" class="control-label" >Category</label>
                            {!! Form::select('category', $categories, old('category'), ['class' => 'form-control', 'required', 'ng-model' => 'post.category', 'ng-init' => 'post.category=\'' . $post['category'] . '\'' ]) !!}
                        </div>
                        <div class="form-group" ng-class="(errors && errors['price']) ? 'has-error' : ''">
                            <label for="name" class="control-label">Price</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon1">&#8369;</span>
                                <input type="text" class="form-control" name="price" ng-init="post.price='{{$post->price}}'"  id="price" required ng-model="post.price">
                                <span class="input-group-addon" id="sizing-addon1">PHP</span>
                            </div>
                        </div>
                        <div class="form-group" ng-class="(errors && errors['glocation']) ? 'has-error' : ''">
                            <label for="name" class="control-label">City / Location</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" name="glocation" id="glocation" autocomplete = "off" ng-init="post.location.name='{{isset($post['location']['name']) ? $post['location']['name'] : '' }}'"  required ng-model="post.location.name">
                            </div>
                        </div>
                        <div class="form-group" ng-class="(errors && errors['description']) ? 'has-error' : ''">
                            <label for="name" class="control-label">Description</label>
                            <textarea class="form-control" name="description" rows="10" ng-init="post.description='{{$post['description']}}'"   ng-model="post.description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 no-side-padding">
                    <div class="panel panel-profile no-side-padding">
                        <div class="panel-heading overflow-h">
                            <button data-loading-text="Loading..." id="submit-button" class="btn btn-primary btn-block" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ-B9onE9ONHRJUV4ech1a3SGrZYY0C_s&libraries=places"></script>
@endsection
