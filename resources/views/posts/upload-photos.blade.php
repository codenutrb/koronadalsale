 <div id="inputFiles"
     ngf-select 
     ngf-pattern="'image/*'"
     ngf-multiple="true"
     ngf-fix-orientation="true"
     accept="image/*" 
     ngf-change="addPhotos($files, $file, $newFiles)"
     ngf-validate="{size: {max: '10MB'} }"
     ngf-max-size="10MB">
</div>
<div class="panel-body post-photos">
    <div class="alert alert-info" style="margin-bottom: 55px">
        Please upload at least one photo of your item. You can add up to six photos that will show how awesome your item. Image size should not exceed 10MB.
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 no-side-padding" >
        <div class="col-xs-6 col-sm-4 col-md-4" style="margin-bottom: 20px" ng-repeat="index in [] | range:6" ng-cloak>
            <div class="col-xs-12 col-sm-12 col-md-12 gallery-box">
                <div class="thumbnail" 
                     ng-click="selectPhotos(index)"
                     ng-class="index === files.length ? 'upload-thumbnail' : ''">

                    <img ng-if="files[index].result.thumbnail" ng-src="@{{files[index].result.thumbnail}}" alt="">
                    <section ng-if="index===files.length">
                        <br/>
                        <i class="fa fa-camera fa-4x"></i>
                        <p>Add photos</p>
                    </section>
                    <section ng-if="files[index].result.errors" class="errors">
                        <i class="fa fa-close fa-4x"></i>
                        <p><span ng-repeat="error in files[index].result.errors">@{{error | joinBy}}</span></p>
                    </section>
                </div>

                <div class="col-md-12" ng-show="files[index].result">
                    <a ng-show="files[index].result" ng-click="removePhoto(index)" >
                        <i class="fa fa-remove"></i> Remove 
                    </a>
                    <a ng-show="files[index].result.errors" ng-click="uploadFile(files[index])">
                        <i class="fa fa-refresh"></i> Retry 
                    </a>
                </div>

                 <div class="col-md-12" ng-show="!files[index].result" style="height: 0">
                    &nbsp; <!-- hack hack hack ! -->
                 </div>
                
                <div ng-hide="files[index].result" class="col-md-12 no-side-padding progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="@{{files[index].progress}}"
                  aria-valuemin="0" aria-valuemax="100" style="width:@{{files[index].progress}}%">
                    <span class="sr-only">@{{files[index].progress}}% Complete</span>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>