<div class="col-xs-12 col-sm-12 col-md-12" ng-controller="CommentController" id="comments">
    <div class="panel panel-profile no-side-padding">
        <div class="panel-heading overflow-h">
            <h2 class="panel-title heading-sm pull-left"><i class="fa fa-comment"></i> Comments <span ng-if="comments.length > 0" ng-cloak> (@{{ comments.length }}) </span></h2>
        </div>
        

        <div class="panel-body comment-body no-top-border">
            <div class="media media-v2">
              <a class="pull-left" href="{{ URL::to('/user/' . Request::session()->get('user')['_id'] . '/posts') }}">
              @if(Request::session()->get('user')['avatar'])
              <img class="media-object thumbnail my-thumbnail" height="80" width="80" src="{!! URL::to(Request::session()->get('user')['avatar']) !!}" alt="">
              @else
              <img class="media-object thumbnail my-thumbnail" height="80" width="80" src="{{URL::to('img/avatar-default.png')}}" alt="">
              @endif
              </a>
              <div class="media-body">
                  <a class="pull-left comment-name" href="{{ URL::to('/user/' . Request::session()->get('user')['_id'] . '/posts') }}">
                  <strong>{{ Request::session()->get('user')['name'] }}</strong> 
                  </a>
                  <form ng-submit="save()">
                    <input type="hidden" ng-model="comment.post_id" ng-init="comment.post_id='{{ $post['_id'] }}'" />
                    <input type="hidden" ng-model="comment.user_id" ng-init="comment.user_id='{{ Request::session()->get('user')['_id'] }}'" />
                    <textarea class="form-control" ng-model="comment.body" rows="4" placeholder="Add a comment..."></textarea>
                    <button type="submit" ng-disabled="!comment.body" class="btn btn-primary comment-button">Comment</button>
                  </form>
              </div>
          </div>
        </div>

        @foreach($post['comments'] as $key => $comment)
        <input type="hidden" ng-model="comments[{{$key}}]" ng-init="comments[{{$key}}]={{ $comment }}" />
        <input type="hidden" ng-model="comments[{{$key}}]['user']" ng-init="comments[{{$key}}]['user']={{ json_encode($comment->shallowUser(['avatar', '_id', 'name'])) }}" />
        @endforeach

        <div class="panel-body panel-profile comment-body" ng-repeat="comment in comments|limitTo:paginate.results" ng-cloak>
            <div class="media">
              <div class="media-left">
              <a class="pull-left" href="{{ URL::to('/user/') }}/@{{ comment['user']['_id'] }}/posts">
                  <img ng-if="comment['user']['avatar']" class="media-object thumbnail my-thumbnail" height="80" width="80" ng-src="@{{comment['user']['avatar']}} " alt="">
                  <img ng-if="!comment['user']['avatar']" class="media-object thumbnail my-thumbnail" height="80" width="80" src="{{URL::to('img/avatar-default.png')}}" alt="">
              </a>
            </div>
              <div class="media-body media-right">
                  <strong><a class="comemnt-name" href="{{ URL::to('/user/') }}/@{{ comment['user']['_id'] }}/posts">@{{ comment['user']['name'] }}</a></strong> 
                  <small class="pull-right"><span am-time-ago="comment['created_at']" }}</span></small>
                  <p class="comment-content">@{{ comment['body'].trim() }}</p>
              </div>
            </div>
        </div>

        <div class="panel-body comment-body" ng-hide="paginate.results>=comments.length" ng-cloak>
          <span class="pull-left comment-load">@{{paginate.results}} of @{{comments.length}} Comments</span>
          <div class="pull-right btn-group">
            <button class="btn btn-default" ng-click="paginate.results=paginate.results+paginate.perpage">Load more</button>    
          </div>
        </div>
 
    </div>
</div>