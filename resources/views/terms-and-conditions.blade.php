@extends('layouts.master')

@section('title', 'Terms and conditions')

@section('content')
<div class="container no-side-padding">
<div class="col-lg-8 col-md-8 col-sm-8 content-inner-detail no-side-padding">
    <div class="col-md-12 center">
        <h3><strong>TERMS AND CONDITIONS</strong></h3>
        <br/>
    </div>
    
    <p class="terms-para">Welcome to Koronadal Sale! Here are all terms and conditions principal for your use. Access Koronadal Sale as website. You accept and consent with all following terms, which is assuring you that Koronadal Sale is work for everyone. After referring this Acceptable Use Policy (AUP) you can easily log in to Koronadal Sale also use to with bonds and privacy policy and detail of listing.
    </p>

    <h4><strong>How to Use:</strong></h4>
    <p class="terms-para">You must satisfied and agree that Koronadal Sale is very easy to use and user friendly website which give you large platform included all guideline, Behind purpose of advertising and information for services.
    </p>
     
    <h4><strong>Eligibility:</strong></h4>
    <p class="terms-para">You can use Koronadal Sale by registration or by any other purpose, it supports all citizens of deferent countries. Not banned by and law of power. If once you use Koronadal Sale by registration or by other mean, you are fully allowed to do the listing, posting or get information placed on the site on behalf of the legal entity is your responsibility and you agree to be accountable for the same to other users of the Site.
    </p>

    <h4><strong>Abuse of Koronadal Sale:</strong></h4>

    <p class="terms-para">If you be a part of any listing that is unpleasant or violates our policy. Please send us an email at <a href="mailto:info@koronadalsale.com">info@koronadalsale.com</a>. We keep the right to take decision for misuse of site in keeping with the listing policy. If any case you come across any problem when using our site or service you kindly requested to report the problem by clicking on this link <a href="mailto:info@koronadalsale.com">info@koronadalsale.com</a>.
    </p>
     
    <h4><strong>Privacy Policy:</strong></h4>

    <p class="terms-para">Here you have to agree that you are not allow to list or post or present detail in relation to the sale or purchase or if you exchange of goods. Please do not post any content or information which are illegal and which not not acceptable as per policy.</p>
     
    <h4><strong>General:</strong></h4>

    <p class="terms-para">As per updating activities we might update, change, modify this policy at any time .If Corporation may share personal information with another company, but this policy will continue to apply. Send any question about this policy to <a href="mailto:info@koronadalsale.com">info@koronadalsale.com</a></p>

</div>
</div>
@endsection