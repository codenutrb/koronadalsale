<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Verify Your Email Address</h2>

        <div>
            Thank you for creating an account with Koronadal Sale.
            <br><br/>
            Please follow the link below to verify your email address
            {{ URL::to('user/verify/' . $confirmation_code) }}.<br/><br/>

            Thanks, <br/>
            Koronadal Sale Team
        </div>

    </body>
</html>