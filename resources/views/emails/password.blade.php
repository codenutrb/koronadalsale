<!DOCTYPE html> 
  <html lang="en-US"> 
  <head> <meta charset="utf-8"> </head> 
  <body> 
    <h3>Password Recovery</h3> 
    
    <div> 
      <p>You (or someone else) has requested to reset your password. This password reset request is valid for the next 24 hours. </p>
      
      <p>Click <a href="{!! url('password/reset/'.$token) !!}">here</a> to reset your password: {{ url('password/reset/'.$token) }} </p>
      
      Thanks, <br/>
      Koronadal Sale Team 
    </div> 
  </body> 
</html>
